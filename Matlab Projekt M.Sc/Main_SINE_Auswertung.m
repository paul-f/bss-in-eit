%% INIT
% noise sine
fs = 47.68;                                 % sample frequency
N = 60*60*fs;                               % sample points for one hour
time = (0:N-1)/fs;

f = 1;                                      % signal frequency
s = sin(2*pi*f*time);                       % sine
rng('default')                              % same random generater
n = wgn(1, N, 0)./3;                        % weak white noise (Feature: Communication_Toolbox, Licensing error: -4,132.)
% load('whitenoise.mat');                   % alternative function
xSn = s+n;                                  % noisy sine

% plot: Signal
figure
subplot(2,1,1)
plot(time,s)
hold on
plot(time,n)
set(gca, 'FontName', 'Times New Roman')
legend('Sinus', 'Rauschen')
xlabel('Zeit / s')
ylabel('Amplitude')
xlim([0 20])
subplot(2,1,2)
plot(time,xSn)
xlim([0 20])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')


%% Digitale Filterung ( Zero-phase Filtering wegen filtfilt() )
% FFT
N = length(xSn);
w = window(@hamming,N);                                                 % window
[XSn, fAxis] = ffts(xSn.*w', N*10, fs);                                 % fft unfiltered

% Filter
type = 'bp';                % lowpass (lp), highpass(hp), bandpass (bp)
character = 'butter';       % Butterworth (butter), Tschebyscheff-I/II (cheby1/cheby2), Cauer (ellip)
order = 4;                  % TP/HP: order, BP: order*2;
[fFilter, ySn] = eit_filter(xSn, type, character, order, f, fs);        % filter

% plot: FFT und gefiltertes Signal
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862*0.5]); 
subplot(1,2,1)
plot(fAxis,XSn(1:length(fAxis)))
xlim([0 2])
ylim([-max(XSn)/20 max(XSn)+max(XSn)/20])
% axis([0 2 -0.05 1.05])
xlabel('Frequenz / Hz')
ylabel('Amplitude')
set(gca, 'FontName', 'Times New Roman')
subplot(1,2,2)
plot(time,ySn)
xlim([0 20])
xlabel('Zeit / s')
ylabel('Amplitude')
set(gca, 'FontName', 'Times New Roman')

% SNR (Filterung)
nFilt = s-ySn;
SNRFilt = snr(s,nFilt)                                          % SNR wird nur ausgegeben

% plot: SNR Filterung
figure;
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(time,s)
hold on
plot(time,ySn)
plot(time,nFilt)
xlim([0 20])
set(gca, 'FontName', 'Times New Roman')
legend('Unverrauschter Sinus','Verrauschter Sinus','Filterung des verrauschten Sinus')
xlabel('Zeit / s')
ylabel('Amplitude')


%% EA
[pks,locs] = findpeaks(s);
clear pks
R_Peaks = zeros(1,length(xSn));
for k = 1:length(locs)
    R_Peaks(locs(k)) = max(xSn);
end

% plot: R-Peaks
figure;
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(time, xSn)
hold on
plot(time, R_Peaks)
xlim([0 20])
set(gca, 'FontName', 'Times New Roman')
legend('Verrauschter Sinus','R-Peaks')
xlabel('Zeit / s')
ylabel('Amplitude')

% Ensemble Averaging
clear EA_
lim = zeros(1,length(locs)-1);
for k = 2:length(locs)
    lim(k-1) = locs(k)-locs(k-1);
end
lim = ceil(mean(lim)/2);

% EA_ = zeros(length(locs)-2,2*lim+1);                        % -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
for k = 2:length(locs)-1
    EA_(k-1,:) = xSn((locs(k)-lim):((locs(k)+lim)));       % Signal bei R-Peaks
end
EA = mean(EA_);
clear k

% plot: Segmente und EA
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862*0.5]); 
subplot(1,2,1)
hold on
plot(time(1:length(EA)),EA_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
xlim([0 time(length(EA))])
box on
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(1,2,2)
plot(time(1:length(EA)),EA)
xlim([0 time(length(EA))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')

%% plot: EA in Abh. der Anz. der Mittelungen
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862*9/12]); 
subplot(2,4,1)
plot(EA_(1,:))
xlim([0 length(EA)])
ylim([-1.5 1.5])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 1')
subplot(2,4,5)
plot(mean(EA_(1:3,:)))
xlim([0 length(EA)])
ylim([-1.5 1.5])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 3')
subplot(2,4,2)
plot(mean(EA_(1:10,:)))
xlim([0 length(EA)])
ylim([-1.5 1.5])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 10')
subplot(2,4,6)
plot(mean(EA_(1:30,:)))
xlim([0 length(EA)])
ylim([-1.5 1.5])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 30')
subplot(2,4,3)
plot(mean(EA_(1:100,:)))
xlim([0 length(EA)])
ylim([-1.5 1.5])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 100')
subplot(2,4,7)
plot(mean(EA_(1:300,:)))
xlim([0 length(EA)])
ylim([-1.5 1.5])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 300')
subplot(2,4,4)
plot(mean(EA_(1:1000,:)))
xlim([0 length(EA)])
ylim([-1.5 1.5])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 1000')
subplot(2,4,8)
plot(EA)
xlim([0 length(EA)])
ylim([-1.5 1.5])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 3598 (max)')

% SNR (EA)
s_ = s((locs(7)-lim):((locs(7)+lim)));              % Signal bei R-Peak an der Stelle locs(2)
nEA = s_-EA;                                         % Rauschen berechen
SNREA = snr(s_,nEA)                                          % SNR wird nur ausgegeben

% SNR in Abh. der Anzahl (EA)
clear EA_SNR SNR
for k = 1:length(EA_)
    % 1. EAs berechnen
    if k == 1
        EA_SNR(k,:) = EA_(1:k,:);                   % EA in Abh. der Peak (EA = EA_(1))
    else
        EA_SNR(k,:) = mean(EA_(1:k,:));             % EA in Abh. der Peaks
    end
    % 2. Rauschen schätzen
    n_EA(k,:) = s_-EA_SNR(k,:);                   % Rauschen für die verschiedenen EAs in Abh. der Peaks              
    % 3. SNR berechnen
    SNR(k) = snr(s_,n_EA(k,:));                   % SNR in Abh. der Peaks
end

% plot: SNR(max) und SNR in Abh. der Anz. der Mittelungen
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862/1.5]);
subplot(1,2,1)
plot(time(1:length(s_)),s_)
hold on
plot(time(1:length(s_)),EA)
plot(time(1:length(s_)),nEA)
set(gca, 'FontName', 'Times New Roman')
legend('Sinus', 'EA', 'Rauschen')
xlim([0 time(length(s_))])
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(1,2,2)
plot(SNR)
hold on
plot(15.5*nthroot(1:length(SNR),8))
grid on
grid minor
xlim([0 length(SNR)])
xticks([0:500:3500])
set(gca, 'FontName', 'Times New Roman')
xlabel('N')
ylabel('SNR(N) / dB')
legend('SNR(N)', '{^{8}\surd}N','Location','southeast')


%% Vgl. Sinus, Filterung und EA
ySn_ = ySn(locs(7)-lim:locs(7)+lim);

% plot: Vergleich von Signal, gefiltertes Signal, EA
figure
plot(time(1:length(s_)),s_)
hold on
plot(time(1:length(s_)),ySn_)
plot(time(1:length(s_)),EA)
xlim([0 time(length(s_))])
set(gca, 'FontName', 'Times New Roman')
legend('unverrauschter Sinus', ...
    'gefilterter verrauschter Sinus', ...
    'EA vom verrauschten Sinus')
xlabel('Zeit / s')
ylabel('Amplitude')
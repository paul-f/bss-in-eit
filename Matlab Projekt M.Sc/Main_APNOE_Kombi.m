%% INIT
addpath(genpath('C:\Users\paulf\Documents\Studium\Master\2. Semester\Projekt M.Sc - EIT'))
clearvars -except simpleFrameData constFrameData
if 0
    help init
%     simpleFrameData = load('HLI_20200405_PIG07_BO-4_reconstructed.mat');
    constFrameData = load('HLI_20200405_PIG07_BO-6_reconstructed.mat');
    clearvars -except simpleFrameData constFrameData
end


%% HP-Filterung ventilated
constEIT = squeeze(constFrameData.data.imgs(7,16,:));
constEIT = constEIT - mean(constEIT);
ven = 1:1200;
constEITven = constEIT(ven);
constEITven = constEITven - mean(constEITven);

fs = 47.68;
time = (0:length(constEIT)-1)/fs;

N = 1200;                                                   % N = 1200 bei ven und ap
w = window(@hamming,N); 
[ConstEITven, fAxis] = ffts(constEITven.*w, N*10,fs);         % fft const

% apnea (hp)
type = 'hp';
order = 8;
character = 'butter';
[M, fconst] =  max(ConstEITven(min(find(fAxis>0.5)):length(fAxis)));      % Mittenfrequenzen suchen
fconst = fAxis(fconst);
clear M
[fFilter, constEITvenFiltHP] = eit_filter(constEITven, type, character, order, fconst, fs);  % filter
%%
ap = 2101:3300;
constEITap = constEIT(ap);
constEITap = constEITap - mean(constEITap);

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]);
hold on
plot(time(ven),constEITven)
plot(time(ven),constEITap)
plot(time(ven),constEITvenFiltHP)
legend('unfiltered', 'filtered')
xlim([0 5])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
% legend('HP-gefiltertes Signal (einfach)', 'HP-gefiltertes Signal (konstant)', ...
%     'Location','southeast')


%% EA (const ven)
% R-Peaks
clear constR_Peaksven
[pks,locs] = findpeaks(constEITvenFiltHP,'MinPeakProminence',0.2);
clear pks
constR_Peaksven = zeros(1,length(constEITvenFiltHP));
for k = 1:length(locs)
    constR_Peaksven(locs(k)) = max(constEITvenFiltHP);
end

% figure
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
% plot(constEITvenFiltHP)
% hold on
% plot(constR_Peaksven)
% % xlim([0 20])
% set(gca, 'FontName', 'Times New Roman')
% xlabel('Zeit / s')
% ylabel('Amplitude')


% Ensemble Averaging
lim = zeros(1,length(locs)-1);
for k = 2:length(locs)
    lim(k-1) = locs(k)-locs(k-1);
end
lim = ceil(mean(lim)/2);
lim = 9;
for k = 2:length(locs)-1
    EAconstEITvenFiltHP_(k-1,:) = constEITvenFiltHP((locs(k)-lim):((locs(k)+lim)));       % Signal bei R-Peaks
end
EAconstEITvenFiltHP = mean(EAconstEITvenFiltHP_);
clear k

% plot: Segmente und EA
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862*9/12]); 
subplot(1,2,1)
hold on
plot(time(1:length(EAconstEITvenFiltHP)),EAconstEITvenFiltHP_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
xlim([0 time(length(EAconstEITvenFiltHP))])
box on
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(1,2,2)
plot(time(1:length(EAconstEITvenFiltHP)),EAconstEITvenFiltHP)
xlim([0 time(length(EAconstEITvenFiltHP))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')

% plot: EA in Abh. der Anz. der Mittelungen
figure
% set(gcf, 'units','normalized','outerposition',[0.3123 0 0.374 0.8])
subplot(2,2,1)
plot(EAconstEITven_(1,:))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('Anzahl der Mittelungen: 1')
subplot(2,2,3)
plot(mean(EAconstEITven_(1:3,:)))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('Anzahl der Mittelungen: 3')
subplot(2,2,2)
plot(mean(EAconstEITven_(1:5,:)))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('Anzahl der Mittelungen: 5')
subplot(2,2,4)
plot(EAconstEITven)
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('Anzahl der Mittelungen: maximal')


%% EA (const apnoe)
% R-Peaks
clear R_Peaks
[pks,constLocs] = findpeaks(constEITap,'MinPeakProminence',0.3);
clear pks
R_PeaksVen = zeros(1,length(constEITap));
for k = 1:length(constLocs)
    R_PeaksVen(constLocs(k)) = max(constEITap);
end

% figure
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
% plot(time(ven),constEITap)
% hold on
% plot(time(ven),R_PeaksVen)
% xlim([0 25])
% set(gca, 'FontName', 'Times New Roman')
% xlabel('Zeit / s')
% ylabel('Amplitude')
% legend('EIT-Signal','R-Peaks')



% Ensemble Averaging
lim = zeros(1,length(constLocs)-1);
for k = 2:length(constLocs)
    lim(k-1) = constLocs(k)-constLocs(k-1);
end
lim = ceil(mean(lim)/2);

lim = 9;
for k = 2:length(constLocs)-1
    EAconstEITap_(k-1,:) = constEITap((constLocs(k)-lim):((constLocs(k)+lim)));       % Signal bei R-Peaks
end
EAconstEITap = mean(EAconstEITap_);
clear k

% plot: Segmente und EA
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862*9/12]); 
subplot(1,2,1)
hold on
plot(time(1:length(EAconstEITap)),EAconstEITap_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
xlim([0 time(length(EAconstEITap))])
box on
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(1,2,2)
plot(time(1:length(EAconstEITap)),EAconstEITap)
xlim([0 time(length(EAconstEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')

% plot: EA in Abh. der Anz. der Mittelungen
figure
hold on
plot(time(1:length(EAconstEITap)),EAconstEITap_(1,:))
plot(time(1:length(EAconstEITap)),mean(EAconstEITap_(1:3,:)))
plot(time(1:length(EAconstEITap)),mean(EAconstEITap_(1:30,:)))
plot(time(1:length(EAconstEITap)),EAconstEITap)
box on
xlim([0 time(length(EAconstEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('N = 1', 'N = 3', 'N = 30', 'N = 71 (max)', ...
    'Location','northwest') 

%% Vgl EA

figure
plot(EAconstEITvenFiltHP)
hold on
plot(EAconstEITap)
% xlim([0 time(length(EAconstEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
%% INIT
addpath(genpath('C:\Users\paulf\Documents\Studium\Master\2. Semester\Projekt M.Sc - EIT'))

% Zeilen zum Frame laden und mit F9 ausführen, dann kann mit F5 das
% Programm ohne den INIT-Part ausgeführt werden

if 0
    
    clear all
    help init       % Beschreibung der Messdaten in der Funktion
    [frame, frameData] = init();
    clearvars -except frame frameData
    
end


%% Darstellung eines einzelnen EIT Bildes 
figure
imagesc(frame(:,:,620))                     % stelle das 415. Bild dar
axis square


%% Impedanz einzelner Pixel
fs = 47.68;                                         % frame(y,x,sig)
xLung = squeeze(frame(32,20,:));                    % Lunge
xLung = xLung-mean(xLung);                          % Mittelwertfrei
xHeart = squeeze(frame(18,33,:));                   % Herz
xHeart = xHeart-mean(xHeart);                       % Mittelwertfrei
time = (0:length(frame)-1)/fs;

figure
hold on
plot(time, xLung)         
plot(time, xHeart)
legend('Lung Pixel', 'Heart Pixel')
xlabel('time [s]')
ylabel('Impedance [arb. units]')


%% FFT
fs = 47.68;
N = length(xLung);
w = window(@blackman,N);                            % window
[XLung, fAxis] = ffts(xLung.*w, N*10, fs);       % zero-padding and windowing
[XHeart, fAxis] = ffts(xHeart.*w, N*10, fs);
bpmAxis = fAxis*60;

figure
subplot(2,1,1)
plot(fAxis,XLung(1:length(fAxis))/max(XLung))
hold on
plot(fAxis,XHeart(1:length(fAxis))/max(XHeart))
axis([0 3 -0.03 1.03])
xlabel('Frequency (Hz)')
ylabel('Magnitude |Y(f)|')
subplot(2,1,2)
plot(bpmAxis,XLung(1:length(fAxis))/max(XLung))
hold on
plot(bpmAxis,XHeart(1:length(fAxis))/max(XHeart))
axis([0 180 -0.03 1.03])
xlabel('bpm')
ylabel('Magnitude |Y(f)|')


%% STFT
figure
stft(xLung,fs, 'Window',w, 'OverlapLength',220,...
    'FFTLength',length(fft(xLung)))
%,'Window',kaiser(256,5),'OverlapLength',220,'FFTLength',512);


%% Filter Test
if 0
% highpass(xLung,0.2,fS)
% lowpass(xLung,0.2,fS)
% 
% figure
% lowpass(xLung,0.2,fS);
% 
% [Y, fY_axis]=ffts(y.*w,length(y)*10,fS);
% figure
% plot(fY_axis,Y(1:length(fY_axis)))
end


%% Filter Lung
type = 'bp';    % lowpass (lp), highpass(hp), bandpass (bp)
character = 'butter';
order = 2;
[M, fMax] = max(XLung(min(find(fAxis>0.1)):length(fAxis)));
fMax = fAxis(fMax);
clear M
fs = 47.68;

[fFilter, yLung] = eit_filter(xLung, type, character, order, fMax, fs);
[YLung, fYAxis] = ffts(yLung.*w,length(yLung)*10,fs);

figure
plot(time(1:length(xLung)),xLung)
hold on
plot(time(1:length(yLung)),yLung)
legend('unfiltered', 'filtered')

figure
plot(fAxis,XLung(1:length(fAxis))/max(XLung))
hold on
plot(fYAxis,YLung(1:length(fYAxis))/max(YLung))
axis([0 3 -0.03 1.03])

% nLung = xLung-yLung;
% % figure
% % plot(time,nLung)
% 
% SNR = snr(xLung, nLung)


%% Filter Heart
type = 'bp';    % lowpass (lp), highpass(hp), bandpass (bp)
character = 'butter';
order = 2;
[M, fMax] = max(XHeart(min(find(fAxis>0.8)):length(fAxis)));
fMax = fAxis(fMax);
clear M
fs = 47.68;

[fFilter, yHeart] = eit_filter(xHeart, type, character, order, fMax, fs);
[YHeart, fYAxis] = ffts(yHeart.*w,length(yHeart)*10,fs);

figure
plot(xHeart)
hold on
plot(yHeart)
legend('unfiltered', 'filtered')

figure
plot(fAxis,XHeart(1:length(fAxis))/max(XHeart))
hold on
plot(fYAxis,YHeart(1:length(fYAxis))/max(YHeart))
axis([0 3 -0.03 1.03])

% nHeart = xHeart-yHeart;
% % figure
% % plot(nHeart)
% 
% SNR = snr(xHeart, nHeart)


%% GlobalSig
globalSig = zeros(length(frame),1);

for i = 1:64
    for j = 1:64
        globalSig = globalSig + squeeze(frame(i,j,:));
    end 
end
function [Y, f_axis] = ffts(y, N, fS)

df = fS/N;
f_axis = 0:fS/N:fS/2;

Y = fft(y,N);               % fft
Y = abs(Y).*(2/N);          % normalize
Y = Y(1:length(f_axis));
 
% PLOT
% plot(df_axis, Y(1:length(f_axis)));

end
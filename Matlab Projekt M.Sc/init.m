function [frame, frame_data] = init()
%   BEISPIEL [ex]:   
%       Beispiel_BeatmeterPatient.mat   -
%
%   SIMULATION [simu]:
%       * Amplitude (High | Normal | Low )
%       * Frequenz von Herzschlag (HR) und Atmung (BR)
%       * Beispiel: M50NmHR_40NmBR
%       ***     Herzfrequenz von 50 Schlägen pro Minute bei "normaler" Amplitude
%       ***     Atemfrequenz von 40 pro Minute bei normaler Amplitude
%       Measurements20190925.mat        - 
%       SentecRecon (32x32 Pixel!)      - 
%       NeuRecons_Messung20190925       - 
%       Messungen202006                 - 
% 

clear all
addpath(genpath(['C:\Users\paulf\Documents\Studium\',...
    'Master\2. Semester\Projekt M.Sc - EIT']))

%% Ordner wählen
help init; % Beschreibung der Datensätze
frame_type = input(['Example (ex), Simulation (simu), ',...
    'EKG (ekg) oder reale Daten (real)?\n\nEingabe: '], 's');
disp(' ')

switch frame_type
    case 'ex'
        folder = fullfile('Messdaten', 'Beispiel');
        
    case 'simu'
        folder = fullfile('Messdaten', 'EIT Interface Simulationen');
        
        disp('Welche Simulation? (copy & paste)')
        disp(' ')
        files = dir(folder);                                                        % Quelle: https://de.mathworks.com/matlabcentral/answers/166629-is-there-any-way-to-list-all-folders-only-in-the-level-directly-below-a-selected-directory
        subFolders = files([files.isdir]);   % dirFlags = [files.isdir]
        % remove '.' and '..'
        subFolders = subFolders(~ismember({subFolders(:).name},{'.','..'}));        % Quelle: https://de.mathworks.com/matlabcentral/answers/398428-how-do-i-get-only-the-subfolders-of-a-folder
        for k = 1 : length(subFolders)
            if k == length(subFolders)
                fprintf('%s', subFolders(k).name);
            else
                fprintf('%s\n', subFolders(k).name);
            end
        end
        dir(fullfile(folder, '*.mat'))                                              % Quelle: https://de.mathworks.com/matlabcentral/answers/147197-how-to-return-files-with-a-specific-extension-using-dir
        folder_sub = input('Eingabe: ', 's');
        
        if folder_sub == "Measurements20190925" || folder_sub == "Measurements20190925.mat"
            frame_type = 'simu1';
        else
            folder = fullfile(folder,folder_sub);
            if folder_sub == "SentecRecon"
                frame_type = 'simu2';
            else
                frame_type = 'simu3-4';
            end
        end
        
    case 'ekg'
        folder = fullfile('Messdaten', 'Daten mit EKG');
        
    case 'real'
        
    otherwise
        disp('error')
end


%% Wählen des Datensatzes
if frame_type == "simu1"
    frame_file = 'Measurements20190925.mat';
    disp('lädt...')
    disp(' ')
    frame_data = load(frame_file);
    frame_name = string(fieldnames(frame_data.data));
    disp('Wähle den Datensatz! (copy & paste)')
    for k = 1 : length(frame_name)
        fprintf('%s\n', frame_name(k));
    end
    frame_name = input('\nEingabe: ','s');
    frame_name = frame_name(~isspace(frame_name));
else
    disp('Wähle den Datensatz! (copy & paste)')
    dir(fullfile(folder, '*.mat'))                      % Quelle: https://de.mathworks.com/matlabcentral/answers/147197-how-to-return-files-with-a-specific-extension-using-dir
    frame_file = input('Eingabe: ','s');
    frame_file = frame_file(~isspace(frame_file));
end

%% Laden des Bildes
if frame_type ~= "simu1"
    disp('lädt...')
    frame_data = load(frame_file);
end

switch frame_type
    case 'ex'
        frame = frame_data.data.measurement.ZeroRef;
        
    case 'simu1'
        switch frame_name
            case 'M50NmHR_40NmBR'
                frame = frame_data.data.M50NmHR_40NmBR.img;
            case 'M72HiHR_12LoBR'
                frame = frame_data.data.M72HiHR_12LoBR.img;
            case 'M75HiHR_12HiBR'
                frame = frame_data.data.M75HiHR_12HiBR.img;
            case 'M75NmHR_12HiBR'
                frame = frame_data.data.M75NmHR_12HiBR.img;
            case 'MNoHR_12HiBR'
                frame = frame_data.data.MNoHR_12HiBR.img;
        end
        
    case 'simu2'
        frame = frame_data.Data.Images;
        
    case 'simu3-4'
        frame = frame_data.data.imgs;
        
    case 'ekg'
        frame = squeeze(frame_data.DataPig6.Apnea.Block01.EITApnea2D);
        
end

end
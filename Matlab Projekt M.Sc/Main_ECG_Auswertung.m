% %% INIT
addpath(genpath('C:\Users\paulf\Documents\Studium\Master\2. Semester\Projekt M.Sc - EIT'))
clearvars -except frameData
if 0
    help init
    frameData = load('DataWithECG.mat');
    clearvars -except frameData
end

%% Block mit konstanter Frequenz suchen -> Block03
% konstante Herzfrequenz suchen
fsECG = 1000;
Blocks = {'Block01', 'Block02', 'Block03', 'Block04', 'Block05', 'Block06', 'Block07', 'Block08', 'Block09', 'Block10'};
figure
hold on
legendEntries = [];
for nBlock = 9:length(Blocks)-1
    try
       set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862*0.5]); 
       plot((1:length(frameData.DataPig6.Apnea.(Blocks{nBlock}).HR))/fsECG, ...
           frameData.DataPig6.Apnea.(Blocks{nBlock}).HR)
       legendEntries = [legendEntries; (Blocks{nBlock})];
    catch
    end
end
set(gca, 'FontName', 'Times New Roman')
box on
% legend(legendEntries)
xlabel('Zeit / s')
ylabel('Herzschlag / bpm')
% ylim([50 70])
xlim([0 length(frameData.DataPig6.Apnea.(Blocks{nBlock}).HR)/fsECG])
% title('Heart Rates of all Apnea Measurements')


%% Signale wählen (beatmet und unbeatmet) in Herzpixel
% Festlegen des Signals im Herz
venEIT = squeeze(frameData.DataPig6.Ventilated.Block09.EITBreathing2D(7,16,:));      % Pixel aus Herzregion [ frame(y,x,sig) ]
venEIT = venEIT - mean(venEIT);
apEIT = squeeze(frameData.DataPig6.Apnea.Block09.EITApnea2D(7,16,:));                % Pixel aus Herzregion [ frame(y,x,sig) ]
apEIT = apEIT - mean(apEIT);

fsEIT = 47.68;     % Hz
timeAp = (0:length(apEIT)-1)/fsEIT;
timeVen = (0:length(venEIT)-1)/fsEIT;

fsECG = 1000;      % Hz
R_Peaks = frameData.DataPig6.Ventilated.Block09.R_Peaks;
ECG = frameData.DataPig6.Ventilated.Block09.ECG;
timeECG = (1:length(ECG))/fsECG;

ROI = load('Regions.mat');

%
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862/1.33]);
subplot(2,5,[1 2 6 7])
imagesc(frameData.DataPig6.Ventilated.Block09.EITBreathing2D(:,:,1180))
axis square
hold on
contour(ROI.ROIP06.Lung,1,'Linewidth',1,'Color',[1 1 1])
contour(ROI.ROIP06.Heart,1,'Linewidth',1,'Color',[1 1 1])
set(gca, 'FontName', 'Times New Roman')
subplot(2,5,3:5)
plot(timeVen(1:length(apEIT)),venEIT(1:length(apEIT)))
hold on
plot(timeAp,apEIT)
xlim([0 20])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('Beatmet', 'Apnoe')
subplot(2,5,8:10)
% hold on
% plot(timeAp, apEIT)                                      % Herzsignal
% yyaxis right
plot(timeECG, ECG)                                         % EKG-Signal
hold on
plot(timeECG(R_Peaks), ECG(R_Peaks), 'rx')                 % R-Peaks
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('ECG', 'R-Peaks')
xlim([0 20])
box on

%% FFT (Verschwindet Atmung?)
N = length(apEIT);
w = window(@hamming,N);                                     % window
[APEIT, fAxis] = ffts(apEIT.*w, N*10, fsEIT);               % fft Apnoe
[VENEIT, fYAxis] = ffts(venEIT(1:N).*w,N*10,fsEIT);         % fft beatmet

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(fYAxis,VENEIT(1:length(fYAxis)))
hold on
plot(fAxis,APEIT(1:length(fAxis)))
xlim([0 2.5])
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / Hz')
ylabel('Amplitude')
legend('FFT Beatment','FFT Apnoe')


%% Digitale Filterung (Ventilated)
% Filter
fVenMean = mean(frameData.DataPig6.Ventilated.Block09.HR)/60
type = 'bp';                % lowpass (lp), highpass(hp), bandpass (bp)
character = 'butter';       % Butterworth (butter), Tschebyscheff-I/II (cheby1/cheby2), Cauer (ellip)
order = 4;                  % TP/HP: order, BP: order*2;
[fFilter, venEITFilt] = eit_filter(venEIT, type, character, order, fVenMean, fsEIT);  % filter

% BP. vs. HP
type = 'hp';                % lowpass (lp), highpass(hp), bandpass (bp)
order = 8;
[fFilter, venEITFiltHP] = eit_filter(venEIT, type, character, order, fVenMean, fsEIT);  % filter

%
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(timeVen,venEIT(1:length(venEIT)))
hold on
plot(timeVen,venEITFilt(1:length(venEIT)))
plot(timeVen,venEITFiltHP(1:length(venEIT)),'color',[0 0.5 0])
xlim([0 10])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('Roh','BP-gefiltert', 'HP-gefiltert')

%% Digitale Filterung (Apnea)
% Filter
fApMean = mean(frameData.DataPig6.Apnea.Block09.HR(1:1200))/60
type = 'bp';                % lowpass (lp), highpass(hp), bandpass (bp)
character = 'butter';       % Butterworth (butter), Tschebyscheff-I/II (cheby1/cheby2), Cauer (ellip)
order = 4;                  % TP/HP: order, BP: order*2;
[fFilter, apEITFilt] = eit_filter(apEIT, type, character, order, fApMean, fsEIT);  % filter

%
% figure
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
% plot(timeAp,apEIT(1:length(apEIT)))
% hold on
% plot(timeAp,apEITFilt(1:length(apEIT)))
% xlim([0 20])
% set(gca, 'FontName', 'Times New Roman')
% xlabel('Zeit / s')
% ylabel('Amplitude')
% legend('Roh', 'Gefiltert')

% BP. vs. HP
type = 'hp';                % lowpass (lp), highpass(hp), bandpass (bp)
order = 8;
[fFilter, apEITFiltHP] = eit_filter(apEIT, type, character, order, fVenMean, fsEIT);  % filter

%
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(timeAp,apEIT(1:length(apEIT)))
hold on
plot(timeAp,apEITFilt(1:length(apEIT)))
plot(timeAp,apEITFiltHP(1:length(apEIT)),'color',[0 0.5 0])
xlim([0 10])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('Roh','BP-gefiltert', 'HP-gefiltert')

%% EA (Ventilated)
fsECG = 1000;      % Hz
venR_Peaks = frameData.DataPig6.Ventilated.Block09.R_Peaks;
ECG = frameData.DataPig6.Ventilated.Block09.ECG;
timeECG = (1:length(ECG))/fsECG;

% Interpolieren (EIT: 47.68 Hz -> ECG: 1000 Hz)
[venEITup, timeEITup] = resample(venEIT, timeVen, fsECG);

% plot: Hochgetastetes Signal und EKG
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]);
plot(timeECG,venEITup(1:length(timeECG))/(10^15.5))
hold on
plot(timeECG, ECG)
plot(timeECG(venR_Peaks), ECG(venR_Peaks), 'rx')                 % R-Peaks
xlim([0 10])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('EIT-Signal','EKG','R-Peaks')


% Ensemble Averaging
lim = zeros(1,length(venR_Peaks)-1);
for k = 2:length(venR_Peaks)
    lim(k-1) = venR_Peaks(k)-venR_Peaks(k-1);
end
lim = ceil(mean(lim)/2);

venEAup_ = zeros(length(venR_Peaks)-2,2*lim+1);                        % -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
for k = 2:length(venR_Peaks)-1
    venEAup_(k-1,:) = venEITup((venR_Peaks(k)-lim):((venR_Peaks(k)+lim)));       % Signal bei R-Peaks
end
venEAup = mean(venEAup_);
clear k

% plot: Segmente und EA
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862*0.5]); 
subplot(1,2,1)
plot(timeEITup(1:length(venEAup_)),venEAup_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
xlim([0 timeEITup(length(venEAup_))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(1,2,2)
plot(timeEITup(1:length(venEAup)),venEAup)
xlim([0 timeEITup(length(venEAup_))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')


% plot: EA in Abh. der Anz. der Mittelungen
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.15, 0.4862*9/12]); 
subplot(2,3,1)
plot(venEAup_(1,:))
xlim([0 length(venEAup)])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 1')
subplot(2,3,4)
plot(mean(venEAup_(1:3,:)))
xlim([0 length(venEAup)])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 3')
subplot(2,3,2)
plot(mean(venEAup_(1:5,:)))
xlim([0 length(venEAup)])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 5')
subplot(2,3,5)
plot(mean(venEAup_(1:20,:)))
xlim([0 length(venEAup)])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 20')
subplot(2,3,3)
plot(mean(venEAup_(1:50,:)))
xlim([0 length(venEAup)])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 50')
subplot(2,3,6)
plot(venEAup)
xlim([0 length(venEAup)])
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 272 (max)')


%% EA (Apnoe)
fsECG = 1000;      % Hz
apR_Peaks = frameData.DataPig6.Apnea.Block09.R_Peaks;
ECG = frameData.DataPig6.Apnea.Block09.ECG;
timeECG = (1:length(ECG))/fsECG;

% Interpolieren (EIT: 47.68 Hz -> ECG: 1000 Hz)
[apEITup, timeEITup] = resample(apEIT, timeAp, fsECG);

% plot: Hochgetastetes Signal und EKG
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]);
plot(timeECG,apEITup(1:length(timeECG))/(10^14.7))
hold on
plot(timeECG, ECG)
plot(timeECG(apR_Peaks), ECG(apR_Peaks), 'rx')                 % R-Peaks
xlim([0 10])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('Apnoe-Signal','EKG','R-Peaks')


% Ensemble Averaging
lim = zeros(1,length(apR_Peaks)-1);
for k = 2:length(apR_Peaks)
    lim(k-1) = apR_Peaks(k)-apR_Peaks(k-1);
end
lim = ceil(mean(lim)/2);

apEAup_ = zeros(length(apR_Peaks)-2,2*lim+1);            	% -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
for k = 2:length(apR_Peaks)-1
    apEAup_(k-1,:) = apEITup((apR_Peaks(k)-lim):((apR_Peaks(k)+lim)));       % Signal bei R-Peaks
end
apEAup = mean(apEAup_);
clear k

% plot: Segmente und EA
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862*0.5]); 
subplot(1,2,1)
plot(timeEITup(1:length(apEAup_)),apEAup_')              % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
xlim([0 timeEITup(length(apEAup_))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(1,2,2)
plot(timeEITup(1:length(apEAup_)),apEAup)
xlim([0 timeEITup(length(apEAup_))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')


%% plot: EA in Abh. der Anz. der Mittelungen
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*3/4, 0.4862*3/4]); 
subplot(2,2,1)
plot(apEAup_(1,:))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 1')
xlim([0 length(apEAup_)])
subplot(2,2,3)
plot(mean(apEAup_(1:5,:)))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 5')
xlim([0 length(apEAup_)])
subplot(2,2,2)
plot(mean(apEAup_(1:10,:)))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 20')
xlim([0 length(apEAup_)])
subplot(2,2,4)
plot(apEAup)
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 61 (max)')
xlim([0 length(apEAup_)])


%% Vgl. ventilated und apnea ( Filterung und EA)
figure
plot(timeVen,venEITFilt(1:length(venEIT)))
hold on
plot(timeAp,apEITFilt(1:length(apEIT)))
xlim([0 5])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('Beatmet','Apnoe', 'Location','southeast')

figure
plot(timeEITup(1:length(venEAup)),venEAup)
hold on 
plot(timeEITup(1:length(apEAup)),apEAup)
xlim([0 timeEITup(length(apEAup))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('Beatmet','Apnoe', 'Location','southeast')


%% Vgl. Sig., Filt., EA (ventilated und apnoe)
%ventilated
k = 34
venEITup_ = venEITup(venR_Peaks(k)-lim:venR_Peaks(k)+lim);
[venEITupFilt, timeEITup] = resample(venEITFilt, timeVen, fsECG);
venEITupFilt_ = venEITupFilt(venR_Peaks(k)-lim:venR_Peaks(k)+lim);
[venEITupHPFilt, timeEITup] = resample(venEITFiltHP, timeVen, fsECG);
venEITupHPFilt_ = venEITupHPFilt(venR_Peaks(k)-lim:venR_Peaks(k)+lim);

figure
plot(timeEITup(1:length(venEITup_)),venEITup_-mean(venEITup_))
hold on
plot(timeEITup(1:length(venEITupFilt_)),venEITupFilt_-mean(venEITupFilt_))
plot(timeEITup(1:length(venEITupHPFilt_)),venEITupHPFilt_-mean(venEITupHPFilt_))
plot(timeEITup(1:length(venEAup)),venEAup-mean(venEAup))
xlim([0 timeEITup(length(venEITup_))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('ungefiltert', 'BP-gefiltert', 'HP-gefiltert', 'EA')

% apnoe
apEITup_ = apEITup(apR_Peaks(7)-lim:apR_Peaks(7)+lim);
[apEITupFilt, timeEITup] = resample(apEITFilt, timeAp, fsECG);
apEITupFilt_ = apEITupFilt(apR_Peaks(7)-lim:apR_Peaks(7)+lim);
[apEITupHPFilt, timeEITup] = resample(apEITFiltHP, timeAp, fsECG);
apEITupHPFilt_ = apEITupHPFilt(apR_Peaks(7)-lim:apR_Peaks(7)+lim);

figure
plot(timeEITup(1:length(apEITup_)),apEITup_-mean(apEITup_))
hold on
plot(timeEITup(1:length(apEITup_)),apEITupFilt_-mean(apEITupFilt_))
plot(timeEITup(1:length(apEITup_)),apEITupHPFilt_-mean(apEITupHPFilt_))
plot(timeEITup(1:length(apEAup_)),apEAup-mean(apEAup))
xlim([0 timeEITup(length(apEITup_))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend({'ungefiltert', 'BP-gefiltert', 'HP-gefiltert', 'EA'}, 'Location','southeast')


% %% STFT
% figure
% % stft(xHeart,fsEIT, 'Window',w, 'OverlapLength',220,...
% %     'FFTLength',length(fft(xHeart)))
% stft(apEIT,fsEIT,'Window',kaiser(256,5),'OverlapLength',220,'FFTLength',512);
% ylim([0 2])


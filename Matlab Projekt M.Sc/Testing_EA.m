%% noise sine
fs = 47.68;                                         % sample frequency
N = 14550;                                          % sample points
time = (0:N-1)/fs;                                  % time axis

f = 1;                                              % signal frequency
s = sin(2*pi*f*time);                               % sine
n = wgn(1, N, 0)./10;                               % weak white noise
sn = s+n;                                           % noisy sine

figure
subplot(3,1,1)
plot(time,s)
xlim([0 5])
subplot(3,1,2)
plot(time,n)
xlim([0 5])
subplot(3,1,3)
plot(time,sn)
xlim([0 5])


%% fft
[SN, fAxis] = ffts(sn, N*10, fs);                  	% fft
figure
plot(fAxis,SN(1:length(fAxis))/max(SN))


%% R-Peaks
[pks,locs] = findpeaks(s); clear pks                % Stellen für Peaks lokalisieren
R_Peaks = zeros(1,length(sn));                      % R-Peak-Signal
for k = 1:length(locs)
    R_Peaks(locs(k)) = 1;                           % Füllen mit 1
end

figure
plot(time, sn)
hold on
plot(time, R_Peaks)


%% Ensemble Averaging
clear EA_
lim = zeros(1,length(locs)-1);
for k = 2:length(locs)
    lim(k-1) = locs(k)-locs(k-1);                           % Abstände von Peak zu Peak
end
lim = ceil(mean(lim)/2);                                    % Mittelwert der Abstände, gerundet

for k = 2:length(locs)-1                                    % 2:length(locs)-1 damit der erste und letzte Peak nicht berücksichtigt wird, sollte das Fenster zu groß sein
    EA_(k-1,:) = sn((locs(k)-lim):((locs(k)+lim)));         % Signal bei R-Peaks
end
EA = mean(EA_);                                             % EA

figure
subplot(2,1,1)
plot(EA_')
subplot(2,1,2)
plot(EA)

% EA-Algorhytmus mit 1/N:
% Bsp: Pos 1
% mit Faktor 1/N alle Werte aufsummieren
% dann zu Pos 2
% mit Faktor 1/N alle Werte aufsummieren
% dann zu Pos 3
% ...


%% SNR ??
s_ = s((locs(20)-lim):((locs(20)+lim)));           % Signal bei R-Peak an der Stelle locs(2)
n_ = s_-EA;                                        % Rauschen berechen

snr(s_,n_)                                         % SNR wird nur ausgegeben

figure
plot(s_)
hold on
plot(EA)
plot(n_)


%% SNR in Abh. der Anzahl
clear EA_SNR SNR
for k = 1:length(EA_)
    % 1. EAs berechnen
    if k == 1
        EA_SNR(k,:) = EA_(1:k,:);                   % EA in Abh. der Peak (EA = EA_(1))
    else
        EA_SNR(k,:) = mean(EA_(1:k,:));             % EA in Abh. der Peaks
    end
    % 2. Rauschen schätzen
    n_SNR(k,:) = s_-EA_SNR(k,:);                   % Rauschen für die verschiedenen EAs in Abh. der Peaks              
    % 3. SNR berechnen
    SNR(k) = snr(s_,n_SNR(k,:));                   % SNR in Abh. der Peaks
end

figure
plot(SNR)

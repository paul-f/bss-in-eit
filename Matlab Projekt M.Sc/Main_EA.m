addpath(genpath('C:\Users\paulf\Documents\Studium\Master\2. Semester\Projekt M.Sc - EIT'))

if 0
    
    help init       % Beschreibung der Messdaten in der Funktion
    [frame, frameData] = init();
    clearvars -except frame frameData 
    
end

%% Darstellung eines einzelnen EIT Bildes
figure
imagesc(frame(:,:,620))                             % stelle das 415. Bild dar
axis square


%% Impedanz einzelner Pixel
fs = 47.68;
time = (0:length(frame)-1)/fs;

figure
hold on
plot(time, squeeze(frame(32,20,:)))                 % frame(y,x,sig)
plot(time, squeeze(frame(18,33,:)))
legend('Lung Pixel', 'Heart Pixel')
xlabel('time [s]')
ylabel('Impedance [arb. units]')


%% fft 
fs = 47.68;
xHeart = squeeze(frame(18,33,:));                   % Herz
xHeart = xHeart-mean(xHeart);                       % Mittelwertfrei
N = length(xHeart);
w = window(@blackman,N);                            % window
[XHeart, fAxis] = ffts(xHeart.*w, 10*N*10, fs);
bpmAxis = fAxis*60;

figure
plot(fAxis,XHeart(1:length(fAxis))/max(XHeart))
axis([0 3 -0.03 1.03])
xlabel('Frequency (Hz)')
ylabel('Magnitude |Y(f)|')


%% Sinus ? 
xHeart_ = xHeart(16:95)-xHeart(1);
xHeart_ = xHeart_ - mean(xHeart_);

[M, fMax] = max(XHeart(min(find(fAxis>0.1)):length(fAxis)));
fMax = fAxis(fMax);
a = (max(xHeart_)-min(xHeart_))/2;
s = a*sin(2*pi*(fMax+0.1)*time);                       % sine
d = (max(xHeart_)-max(s));
s = s+d;

figure
plot(time(1:length(xHeart_)), xHeart_)
xlabel('time [s]')
hold on
plot(time(1:length(xHeart_)),s(1:length(xHeart_)))


%% R-Peak erzeugen
[pks,locs] = findpeaks(xHeart,'MinPeakHeight',0.3);     % minimale peakamplitude angeben
% [pks,locs] = findpeaks(xHeart,'MinPeakProminence',0.5);     % minimale peakamplitude angeben

clear pks
R_Peaks = zeros(1,length(xHeart));
for k = 1:length(locs)
    R_Peaks(locs(k)) = 1;
end
clear k

figure
plot(time, xHeart)
hold on
plot(time, R_Peaks)


%% Ensemble Averaging
lim = zeros(1,length(locs)-1);
for k = 2:length(locs)
    lim(k-1) = locs(k)-locs(k-1);
end
lim = ceil(mean(lim)/2);

EA_ = zeros(length(locs)-2,2*lim+1);                        % -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
for k = 2:length(locs)-1
    EA_(k-1,:) = xHeart((locs(k)-lim):((locs(k)+lim)));       % Signal bei R-Peaks
end
EA = mean(EA_);
clear k

figure
subplot(2,1,1)
hold on
plot(EA_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
subplot(2,1,2)
plot(EA)


%% SNR
xHeart_ = zeros(1,2*lim+1);                        % -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
xHeart_ = xHeart((locs(2)-lim):((locs(2)+lim)));       % Signal bei R-Peaks
xHeart_ = xHeart_'-mean(xHeart_'); 
n_ = xHeart_-EA-mean(EA); 

SNR = snr(xHeart_,n_)

figure 
plot(xHeart_)
hold on
plot(EA)
plot(n_)

%% EA über die Pixel ??
pixEA = zeros(64,64,2*lim+1);
for x = 1:64
    for y = 1:64
        pix = squeeze(frame(y,x,:));
        pix = pix-mean(pix);
        EA_ = zeros(length(locs)-2,2*lim+1);                        % -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
        for k = 2:length(locs)-1
            EA_(k-1,:) = pix((locs(k)-lim):((locs(k)+lim)));       % Signal bei R-Peaks
        end
        EA = mean(EA_);
        pixEA(y,x,:) = EA;
    end
end
clear x y


%% Amplitudenverteilung ??
Ampl = zeros(64,64);
for x = 1:64
    for y = 1:64
        Ampl(y,x) =  max(pixEA(y,x,:));
    end
end
clear x y

figure
imagesc(Ampl(:,:))
axis square


%% Phasenverschiebung ??:  maxAmpl finden -> Phase = 0
maxAmpl = 0;
for x = 1:64
    for y = 1:64
        if max(pixEA(y,x,:)) > maxAmpl
            maxAmplx = x;
            maxAmply = y;
            maxAmpl = max(pixEA(y,x,:));
        end
    end
end
clear x y
phase0 = squeeze(pixEA(maxAmply,maxAmplx,:));           % zero phase

Phase = zeros(64,64);
for x = 1:64
    for y = 1:64
        Phase(y,x) =  phdiffmeasure(phase0, squeeze(pixEA(y,x,:)));
    end
end
clear x y

% pixEA(y,x,:)

figure
imagesc(Phase(:,:))
axis square
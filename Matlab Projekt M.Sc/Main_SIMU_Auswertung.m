%% INIT
% addpath(genpath('C:\Users\paulf\Documents\Studium\Master\2. Semester\Projekt M.Sc - EIT'))
clearvars -except frameData
if 0
    help init
    frameData = load('20200716_ETA_50NmHR_12NmBR_reconstructed.mat');
    clearvars -except frameData
end


%% Pixel im Herz und im Linken Lungenflügel
% Festlegen des Signals im Herz
fs = 47.68;     % Hz
rng('default')                              % same random generater
xHeart = squeeze(frameData.data.imgs(20,33,:));
n = wgn(length(xHeart),1, 0)./4;
xHeart = xHeart - mean(xHeart);
xLung = squeeze(frameData.data.imgs(32,18,:));
xLung = xLung-mean(xLung);
time = (0:length(xHeart)-1)/fs;

% plot: Thorax und Signale
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862/2]);
subplot(1,3,1)
imagesc(frameData.data.imgs(:,:,950))       % Regionen
axis square
set(gca, 'FontName', 'Times New Roman')
subplot(1,3,2:3)
plot(time,xLung)
hold on
plot(time,xHeart)
xlim([0 20])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('Pixel aus Lungenregion', 'Pixel aus Herzregion')

%% Digitale Filterung
%
fs = 47.68;
N = length(xLung);
w = window(@hamming,N);                             % window
[XLung, fXAxis] = ffts(xLung.*w, N*10, fs);         % zero-padding and windowing
[XHeart, fXAxis] = ffts(xHeart.*w, N*10, fs);

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(fXAxis,XLung(1:length(fXAxis)))
hold on
plot(fXAxis,XHeart(1:length(fXAxis)))
xlim([0 1])
ylim([-max(XHeart)/20 max(XHeart)+max(XHeart)/20])
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / Hz')
ylabel('Amplitude')
legend('FFT eines Pixels in der Lunge','FFT eines Pixels im Herzen','Location','northwest')

% Filter (Zero-Phase Filter)
type = 'bp';                % lowpass (lp), highpass(hp), bandpass (bp)
character = 'butter';       % Butterworth (butter), Tschebyscheff-I/II (cheby1/cheby2), Cauer (ellip)
order = 4;                  % TP/HP: order, BP: order*2;
[M, fHeart] = max(XHeart(min(find(fXAxis>0.1)):length(fXAxis)));      % Mittenfrequenzen suchen
fHeart = fXAxis(fHeart);
clear M
% fHeart = 72/60; % aus Namen 

[fFilter, yHeart] = eit_filter(xHeart, type, character, order, fHeart, fs);     % filter
[YHeart, fYAxis] = ffts(yHeart.*w,length(yHeart)*10,fs);                        % fft filtered

% plot: FFT (ungefiltert/gefiltert) und gefiltertes Signal
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862*0.5]); 
subplot(1,2,1)
plot(fYAxis,XHeart(1:length(fYAxis)))
hold on
plot(fYAxis,YHeart(1:length(fYAxis)))
xlim([0 1])
ylim([-max(XHeart)/20 max(XHeart)+max(XHeart)/20])
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / Hz')
ylabel('Amplitude')
legend('FFT vor der Filterung','FFT nach Filterung','Location','northwest')
subplot(1,2,2)
plot(time,yHeart)
xlim([0 20])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')

%% Ensemble Averaging
% findpeaks
[pks,locs] = findpeaks(xHeart,'MinPeakProminence',0.03);
clear pks
R_Peaks = zeros(1,length(xHeart));
for k = 1:length(locs)
    R_Peaks(locs(k)) = max(xHeart);
end

% plot: R-Peaks
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(time, xHeart)
hold on
plot(time, R_Peaks)
xlim([0 20])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('EIT-Signal','R-Peaks')


% EA
clear EA_
lim = zeros(1,length(locs)-1);
for k = 2:length(locs)
    lim(k-1) = locs(k)-locs(k-1);
end
lim = ceil(mean(lim)/2);

% EA_ = zeros(length(locs)-2,2*lim+1);                        % -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
for k = 2:length(locs)-1
    EA_(k-1,:) = xHeart((locs(k)-lim):((locs(k)+lim)));       % Signal bei R-Peaks
end
EA = mean(EA_);
clear k

% plot: Segmente und EA
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862*0.5]); 
subplot(1,2,1)
hold on
plot(time(1:length(EA)),EA_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
xlim([0 time(length(EA))])
box on
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(1,2,2)
plot(time(1:length(EA)),EA)
xlim([0 time(length(EA))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')


%% Vgl. Sinus, Filterung und EA
xHeart_ = xHeart(locs(6)-lim:locs(6)+lim);
yHeart_ = yHeart(locs(6)-lim:locs(6)+lim);

% plot: Vergleich von Signal, gefiltertes Signal, EA
figure
plot(time(1:length(xHeart_)),xHeart_-mean(xHeart_))
hold on
plot(time(1:length(xHeart_)),yHeart_-mean(yHeart_))
plot(time(1:length(xHeart_)),EA-mean(EA))
xlim([0 time(length(xHeart_))])
set(gca, 'FontName', 'Times New Roman')
legend('Rohes Signal', ...
    'Gefiltertes Signal', ...
    'Ensemble averaging')
xlabel('Zeit / s')
ylabel('Amplitude')

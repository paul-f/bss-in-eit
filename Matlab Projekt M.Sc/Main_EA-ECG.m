addpath(genpath('C:\Users\paulf\Documents\Studium\Master\2. Semester\Projekt M.Sc - EIT'))

if 0
    
    help init       % Beschreibung der Messdaten in der Funktion
    [frame, frameData] = init();
    globalSig = squeeze(nansum(nansum(frameData.DataPig6.Apnea.Block01.EITApnea2D,2),1));
    clearvars -except frame frameData 
            
end

%% EKG
fsECG = 1000;      % Hz
fsEIT = 47.68;     % Hz

ECG = frameData.DataPig6.Apnea.Block01.ECG;
R_Peaks = frameData.DataPig6.Apnea.Block01.R_Peaks;
timeECG = (1:length(ECG))/fsECG;
globalSig = squeeze(nansum(nansum(frameData.DataPig6.Apnea.Block01.EITApnea2D,2),1));
timeEIT = (1:size(frameData.DataPig6.Apnea.Block01.EITApnea2D,3))/fsEIT;

% ECG = frameData.DataPig6.Ventilated.Block01.ECG;
% R_Peaks = frameData.DataPig6.Ventilated.Block01.R_Peaks;
% time_ECG = (1:length(ECG))/fS_ECG;
% xHeart = squeeze(nansum(nansum(frameData.DataPig6.Ventilated.Block01.EITBreathing2D,2),1));
% time_EIT = (1:size(frameData.DataPig6.Ventilated.Block01.EITBreathing2D,3))/fS_EIT;


figure, hold on
yyaxis left
plot(timeECG, ECG)                                         % EKG-Signal
plot(timeECG(R_Peaks), ECG(R_Peaks), 'rx')                 % R-Peaks
yyaxis right
plot(timeEIT, globalSig)                                      % Herzsignal
xlabel('time [s]')
legend('ECG', 'Detected R-Peaks', 'global EIT')
% xlim([0,7])


%% Runden
% runden
factor = fsEIT/fsECG;
R_Peaks_Round = round(R_Peaks.*factor);

figure, hold on
yyaxis left
plot(timeECG(R_Peaks), max(globalSig)-0.5, 'rx')                 % R-Peaks
yyaxis left
plot(timeEIT(R_Peaks_Round), max(globalSig), 'rx')         % R-Peaks
% plot(time_EIT, globalSig)                             % Herzsignal
xlabel('time [s]')
legend('ECG', 'Detected R-Peaks', 'global EIT')
% xlim([0,7])

% Ensemble Averaging
lim = zeros(1,length(R_Peaks_Round)-1);
for k = 2:length(R_Peaks_Round)
    lim(k-1) = R_Peaks_Round(k)-R_Peaks_Round(k-1);
end
lim = ceil(mean(lim)/2);

EA_ = zeros(length(R_Peaks_Round)-2,2*lim+1);                        % -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
for k = 2:length(R_Peaks_Round)-1
    EA_(k-1,:) = globalSig((R_Peaks_Round(k)-lim):((R_Peaks_Round(k)+lim)));       % Signal bei R-Peaks
end
EA = mean(EA_);
clear k

figure
subplot(2,1,1)
hold on
plot(EA_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
subplot(2,1,2)
plot(EA)

%% Interpolieren/Hochtasten (Lisa)
% fS angleichen (hochtasten): fS_EIT=1000 Hz

% das Resampling kannst du auch einfach mit den Messzeitpunkten 
% und der gewünschten Smapling Rate als Input machen:
%
% time_EIT    = 0:1/imageRate:length(LungSignal)/imageRate-1/imageRate;
% [LungRS, timeRS] = resample(LungSignal, time_EIT, ADIrate);
%
% Über die Länge der EKG und EIT Signale kannst du überprüfen,
% wie groß der zeitliche Unterschied der Signale ist. 
% Er sollte nicht größer als die EIT Sampling Rate sein.

time_EIT_  = 0:1/fsEIT:length(globalSig)/fsEIT-1/fsEIT;
[globalSig_up_, time_EIT_up] = resample(globalSig, time_EIT_, fsECG);

figure
plot(ECG)
hold on
plot(globalSig_up_/(10^17))%-2*10^13)
xlim([0,8000])


% Ensemble Averaging
lim = zeros(1,length(R_Peaks)-1);
for k = 2:length(R_Peaks)
    lim(k-1) = R_Peaks(k)-R_Peaks(k-1);
end
lim = ceil(mean(lim)/2);

EAup_ = zeros(length(R_Peaks)-2,2*lim+1);                        % -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
for k = 2:length(R_Peaks)-1
    EAup_(k-1,:) = globalSig_up_((R_Peaks(k)-lim):((R_Peaks(k)+lim)));       % Signal bei R-Peaks
end
EAup_lis = mean(EAup_);
clear k

figure
subplot(2,1,1)
hold on
plot(EAup_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
subplot(2,1,2)
plot(EAup_lis)


%% Interpolieren/Hochtasten
% fS angleichen (hochtasten): fS_EIT=1000 Hz
[p,q] = rat(fsECG / fsEIT);           % [p,q] = rat(desiredFs / originalFs)
% y = upsample(xHeart,length(ECG)/length(xHeart))
globalSig_up = resample(globalSig,p,q);   % y = resample(x,p,q)
globalSig_up = globalSig_up-mean(globalSig_up);                          % Mittelwertfrei

% das Resampling kannst du auch einfach mit den Messzeitpunkten 
% und der gewünschten Smapling Rate als Input machen:
%
% time_EIT    = 0:1/imageRate:length(LungSignal)/imageRate-1/imageRate;
% [LungRS, timeRS] = resample(LungSignal, time_EIT, ADIrate);
%
% Über die Länge der EKG und EIT Signale kannst du überprüfen,
% wie groß der zeitliche Unterschied der Signale ist. 
% Er sollte nicht größer als die EIT Sampling Rate sein.

time_EIT_  = 0:1/fsEIT:length(globalSig)/fsEIT-1/fsEIT;
[globalSig_up_, time_EIT_up] = resample(globalSig, time_EIT_, fsECG);

figure
plot(ECG)
hold on
plot(globalSig_up/(10^17))%-2*10^13)
xlim([0,8000])


% Ensemble Averaging
lim = zeros(1,length(R_Peaks)-1);
for k = 2:length(R_Peaks)
    lim(k-1) = R_Peaks(k)-R_Peaks(k-1);
end
lim = ceil(mean(lim)/2);

EAup_ = zeros(length(R_Peaks)-2,2*lim+1);                        % -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
for k = 2:length(R_Peaks)-1
    EAup_(k-1,:) = globalSig_up((R_Peaks(k)-lim):((R_Peaks(k)+lim)));       % Signal bei R-Peaks
end
EAup = mean(EAup_);
clear k

figure
subplot(2,1,1)
hold on
plot(EAup_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
subplot(2,1,2)
plot(EAup)


%% Vgl. q-p und Lisa
figure
plot(EAup)
hold on
plot(EAup_lis)

a = EAup == EAup_lis;
find(~a)    % Úntersucht auf 0-en -> ohne Tilde würden die Werte ungleich 0 ausgegeben werden


%% Vergleich von Runden und Hochtasten
time_EA = (0:(length(EA)-1))/fsEIT;
time_EAup = (0:(length(EAup)-1))/fsECG;

figure
plot(time_EA,EA-mean(EA))
hold on
plot(time_EAup,EAup-mean(EAup))
legend('gerundet','interpoliert')


%% SNR (mit mittelwertfreien Signalen/Rauschen)
nEA = EA_(1,:)-mean(EA_(1,:))-EA-mean(EA);      % Rauschen
SNR = snr(EA_(1,:)-mean(EA_(1,:)), nEA)         % 

figure 
plot(EA_(1,:)-mean(EA_(1,:)))
hold on
plot(EA-mean(EA))
plot(nEA)
legend('Herzschlag', 'Ensemble Average', 'Rauschen')

% SNR:  xHeartBeat stellt das gefensterte Signal dar
%       Noise = xHeartBeat - EA

%% Drift der einzelnen Pixel untersuchen
% 1. Trend der einzelnen Pixel berechnen
% 2. Auswertung (Plot)

% INIT
fsEIT = 47.68;
frameDrift = zeros(size(frame,1),size(frame,2),ceil(size(frame,3)/16));
timeEIT = (1:size(frame,3))/fsEIT;

for y = 1:size(frame,2)
    for x = 1:size(frame,1)
        if mean(squeeze(frame(y,x,:))) ~= 0
            pos = x+32*(y-1)
            sig = squeeze(frame(y,x,1:16:end));
            frameDrift(y,x,:) = smooth(timeEIT(1:16:end),sig,0.25,'rloess')';    % trend berechnen
%             [yMin,M] = min(sig);
%             [yMax,M] = max(sig);
        end
    end
end 

% skalierung für plot
yMin = 0;
yMax = 0;
for y = 1:size(frame,2)
    for x = 1:size(frame,1)
        if mean(squeeze(frame(y,x,:))) ~= 0
            sig = squeeze(frameDrift(y,x,:));
            [yMin_,M] = min(sig);
            [yMax_,M] = max(sig);
            if yMin_ < yMin
                yMin = yMin_;
            end
            if yMax_ > yMax
                yMax = yMax_;
            end
            clear M
        end
    end
end 

%% Schleifen trennen (Berechnung und Plot)
figure('Name','Drift');
set(gcf, 'units','normalized','outerposition',[0 0 0.5625 1])
for y = 16:16%size(frame,2)
    for x = 2:2%size(frame,1)
        if mean(squeeze(frame(y,x,:))) ~= 0
            pos = x+32*(y-1)
            subplot(size(frame,1),size(frame,2),pos)        % plot
            plot(timeEIT(1:16:end),squeeze(frameDrift(y,x,:)))
            ylim([yMin yMax])
            set(gca,'XTickLabel',{})
            set(gca,'YTickLabel',{})
        end
    end
end
            
%% Drift in Regionen betrachten
% Regionen
ROI = load('Regions.mat');
frame = frameData.DataPig6.Apnea.Block03.EITApnea2D;

HeartROI = ROI.ROIP06.Heart;
LeftLungROI = ROI.ROIP06.Lung;
for x = 1:32
    for y = 16:32
        if LeftLungROI(x,y) == 1
            LeftLungROI(x,y) = 0;
        end
    end
end
RightLungROI = ROI.ROIP06.Lung;
for x = 1:32
    for y = 1:16
        if RightLungROI(x,y) == 1
            RightLungROI(x,y) = 0;
        end
    end
end
OutROI = ROI.ROIP06.Lung + ROI.ROIP06.Heart;
for x = 1:32
    for y = 1:32
        if OutROI(x,y) == 0
            OutROI(x,y) = 1;
        elseif OutROI(x,y) == 1
            OutROI(x,y) = 0;
        end
    end
end
ThoraxROI = ROI.ROIP06.Lung;
for x = 1:32
    for y = 1:32
        if ThoraxROI(x,y) == 0
            ThoraxROI(x,y) = 1;
        elseif any(isnan(ThoraxROI(x,y)))
            ThoraxROI(x,y) = 0;
        end
    end
end

figure
imagesc(frame(:,:,1000))
axis square
hold on
contour(LeftLungROI,1,'Linewidth',1,'Color',[1 1 1])
contour(RightLungROI,1,'Linewidth',1,'Color',[1 1 1])
contour(HeartROI,1,'Linewidth',1,'Color',[1 1 1])
contour(ThoraxROI,1,'Linewidth',1,'Color',[1 1 1])

driftLeftLungROI = frame.*LeftLungROI;
driftRightLungROI = frame.*RightLungROI;
driftHeartROI = frame.*HeartROI;
driftOutROI = frame.*OutROI;
driftLeftLung = squeeze(nansum(nansum(driftLeftLungROI,2),1));
driftRightLung = squeeze(nansum(nansum(driftRightLungROI,2),1));
driftHeart = squeeze(nansum(nansum(driftHeartROI,2),1));
driftOut = squeeze(nansum(nansum(driftOutROI,2),1));

% trend = smooth(timeEIT,sig,0.25,'rloess')';    % trend berechnen
figure
plot(timeEIT(1:length(driftLeftLung)),driftLeftLung)
hold on
plot(timeEIT(1:length(driftLeftLung)),smooth(timeEIT(1:length(driftLeftLung)),driftLeftLung,0.25,'rloess')','LineWidth',1.5)
plot(timeEIT(1:length(driftLeftLung)),driftRightLung)
plot(timeEIT(1:length(driftLeftLung)),smooth(timeEIT(1:length(driftLeftLung)),driftRightLung,0.25,'rloess')','LineWidth',1.5)
plot(timeEIT(1:length(driftLeftLung)),driftHeart)
plot(timeEIT(1:length(driftLeftLung)),smooth(timeEIT(1:length(driftLeftLung)),driftHeart,0.25,'rloess')','LineWidth',1.5)
plot(timeEIT(1:length(driftLeftLung)),driftOut)
plot(timeEIT(1:length(driftLeftLung)),smooth(timeEIT(1:length(driftLeftLung)),driftOut,0.25,'rloess')','LineWidth',1.5)
legend('linke Lunge', 'Trend linke Lunge', ...
    'rechte Lunge', 'rechte Trend Lunge', ...
    'Herz', 'Trend Herz', ...
    'Außerhalb', 'Außerhalb Trend')

%% Mit und Ohne Atmung vergleichen
frameDataApnea = frameData.DataPig6.Apnea;              % ohne Beatmung
frameDataVentilated = frameData.DataPig6.Ventilated;    % mit Beatmung

Blocks = {'Block01', 'Block02', 'Block03', 'Block04',...
    'Block05', 'Block06', 'Block07', 'Block08', 'Block09', 'Block10'};

testA = squeeze(nansum(nansum(frameData.DataPig6.Apnea.Block04.EITApnea2D,2),1));
testA = testA-mean(testA);  
[TESTA, fAxisA] = ffts(testA, length(testA), fsEIT);
bpmAxis = 60*fAxisA;
testV = squeeze(nansum(nansum(frameData.DataPig6.Ventilated.Block04.EITBreathing2D,2),1));
testV = testV(1:length(testA))-mean(testV(1:length(testA)));  
[TESTV, fAxisV] = ffts(testV, length(testV), fsEIT);
bpmAxis = 60*fAxisV;

figure
plot(bpmAxis, TESTA)
hold on
plot(bpmAxis, TESTV)
xlim([0 200])
legend('Apnea', 'Ventialated')


%% FFT in Zeitdomäne: STFT
globalSig = globalSig - mean(globalSig);
w = window(@blackman,length(globalSig));                            % window

figure
stft(globalSig,fsEIT, 'Window',w, 'OverlapLength',220,...
    'FFTLength',length(fft(globalSig)))
%,'Window',kaiser(256,5),'OverlapLength',220,'FFTLength',512);

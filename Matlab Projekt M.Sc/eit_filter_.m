function [fFilter, y] = eit_filter(x, type, character, order, fHeart, fs)
%
% 1. Bessel
% 2. Butterworth 'butter'
% 3. Tschebyscheff 'cheby1'
% 4. Cauer 'ellip'
% 5. Gauß
%
% fFilter   |--(1)--|--(2)--|--(3)--|
%           |   fC  |   fL  |   fH  |
%
% Alternative:
% d = designfilt('lowpassfir','FilterOrder',nfilt, ...
%                'CutoffFrequency',Fst,'SampleRate',Fs);
           
fC = 0;
fL = 0;
fH = 0;

switch character
    
    case 'butter'               % Butterworth
        switch type
            case 'lp'
                d = designfilt('lowpassiir', ...
                    'PassbandFrequency',fHeart, ...
                    'StopbandFrequency',fHeart+0.1, ...
                    'PassbandRipple',1, ...
                    'StopbandAttenuation',6, ...
                    'SampleRate',fs, ...
                    'DesignMethod','butter');
                disp(filtord(d))
            case 'hp'
                d = designfilt('highpassiir', ...
                    'PassbandFrequency',fHeart, ...
                    'StopbandFrequency',fHeart-0.1, ...
                    'PassbandRipple',1, ...
                    'StopbandAttenuation',6, ...
                    'SampleRate',fs, ...
                    'DesignMethod','butter');
                disp(filtord(d))
            case 'bp'
                d = designfilt('bandpassiir', ...
                    'FilterOrder',order, ...
                    'HalfPowerFrequency1',fHeart-0.5, ...
                    'HalfPowerFrequency2',fHeart+0.5, ...
                    'SampleRate',fs, ...
                    'DesignMethod','butter');
        end
        
    case 'cheby1'               % Tschebyscheff Type I
        switch type
            case 'lp'
                % [b,a] = cheby1(n,Rp,Wp,ftype);
%                 Wp = 2*fHeart/1000;
%                 [b,a] = cheby1(order,10,Wp,'low');
                d = designfilt('lowpassiir', ...
                    'PassbandFrequency',fHeart, ...
                    'StopbandFrequency',fHeart+0.1, ...
                    'PassbandRipple',1, ...
                    'StopbandAttenuation',6, ...
                    'SampleRate',fs, ...
                    'DesignMethod','cheby1');
                disp(filtord(d))
            case 'hp'
%                 [b,a] = cheby1(order,Rp,Wp,'high');
                d = designfilt('highpassiir', ...
                    'PassbandFrequency',fHeart, ...
                    'StopbandFrequency',fHeart-0.1, ...
                    'PassbandRipple',1, ...
                    'StopbandAttenuation',6, ...
                    'SampleRate',fs, ...
                    'DesignMethod','cheby1');
                disp(filtord(d))
            case 'bp'
                % [b,a] = cheby1(order,Rp,Wp,'bandpass');
                d = designfilt('bandpassiir', ...
                    'FilterOrder',order, ...
                    'PassbandFrequency1',fHeart-0.15, ...
                    'PassbandFrequency2',fHeart+0.15, ...
                    'PassbandRipple',3, ...
                    'SampleRate',fs);
        end
        
    case 'cheby2'             % Tschebyscheff Type II
        switch type
            case 'lp'
                cheby2();
            case 'hp'
            case 'bp'
        end
        
    case 'cauer'                % Cauer
        switch type
            case 'lp'
                % [b,a] = ellip(n,Rp,Rs,Wp,ftype);
                [b,a] = ellip(6,5,40,0.6);
                [b,a] = ellip(order,5,40,Wp,'low');
            case 'hp'
            case 'bp'
        end
        
end

% figure % Filter
% freqz(b,a)

fFilter(1,1) = fC;
fFilter(1,2) = fL;
fFilter(1,3) = fH;

fvtool(d);
y = filtfilt(d,x);      % Zero-phase Filtering
% y = filter(b,a,x);

% grpdelay(d,length(x),fs)
% 
% i = 0;
% delay = mean(grpdelay(d))
% 
% tt = tn(1:end-delay);
% sn = xn(1:end-delay);
% 
% sf = xf;
% sf(1:delay) = [];
% 
% plot(tt,sn)
% hold on, plot(tt,sf,'-r','linewidth',1.5), hold off
% title 'Electrocardiogram'
% xlabel('Time (s)'), legend('Original Signal','Filtered Shifted Signal')

end
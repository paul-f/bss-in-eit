function [fFilter, y] = eit_filter(x, type, character, order, fHeart, fS)
%
% 1. Bessel
% 2. Butterworth
% 3. Tschebyscheff
% 4. Cauer
% 5. Gauß
%
% fFilter   |--(1)--|--(2)--|--(3)--|
%           |   fC  |   fL  |   fH  |
%
% Alternative:
% d = designfilt('lowpassfir','FilterOrder',nfilt, ...
%                'CutoffFrequency',Fst,'SampleRate',Fs);
           
fC = 0;
fL = 0;
fH = 0;

switch character
    
    case 'butter'               % Butterworth
        switch type
            case 'lp'
                fC = fHeart+0.1;
                [b,a] = butter(order,fC/(fS/2),'low');
            case 'hp'
                fC = fHeart-0.1;
                [b,a] = butter(order,fC/(fS/2),'high');
            case 'bp'
                fL = fHeart-0.15;
                fH = fHeart+0.15;
                [b,a] = butter(order,[fL,fH]/(fS/2),'bandpass');
        end
        
    case 'cheby1'               % Tschebyscheff Type I - [b,a] = cheby1(n,Rp,Wp,ftype);
        switch type
            case 'lp'
                Wp = 2*fHeart/1000;
                [b,a] = cheby1(order,Rp,Wp,'low');
            case 'hp'
                [b,a] = cheby1(order,Rp,Wp,'high');
            case 'bp'
                fL = fHeart-0.15;
                fH = fHeart+0.15;
                Rp = 3;
                Wp = [fL,fH]/(fS/2);
                [b,a] = cheby1(order,Rp,Wp,'bandpass');
        end
        
    case 'cheby2'             % Tschebyscheff Type II - [b,a] = cheby2(n,Rs,Ws,ftype)
        switch type
            case 'lp'
                cheby2();
            case 'hp'
            case 'bp'
                fL = fHeart-0.15;
                fH = fHeart+0.15;
                Rs = 3;
                Ws = [fL,fH]/(fS/2);
                [b,a] = cheby2(order,Rs,Ws,'bandpass');
        end
        
    case 'cauer'                % Cauer - [b,a] = ellip(n,Rp,Rs,Wp,ftype)
        switch type
            case 'lp'
                [b,a] = ellip(6,5,40,0.6);
                [b,a] = ellip(order,5,40,Wp,'low');
            case 'hp'
            case 'bp'
                fL = fHeart-0.15;
                fH = fHeart+0.15;
                Rp = 3;
                Rs = 40;
                Wp = [fL,fH]/(fS/2);
                [b,a] = ellip(order,Rp,Rs,Wp,'bandpass');
        end
        
end

% figure % Filter
% freqz(b,a)

fFilter(1,1) = fC;
fFilter(1,2) = fL;
fFilter(1,3) = fH;

y = filtfilt(b,a,x);    % Zero-phase Filtering

end
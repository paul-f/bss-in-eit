%% INIT
addpath(genpath('C:\Users\paulf\Documents\Studium\Master\2. Semester\Projekt M.Sc - EIT'))
clearvars -except simpleFrameData constFrameData
if 0
    help init
    simpleFrameData = load('HLI_20200405_PIG07_BO-4_reconstructed.mat');
    constFrameData = load('HLI_20200405_PIG07_BO-6_reconstructed.mat');
    clearvars -except simpleFrameData constFrameData
end


%% Signale
simpEIT = squeeze(simpleFrameData.data.imgs(7,16,:));
simpEIT = simpEIT - mean(simpEIT);
constEIT = squeeze(constFrameData.data.imgs(7,16,:));
constEIT = constEIT(1:length(simpEIT)) - mean(constEIT(1:length(simpEIT)));

fs = 47.68;
time = (0:length(simpEIT)-1)/fs;

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]);
plot(time,simpEIT)
hold on
plot(time,constEIT)
xlim([0 80])
ylim([-2.5 2.5])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('einfach', 'konstant','Location','northwest')


%% Ventilation and Apnoe
% ventilated
ven = 1:1200;
simpEITven = simpEIT(ven);
simpEITven = simpEITven - mean(simpEITven);
constEITven = constEIT(ven);
constEITven = constEITven - mean(constEITven);

% apnoe
ap = 2101:3300;
simpEITap = simpEIT(ap);
simpEITap = simpEITap - mean(simpEITap);
constEITap = constEIT(ap);
constEITap = constEITap - mean(constEITap);

% FFT
% window
N = 1200;                                                   % N = 1200 bei ven und ap
w = window(@hamming,N);                                    % window

%ventilated
[SimpEITven, fAxis] = ffts(simpEITven.*w, N*10, fs);      	% fft simple
[ConstEITven, fAxis] = ffts(constEITven.*w, N*10,fs);       % fft const

% Apnoe
[SimpEITap, fAxis] = ffts(simpEITap.*w, N*10, fs);          % fft simple
[ConstEITap, fAxis] = ffts(constEITap.*w, N*10,fs);         % fft const

% plot: Signale und deren FFT
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862*9/12]); 
subplot(2,2,1)
plot(time(ven),simpEITven)
hold on
plot(time(ven),constEITven)
xlim([0 7.5])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('Beatmet (einfach)','Beatmet (konstant)','Location','southeast')
subplot(2,2,3)
plot(time(ven),simpEITap)
hold on
plot(time(ven),constEITap)
xlim([0 7.5])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('Apnoe (einfach)','Apnoe (konstant)','Location','southeast')
subplot(2,2,2)
plot(fAxis,SimpEITven(1:length(fAxis)))
hold on
plot(fAxis,ConstEITven(1:length(fAxis)))
xlim([0 12])
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / Hz')
ylabel('Amplitude')
legend('FFT Beatmet (einfach)','FFT Beatmet (konstant)')
subplot(2,2,4)
plot(fAxis,SimpEITap(1:length(fAxis)))
hold on
plot(fAxis,ConstEITap(1:length(fAxis)))
xlim([0 12])
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / Hz')
ylabel('Amplitude')
legend('FFT Apnoe (einfach)','FFT Apnoe (konstant)')

%% Digitale Filterung (Apnoe)
% init
type = 'bp';                % lowpass (lp), highpass(hp), bandpass (bp)
character = 'butter';       % Butterworth (butter), Tschebyscheff-I/II (cheby1/cheby2), Cauer (ellip)
order = 4;                  % TP/HP: order, BP: order*2;

% FFT: SimpEITven, ConstEITven, SimpEITap, ConstEITap
% Sig: simpEITven, constEITven, simpEITap, constEITap

% simple
[M, fsimp] = max(SimpEITap);      % Mittenfrequenzen suchen
fsimp = fAxis(fsimp);
clear M
[fFilter, simpEITvenFilt] = eit_filter(simpEITven, type, character, order, fsimp, fs);  % filter
[fFilter, simpEITapFilt] = eit_filter(simpEITap, type, character, order, fsimp, fs);  % filter

% const
[M, fconst] = max(ConstEITap);      % Mittenfrequenzen suchen
fconst = fAxis(fconst);
clear M
[fFilter, constEITvenFilt] = eit_filter(constEITven, type, character, order, fconst, fs);  % filter
[fFilter, constEITapFilt] = eit_filter(constEITap, type, character, order, fconst, fs);  % filter

% % ventilated
% figure
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
% plot(time(ven),simpEITvenFilt)
% hold on
% plot(time(ven),constEITvenFilt)
% xlim([0 10])
% set(gca, 'FontName', 'Times New Roman')
% xlabel('Zeit / s')
% ylabel('Amplitude')
% legend('Gefiltertes Signal Beatmet (simple)', 'Gefiltertes Signal Beatmet (const)')

% apnea (bp)
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(time(ven),simpEITapFilt)
hold on
plot(time(ven),constEITapFilt)
legend('unfiltered', 'filtered')
xlim([0 5])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('BP-gefiltertes Signal (einfach)', 'BP-gefiltertes Signal (konstant)', ...
        'Location','southeast')

% apnea (hp)
type = 'hp';
order = 8;
[fFilter, simpEITapFiltHP] = eit_filter(simpEITap, type, character, order, fsimp, fs);  % filter
[fFilter, constEITapFiltHP] = eit_filter(constEITap, type, character, order, fconst, fs);  % filter


figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(time(ven),simpEITapFiltHP)
hold on
plot(time(ven),constEITapFiltHP)
legend('unfiltered', 'filtered')
xlim([0 5])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('HP-gefiltertes Signal (einfach)', 'HP-gefiltertes Signal (konstant)', ...
    'Location','southeast')


% % simple
% figure
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
% plot(time(ven),simpEITvenFilt)
% hold on
% plot(time(ven),simpEITapFilt)
% legend('unfiltered', 'filtered')
% xlim([0 10])
% set(gca, 'FontName', 'Times New Roman')
% xlabel('Zeit / s')
% ylabel('Amplitude')
% legend('Gefiltertes Signal beatmet (simple)', 'Gefiltertes Signal Apnoe (simple)')
% 
% % const
% figure
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
% plot(time(ven),constEITvenFilt)
% hold on
% plot(time(ven),constEITapFilt)
% legend('unfiltered', 'filtered')
% xlim([0 10])
% set(gca, 'FontName', 'Times New Roman')
% xlabel('Zeit / s')
% ylabel('Amplitude')
% legend('Gefiltertes Signal beatmet (const)', 'Gefiltertes Signal Apnoe (const)')

%% Digitale Filterung (Gesamtsignal)

%% EA (simple ven)
% R-Peaks
clear simpR_Peaksven
[pks,locs] = findpeaks(simpEITven,'MinPeakProminence',0.17);
clear pks
simpR_Peaksven = zeros(1,length(simpEITven));
for k = 1:length(locs)
    simpR_Peaksven(locs(k)) = max(simpEITven);
end

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(time(ven),simpEITven)
hold on
plot(time(ven),simpR_Peaksven)
xlim([0 25])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')


% Ensemble Averaging
lim = zeros(1,length(locs)-1);
for k = 2:length(locs)
    lim(k-1) = locs(k)-locs(k-1);
end
lim = ceil(mean(lim)/2);
lim = 9;
for k = 2:length(locs)-1
    EAsimpEITven_(k-1,:) = simpEITven((locs(k)-lim):((locs(k)+lim)));       % Signal bei R-Peaks
end
EAsimpEITven = mean(EAsimpEITven_);
clear k

% plot: Segmente und EA
figure
subplot(2,1,1)
hold on
plot(time(1:length(EAsimpEITven)),EAsimpEITven_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
xlim([0 time(length(EAsimpEITven))])
box on
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(2,1,2)
plot(time(1:length(EAsimpEITven)),EAsimpEITven)
xlim([0 time(length(EAsimpEITven))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')

% plot: EA in Abh. der Anz. der Mittelungen
figure
% set(gcf, 'units','normalized','outerposition',[0.3123 0 0.374 0.8])
subplot(2,2,1)
plot(EAsimpEITven_(1,:))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 1')
subplot(2,2,3)
plot(mean(EAsimpEITven_(1:3,:)))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 3')
subplot(2,2,2)
plot(mean(EAsimpEITven_(1:5,:)))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 5')
subplot(2,2,4)
plot(EAsimpEITven)
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('N = 63 (max)')


%% EA (simple apnoe)
% R-Peaks
clear R_PeaksAp
[pks,simpLocs] = findpeaks(simpEITap,'MinPeakProminence',0.2);
clear pks
R_PeaksAp = zeros(1,length(simpEITap));
for k = 1:length(simpLocs)
    R_PeaksAp(simpLocs(k)) = max(simpEITap);
end

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(time(ven),simpEITap)
hold on
plot(time(ven),R_PeaksAp)
xlim([0 25])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('EIT-Signal','R-Peaks')

% Ensemble Averaging
lim = zeros(1,length(simpLocs)-1);
for k = 2:length(simpLocs)
    lim(k-1) = simpLocs(k)-simpLocs(k-1);
end
lim = ceil(mean(lim)/2);

for k = 2:length(simpLocs)-1
    EAsimpEITap_(k-1,:) = simpEITap((simpLocs(k)-lim):((simpLocs(k)+lim)));       % Signal bei R-Peaks
end
EAsimpEITap = mean(EAsimpEITap_);
clear k

% plot: Segmente und EA
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862*9/12]); 
subplot(1,2,1)
hold on
plot(time(1:length(EAsimpEITap)),EAsimpEITap_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
xlim([0 time(length(EAsimpEITap))])
box on
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(1,2,2)
plot(time(1:length(EAsimpEITap)),EAsimpEITap)
xlim([0 time(length(EAsimpEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')

% plot: EA in Abh. der Anz. der Mittelungen
figure
hold on
plot(time(1:length(EAsimpEITap)),EAsimpEITap_(1,:))
plot(time(1:length(EAsimpEITap)),mean(EAsimpEITap_(1:3,:)))
plot(time(1:length(EAsimpEITap)),mean(EAsimpEITap_(1:30,:)))
plot(time(1:length(EAsimpEITap)),EAsimpEITap)
box on
xlim([0 time(length(EAsimpEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('N = 1', 'N = 3', 'N = 30', 'N = 71 (max)', ...
    'Location','northwest') 

% % plot: EA in Abh. der Anz. der Mittelungen (old)
% figure
% % set(gcf, 'units','normalized','outerposition',[0.3123 0 0.374 0.8])
% subplot(2,2,1)
% plot(EAsimpEITap_(1,:))
% set(gca, 'FontName', 'Times New Roman')
% set(gca,'XTickLabel',[],'YTickLabel',[]); 
% xlabel('N = 1')
% subplot(2,2,3)
% plot(mean(EAsimpEITap_(1:3,:)))
% set(gca, 'FontName', 'Times New Roman')
% set(gca,'XTickLabel',[],'YTickLabel',[]); 
% xlabel('N = 3')
% subplot(2,2,2)
% plot(mean(EAsimpEITap_(1:5,:)))
% set(gca, 'FontName', 'Times New Roman')
% set(gca,'XTickLabel',[],'YTickLabel',[]); 
% xlabel('N = 5')
% subplot(2,2,4)
% plot(EAsimpEITap)
% set(gca, 'FontName', 'Times New Roman')
% set(gca,'XTickLabel',[],'YTickLabel',[]); 
% xlabel('N = 71 (max)')


%% EA (const ven)
% R-Peaks
clear constR_Peaksven
[pks,locs] = findpeaks(constEITven,'MinPeakProminence',0.17);
clear pks
constR_Peaksven = zeros(1,length(constEITven));
for k = 1:length(locs)
    constR_Peaksven(locs(k)) = max(constEITven);
end

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(constEITven)
hold on
plot(constR_Peaksven)
% xlim([0 20])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')


% Ensemble Averaging
lim = zeros(1,length(locs)-1);
for k = 2:length(locs)
    lim(k-1) = locs(k)-locs(k-1);
end
lim = ceil(mean(lim)/2);
lim = 9;
for k = 2:length(locs)-1
    EAconstEITven_(k-1,:) = constEITven((locs(k)-lim):((locs(k)+lim)));       % Signal bei R-Peaks
end
EAconstEITven = mean(EAconstEITven_);
clear k

% plot: Segmente und EA
figure
subplot(2,1,1)
hold on
plot(time(1:length(EAconstEITven)),EAconstEITven_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
xlim([0 time(length(EAconstEITven))])
box on
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(2,1,2)
plot(time(1:length(EAconstEITven)),EAconstEITven)
xlim([0 time(length(EAconstEITven))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')

% plot: EA in Abh. der Anz. der Mittelungen
figure
% set(gcf, 'units','normalized','outerposition',[0.3123 0 0.374 0.8])
subplot(2,2,1)
plot(EAconstEITven_(1,:))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('Anzahl der Mittelungen: 1')
subplot(2,2,3)
plot(mean(EAconstEITven_(1:3,:)))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('Anzahl der Mittelungen: 3')
subplot(2,2,2)
plot(mean(EAconstEITven_(1:5,:)))
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('Anzahl der Mittelungen: 5')
subplot(2,2,4)
plot(EAconstEITven)
set(gca, 'FontName', 'Times New Roman')
set(gca,'XTickLabel',[],'YTickLabel',[]); 
xlabel('Anzahl der Mittelungen: maximal')


%% EA (const apnoe)
% R-Peaks
clear R_Peaks
[pks,constLocs] = findpeaks(constEITap,'MinPeakProminence',0.3);
clear pks
R_PeaksVen = zeros(1,length(constEITap));
for k = 1:length(constLocs)
    R_PeaksVen(constLocs(k)) = max(constEITap);
end

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/2]); 
plot(time(ven),constEITap)
hold on
plot(time(ven),R_PeaksVen)
xlim([0 25])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('EIT-Signal','R-Peaks')



% Ensemble Averaging
lim = zeros(1,length(constLocs)-1);
for k = 2:length(constLocs)
    lim(k-1) = constLocs(k)-constLocs(k-1);
end
lim = ceil(mean(lim)/2);

lim = 9;
for k = 2:length(constLocs)-1
    EAconstEITap_(k-1,:) = constEITap((constLocs(k)-lim):((constLocs(k)+lim)));       % Signal bei R-Peaks
end
EAconstEITap = mean(EAconstEITap_);
clear k

% plot: Segmente und EA
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862*9/12]); 
subplot(1,2,1)
hold on
plot(time(1:length(EAconstEITap)),EAconstEITap_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
xlim([0 time(length(EAconstEITap))])
box on
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
subplot(1,2,2)
plot(time(1:length(EAconstEITap)),EAconstEITap)
xlim([0 time(length(EAconstEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')

% plot: EA in Abh. der Anz. der Mittelungen
figure
hold on
plot(time(1:length(EAconstEITap)),EAconstEITap_(1,:))
plot(time(1:length(EAconstEITap)),mean(EAconstEITap_(1:3,:)))
plot(time(1:length(EAconstEITap)),mean(EAconstEITap_(1:30,:)))
plot(time(1:length(EAconstEITap)),EAconstEITap)
box on
xlim([0 time(length(EAconstEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('N = 1', 'N = 3', 'N = 30', 'N = 71 (max)', ...
    'Location','northwest') 

% % plot: EA in Abh. der Anz. der Mittelungen (old)
% figure
% % set(gcf, 'units','normalized','outerposition',[0.3123 0 0.374 0.8])
% subplot(2,2,1)
% plot(EAconstEITap_(1,:))
% set(gca, 'FontName', 'Times New Roman')
% set(gca,'XTickLabel',[],'YTickLabel',[]); 
% xlabel('N = 1')
% subplot(2,2,3)
% plot(mean(EAconstEITap_(1:3,:)))
% set(gca, 'FontName', 'Times New Roman')
% set(gca,'XTickLabel',[],'YTickLabel',[]); 
% xlabel('N = 3')
% subplot(2,2,2)
% plot(mean(EAconstEITap_(1:30,:)))
% set(gca, 'FontName', 'Times New Roman')
% set(gca,'XTickLabel',[],'YTickLabel',[]); 
% xlabel('N = 5')
% subplot(2,2,4)
% plot(EAconstEITap)
% set(gca, 'FontName', 'Times New Roman')
% set(gca,'XTickLabel',[],'YTickLabel',[]); 
% xlabel('N = 71 (max)')


%% Vgl. EA ven: simple vs. const
if 0
figure
plot(time(1:length(EAconstEITap)),EAsimpEITven)
hold on
plot(time(1:length(EAconstEITap)),EAconstEITven)
xlim([0 time(length(EAconstEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
end

%% Vgl. Filter: 1. BP (simple vs. const), 2. HP (simple vs. const)
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.5, 0.4862*9/12]); 
subplot(1,2,1)


subplot(1,2,2)

%% Vgl. BP-Filter: simple vs. const 
k = 9;
simpEITapFilt_ = simpEITapFilt(simpLocs(k)-lim:simpLocs(k)+lim);
constEITapFilt_ = constEITapFilt(simpLocs(k)-lim:simpLocs(k)+lim);
%
figure
hold on
plot(time(1:length(simpEITapFilt_)),simpEITapFilt_)
plot(time(1:length(constEITapFilt_)),constEITapFilt_)
xlim([0 time(length(constEITapFilt_))])
box on
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('einfach','konstant')


%% Vgl. HP-Filter: simple vs. const 
k = 9;
simpEITapFiltHP_ = simpEITapFiltHP(simpLocs(k)-lim:simpLocs(k)+lim);
constEITapFiltHP_ = constEITapFiltHP(simpLocs(k)-lim:simpLocs(k)+lim);
%
figure
hold on
plot(time(1:length(simpEITapFiltHP_)),simpEITapFiltHP_)
plot(time(1:length(constEITapFiltHP_)),constEITapFiltHP_)
xlim([0 time(length(constEITapFiltHP_))])
box on
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('einfach','konstant')


%%
plot(timeEITup(1:length(apEITup_)),apEITup_-mean(apEITup_))
hold on
plot(timeEITup(1:length(apEITup_)),apEITupFilt_-mean(apEITupFilt_))
plot(timeEITup(1:length(apEITup_)),apEITupHPFilt_-mean(apEITupHPFilt_))
plot(timeEITup(1:length(apEAup_)),apEAup-mean(apEAup))
xlim([0 timeEITup(length(apEITup_))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend({'ungefiltert', 'BP-gefiltert', 'HP-gefiltert', 'EA'}, 'Location','southeast')

%% Vgl. EA apnoe: simple vs. const
figure
plot(time(1:length(EAconstEITap)),EAsimpEITap)
hold on
plot(time(1:length(EAconstEITap)),EAconstEITap)
xlim([0 time(length(EAconstEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('einfach','konstant')

%% Vgl. EA simple: ven vs. ap
if 0 
figure
plot(time(1:length(EAconstEITap)),EAsimpEITven)
hold on
plot(time(1:length(EAconstEITap)),EAsimpEITap)
xlim([0 time(length(EAconstEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
end
%% Vgl. EA const: ven vs. ap
if 0
figure
plot(time(1:length(EAconstEITap)),EAconstEITven)
hold on
plot(time(1:length(EAconstEITap)),EAconstEITap)
xlim([0 time(length(EAconstEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')

end
%% Vgl. EA: simp(ven) vs. simp(ap) vs. const(ven) vs. const(ap)
if 0
figure
plot(time(1:length(EAconstEITap)),EAsimpEITven)
hold on
plot(time(1:length(EAconstEITap)),EAsimpEITap)
plot(time(1:length(EAconstEITap)),EAconstEITven)
plot(time(1:length(EAconstEITap)),EAconstEITap)
xlim([0 time(length(EAconstEITap))])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s')
ylabel('Amplitude')
legend('EA beatmet (simple)','EA apnoe (simple)','EA beatmet (const)','EA apnoe (const)')
end
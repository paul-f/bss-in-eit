if 0 
clear
load('Beispiel_BeatmeterPatient')   
% Besipielmessung enthaelt Beatmung bei fester Frequenz sowie verschiedene
% Manöver, z.B. angehaltene Atmung nach Ein- oder Ausatmen
end
%% Stelle Geamtimpedanz über Zeit dar
time = (1:length(data.measurement.CompositValue))/data.imageRate;
figure
plot(time, data.measurement.CompositValue)
xlabel('time [s]')
ylabel('Impedance [arb. units]')

%% Darstellung Regionen

figure
imagesc(data.patient.ROI.Inside)
title('Thorax')

figure
imagesc(data.patient.ROI.LeftLung + data.patient.ROI.RightLung)
title('Lung')

figure
imagesc(data.patient.ROI.Heart)
title('Heart')

%% Gesamtimpedanz als Summe über Pixel

globalEIT = squeeze(nansum(nansum(data.measurement.ZeroRef,2),1));

figure
plot(time, globalEIT);

%% Darstellung eines einzelnen EIT Bildes 

figure
imagesc(data.measurement.ZeroRef(:,:,500)) % stelle das 500. Bild dar
axis square
hold on
contour(data.patient.ROI.LeftLung + data.patient.ROI.RightLung,1,'Linewidth',1,'Color',[1 1 1])
contour(data.patient.ROI.Inside,1,'Linewidth',1,'Color',[1 1 1])
contour(data.patient.ROI.Heart,1,'Linewidth',1,'Color',[1 1 1])


%% Impedanz einzelner Pixel

figure
hold on 
plot(time, squeeze(data.measurement.ZeroRef(30,22,:)))
plot(time, squeeze(data.measurement.ZeroRef(20,35,:)))
legend('Lung Pixel', 'Heart Pixel')
xlabel('time [s]')
ylabel('Impedance [arb. units]')


%% FFT
%sig = data.measurement.CompositValue;
sig = squeeze(data.measurement.ZeroRef(30,22,:));  % Lunge
%sig = squeeze(data.measurement.ZeroRef(20,35,:));   % Herz

% zero-padding
sig = sig(4000:9000);
len = length(sig);
multi = 9;
zero_pad = zeros(multi*len,1);
sig_ = [sig; zero_pad];

Fs=44100;
L=length(sig_);
NFFT = 2^nextpow2(L);
f = Fs/2*linspace(0,1,NFFT/2+1);
Y = fft(sig_,NFFT)/L;
psd = 2*abs(Y(1:NFFT/2+1));

figure
plot(f,psd)
xlabel('Frequency (Hz)')
ylabel('Magnitude |Y(f)|')

%% Filter

[A,B,C,D] = butter(10,[50 56]/750);
d = designfilt('bandpassiir','FilterOrder',20, ...
    'HalfPowerFrequency1',50,'HalfPowerFrequency2',56, ...
    'SampleRate',1500);
sos = ss2sos(A,B,C,D);
fvt = fvtool(sos,d,'Fs',1500);
legend(fvt,'butter','designfilt')
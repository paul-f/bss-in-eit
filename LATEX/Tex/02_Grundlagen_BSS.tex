\chapter{Grundlagen der Signaltrennung}
\label{cha:Grundlagen_Sig}
	Wie bereits eingangs in der Einleitung \ref{cha:Einleitung} erwähnt, besteht die Hauptaufgabe dieser Arbeit darin, die einzelnen Signalkomponenten der Ventilation und der Perfusion aus dem gemessenen EIT-Datensatz zu separieren. Dabei ist die größte Herausforderung eine adäquate Trennung der Herzkomponente, da diese im Vergleich zur Ventilation eine relativ kleine Amplitude aufweist und zudem von den Oberwellen der Beatmung im Frequenzbereich überlagert wird \cite[S. 1]{Leonhardt.2012}.
	
	Zur Separation der einzelnen Komponenten wurden unter anderem in der Vergangenheit die Frequenzfilterung und das EA untersucht. Aufgrund ihrer Limitation wurden neue BSS-basierte Methoden entwickelt, die sich dynamsich auf Veränderung anpassen sollen. 	
	Dieses Kapitel beschreibt die nötige Theorie zum Verständnis der einzelnen Trennungsverfahren.	
	
	
	\section{Digitale Filterung}
	\label{sec:FIR}
	Die digitale Filterung im Frequenzbereich stellt eine einfache Methode der Signaltrennung dar. Es liegt der Beobachtung zugrunde, dass sich die Atemfrequenz deutlich von der Herzfrequenz unterscheidet. Denn während ein erwachsener Mensch durchschnittlich 12 bis 16 Atemzüge pro Minute vollzieht, schlägt das Herz hingegen ca. 60 bis 90 Mal die Minute. Sie weisen also theoretisch jeweils ein unterschiedliches Frequenzgehalt auf. Somit liegt es nahe, die Ventilations- und Perfusionskomponentenen der EIT-Signale mithilfe einer Tiefpass- und Hochpassfilterung zu separieren \cite[S. 10]{Frerichs.2017}, wie in Abbildung \ref{fig:3-1_Filter} veranschaulicht.
	
	\begin{figure}[htp]
 	\centering
 	% trim = left bottom right top
 	\includegraphics[trim = 0 50 0 50,clip, width=0.9\textwidth]{Figures/3-1_Filter}
 	\caption[Separation mittels digitaler Filterung]{Prinzip der Separation von Ventilations- und der Perfusionskomponente durch Einsatz einer Hochpass- und Tiefpassfilterung (basierend auf \cite[S. 2]{Leonhardt.2012}, \cite[S. 39]{Rahman.2013})}
 	\label{fig:3-1_Filter}
	\end{figure}
	
	Digitale Filter lassen sich grundlegend in FIR- (engl. finite impulse response, FIR) und IIR-Filter (engl. infinite impulse response, IIR) unterteilen. Im Gegensatz zu IIR-Filtern weisen die FIR-Filter einen linearen Phasenverlauf auf, was zu einer vorhersehbaren und konstanten zeitlichen Verzögerung (Gruppenlaufzeit) führt, wodurch eine Verzerrung des gefilterten Signals vermieden wird. Außerdem gelten FIR-Filter grundsätzlich als stabil, weshalb sie hier bevorzugt betrachtet werden \cite[S. 149]{Kammeyer.2018}, \cite[S. 3]{Deibele.2008}. \\
		%welche sich mittels der Vorwärts- und Rückwärts-Filterung kompensieren lässt. \cite[S. 3]{Deibele.2008} Die Vorwärts- und Rückwärts-Filterung (engl. Zero-phase filtering) beschreibt hierbei einen Fall der \cite{Smith.2021}... WEITER?	
	% Abkürzungen
	\abk{FIR}{engl. finite impulse response, dt. Endliche Impulsantwort} 
	\abk{IIR}{engl. infinite impulse response, dt. Unendliche Impulsantwort} 
	
	
	Nach der Filterung der Signale durch den Einsatz eines geeigneten Hochpasses und eines geeigneten Tiefpasses sind jedoch weiterhin unerwünschte Frequenzanteile in den gefilterten Signalen enthalten. Die Ursache liegt in Frequenzanteilen, die niedriger als die erwartete Atemfrequenz sind und z.B. der von der Hardware einen Grundliniendrift verursachen kann. Hingegen höherfrequente Anteile, oberhalb der Herzfrequenz, können z.B. durch das Vorhandensein anderer störende Geräte oder dem systemisches Rauschen hervorgerufen werden. Abhilfe schafft in der Praxis oftmals der Einsatz von jeweils eines Bandpassfilters für das erwartende Frequenzband der Ventilation und der Perfusion \cite[S. 10]{Frerichs.2017}.\\
	
	Mithilfe von vordefinierten Frequenzen für das Atmungsband und das Perfusionsband lassen sich ohne jegliche Vorbetrachtung des realen Frequenzgehalts bereits erste Ergebnisse erzielen.
	
	Eine Grenze der Methode der Frequenzfilterung stellt das mögliche Vorhandensein von Oberwellen der Atmung dar. Eine adäquate Trennung, vor allem der Perfusionskomponente, ist dann nicht mehr möglich. Oberwellen treten bedingt durch eine konstante Atemfrequenz auf, z.B. durch Einsatz eines Beatmungsgerätes. Es ist dadurch wahrscheinlich, dass die Ventilationskomponente im Frequenzband der Perfusion vorhanden ist, insbesondere dann, wenn die Herzfrequenz einem Vielfachen der Atemfrequenz entspricht \cite[S. 2]{Deibele.2008}.
	
		
	\section{Ensemble averaging}
	\label{sec:EA}
	Eine weitere bewährte Trennungsmethode stellt das Ensemble Averaging (dt. Ensemble-Mittlung, EA) dar. Dabei handelt es sich um die Mittelwertbildung von einzelnen zeitlichen Signalsegmenten eines Gesamtsignals. Die Methode zielt auf die Separierung der Perfusionskomponente durch Unterdrückung der Ventilations- und der Rauschkomponente ab \cite[S. 24]{Braun.2013}.
	Um die EA berechnen zu können, werden sogenannte Trigger (dt. Auslöser) benötigt, die u.a. die zeitliche Länge eines Segmentes, also ein Fenster, was der zeitlichen Dauer eines Herzschlags entspricht, bestimmen sollen. Außerdem geben sie den Zeitpunkt der Mittelwertbildung der Segmente an. Die Trigger werden durch den Einsatz eines Elektrokardiogramms (EKG) erzeugt. Aus einem parallel aufgezeichneten EKG-Signal (R-Welle) lassen sich R-Peaks extrahieren, die einen Zeitpunkt eines Herzschlags markieren. An diesen Stellen wird dann das EA ausgelöst \cite[S. 25]{Braun.2013}.
	
	Mit Hilfe der Gleichung  	
	\begin{align}
	EA(t) = \frac{1}{N} \sum\limits_{i=1}^N EIT(t-T_i) \cdot w(t-T_i)
	\label{eq:EA}
	\end{align}	
	lässt sich das EA-Signal $EA(t)$ eines Herzschlags berechnen\cite[S. 25]{Braun.2013}.
	
	Dabei entspricht $EIT(t)$ dem ursprünglichen EIT-Signal, $T$ einem Vektor, der alle Triggerzeitpunkte enthält, und $w(t)$ ein Rechteckfenster mit der Länge eines Herzschlags, in dem die einzelnen Triggerzeitpunkte zentriert ist. Aus der Überlagerung der durch die Fensterung entstandenen Segmente lässt sich das EA berechnen. Durch das EA lässt sich dann eine durchschnittliche Periode des Herzschlags darstellen \cite[S. 25]{Braun.2013}.
	
	Die Abbildung \ref{fig:3-2_EA} stellt die Methode des EA graphisch dar. Das Beispielsignal wurde mithilfe der Methode der Impedanzkardiographie (IKG), ein Verfahren zur Bestimmung von Herzparametern wie  der Herzfrequenz oder dem Schlagvolumen aufgezeichnet \cite[S. 149]{Li.2000}.
			
	\begin{figure}[h]
 	\centering
 	\includegraphics[trim = 10 50 0 45,clip, width=0.9\textwidth]{3-2_EA}
 	\caption[Prinzip vom Ensemble Averaging]{Prinzipielle Beschreibung des Vorgehens vom Anwenden des Ensemble Averaging auf ein IKG-Signals (IKG) (basierend auf \cite[S. 80]{SolaICarosJosepMaria.2011})}
 	\label{fig:3-2_EA}
	\end{figure}
	
	Die Grenze des Verfahrens wird erreicht, wenn eine stark variierende Herzfrequenz vorliegt. Durch die unterschiedliche zeitliche Dauer eines Herzschlags, lässt sich kein geeignetes Rechteckfenster festlegen, was der zeitlichen Dauer eines Herzschlags entspricht. Es erfasst dann keine vollständige Periode oder bereits Teile einer neuen Periode, was somit zur Verfälschung des Endergebnis führt \cite[S. 25]{Braun.2013}. Außerdem muss eine ausreichend große Anzahl an Mittelwertbildungen erfolgen, damit die unerwünschten Signalkomponenten der Ventilation und des Rauschens ausreichend kompensiert werden können \cite[S. 2]{Deibele.2008}. 
	
	% Abkürzungen
	\abk{EA}{engl. Ensemble Averaging, dt. Ensemble-Mittlung}
	\abk{EKG}{Elektrokardiogramm}
	\abk{IKG}{Impedanzkardiographie} 
	
	\section{Blind Source Separation}
	\label{sec:BSS}
	Die zuvor beschriebenen Möglichkeiten zur Trennung der Signalkomponenten stellen die Daten im Zeit- und Frequenzbereich dar. Es existieren aber auch Ansätze, die zur Darstellung der Daten im statistischen Bereich dienen. Die für diese Arbeit relevanten Verfahren sind die Hauptkomponentenanalyse (PCA) und die Unabhängigkeitsanalyse (ICA). 
	
	Beide Ansätze projizieren die Daten von einen Hauptraum in einen Unterraum, der ein bestimmtes statistisches Kriterium erfüllt, das Unabhängigkeit impliziert \cite[S. 1]{Clifford.2008}, \cite{Rahman.2013}. Dadurch lässt sich eine Datei in ihre separaten Komponenten bzw. Quellen zerlegen, aus denen sich wichtige Strukturen ableiten lassen. Während man bei der Frequenzfilterung davon ausgehen muss, dass die Frequenzspektren der einzelnen Signalkomponenten voneinander unabhängig sind, weist man bei der Verwendung der PCA und der ICA den Signalkomponenten keine speziellen Eigenschaften, wie z.B. die Energie bei einer bestimmten Frequenz, zu. Darum ist es möglich, trotz überlappender Frequenzen der Signalkomponenten eine exakte Quellentrennung durchzuführen. Somit eignen sich die PCA und die ICA für die Lösung des Problems der Blind Source Separation (dt. Blinde Quellentrennung, BSS) \cite[S. 1f.]{Clifford.2008}.
	
	
	Unter der Blind Source Separation versteht man im Allgemeinen die Separation einzelner unbekannter, voneinander unabhängiger Quellen (Source Separation) aus beobachteten bzw. aufgezeichneten Signalen. Dabei verfügt man weder über Informationen zum zeitlichen Verlauf noch inwieweit die einzelnen unabhängigen Quellen zueinander gewichtet sind (Blind). Die Aufgabe der BSS ist es also, statistisch unabhängige Signalkomponenten zu identifizieren, die den unbeobachteten Quellsignalen entsprechen \cite[S. 89]{Kohler.2005}. 
	
	\begin{figure}[h]
 	\centering
 	% trim = left bottom right top
 	\includegraphics[trim = 0cm 1cm 0cm 2cm,clip, width=0.9\textwidth]{3-3_BSS}
 	\caption[Prinzip der BSS]{Prinzip der BSS (basierend auf \cite[S. 89]{Kohler.2005})}
 	\label{fig:3-3_BSS}
	\end{figure}
	
	Die Abbildung \ref{fig:3-3_BSS} stellt diesen Zusammenhang als lineares Mischmodell dar. Dabei beschreibt die Matrix $\boldsymbol{S} = (\boldsymbol{s}_1 \cdots \boldsymbol{s}_m)^T$ unabhängigen, unbeobachteten Quellsignale. Aus der linearen Kombination mit der Mischmatrix $\boldsymbol{A}$ resultiert die beobachtete Datenmatrix $\boldsymbol{X} = (\boldsymbol{x}_1 \cdots \boldsymbol{x}_m)^T$. Mithilfe einer geeigneten Separationsmatrix $\boldsymbol{W}$ (ideal: $\boldsymbol{W} = \boldsymbol{A}^{-1}$ \cite[S. 95]{Kohler.2005}) lassen sich aus der Datenmatrix beobachten $\boldsymbol{X}$ die gesuchen unabhängigen Quellsignale $\hat{\boldsymbol{S}} = (\hat{\boldsymbol{s}}_1 \cdots \hat{\boldsymbol{s}}_m)^T$ rekonstruieren \cite[S. 89f.]{Kohler.2005}.\\
	
	Auch EIT-Datensätze stellen eine Linearkombination von Quellsignalen dar \cite[S. 39]{Rahman.2013}, wodurch ebenfalls das Modell aus Abbildung  \ref{fig:3-3_BSS} herangezogen werden kann. Ein solcher EIT-Datensatz $\boldsymbol{X}$ (siehe Abschnitt \ref{subsec:messverfahren}) wird durch die $M\times N$-dimensionale Matrix
	
	\begin{align}
	\underset{M\times N}{\mathrm{\boldsymbol{X}}}
	=
	\begin{pmatrix}
   	x_{11} & \cdots & x_{1N} \\
   	\vdots & \ddots & \vdots \\
   	x_{M1} & \cdots & x_{MN} \\
   	\end{pmatrix}
	\end{align}
	
	beschrieben. Dabei entsprechen die $M$ Zeilen den unabhängigen Variablen (d.h. den einzelnen Elektrodenmesskanäle) und die $N$ Spalten den einzelnen zeitlichen Beobachtungen (d.h. die aufeinanderfolgenden Rahmen) \cite[S. 4]{Deibele.2008}. 
	
	Durch das Finden einer geeigneten Separationsmatrix $\boldsymbol{W}$ unter Anwendung von der PCA und/oder der ICA lassen sich Näherungen der unabhängigen Quellen $\hat{\boldsymbol{S}}$ rekonstruieren \cite[S. 95]{Kohler.2005}. In Bezug auf die EIT stellen die unabhängigen Quelle den zu erwartenden zeitlichen Verlauf der einzelnen Signalkomponenten (Ventilation, Perfusion, Rauschen) dar und werden als Schablonenfunktion (engl. Template functions) bezeichnet. Diese Funktionen lassen sich jeweils mithilfe der adaptiven Zeitbereichsfilterung in den beobachtete EIT-Datensatz der Spannungsmessungen einpassen, woraus sich dann die EIT-Bilder für die einzelnen Signalkomponenten rekonstruieren lassen. \cite[S. 3]{Deibele.2008}\\
	%\cite[S. 2]{Cardoso.1998} $\rightarrow$ [Und Entfernen der ungewollten Komponenten wie Rauschen!!!]
	
	Zunächst werden im folgenden Abschnitt die beiden statistischen Verfahren der PCA und der ICA beschrieben, woraus die geeignete Schablonenfunktionen gewonnen werden. Anschließend wird die Anwendung der adaptiven Zeitbereichsfilterung erläutert.
	
	% Abkürzung
	\abk{BSS}{engl. Blind Source Separation, dt. Blinde Quellentrennung} 
	\abk{PCA}{engl. Principal Component Analysis, dt. Hauptkomponentenanalyse} 
	\abk{ICA}{engl. Independent Component Analysis, dt. Unabhängigkeitsanalyse} 

	
	\subsection{Die Hauptkomponentenanalyse}
	\label{subsec:PCA}
	Die Hauptkomponentenanalyse ist eines der bekanntesten und verbreitetsten Verfahren der multivarianten Statistik \cite[S. 1094]{Lovric.2011}. Sie dient der Extraktion wichtiger statistischer Informationen und der Reduzierung der Dimensionalität von großen Datensätzen unter Beibehaltung dieser Informationen \cite[S. 434]{Abdi.2010}. Dafür wird mittels der PCA ein Datensatz, bestehend aus einer großen Anzahl untereinander korrelierter Variablen, in einen neuen Satz von unkorrelierten Variablen (\glqq Hauptkomponenten\grqq) überführt \cite[S. 1]{Jolliffe.2002}. Die Hauptkomponenten werden nach absteigender Varianz sortiert. Im Bereich der digitalen Signalverarbeitung besteht ein solcher Datensatz aus Sätzen von durchgeführten Zeitabtastungen an einem Objekt \cite[S. 1]{Castells.2007}. Aufgrund der Unkorreliertheit eignen sich die erzeugten Hauptkomponenten zur Identifikation und Trennung unabhängiger Quellsignalen (BSS) und entsprechen den gesuchten Schablonenfunktionen \cite[S. 1022]{Raine.2004}, \cite[S. 15]{Castells.2007}.\\
	
	Um die Hauptkomponenten zu finden, benötigt es eine geeignete Separationsmatrix $\boldsymbol{W} = (\boldsymbol{w}_1 \cdots \boldsymbol{w}_M)$ -- im Bereich der PCA spricht man von der Koeffizientenmatrix $\boldsymbol{W}$, bei der die Spalten $\boldsymbol{w_m}$ die Koeffizientenvektoren der Hauptkomponenten mit absteigender Varianz bilden \cite[S. 4]{Deibele.2008}.
	
	Im Prinzip lässt sich die Berechnung des ersten Koeffizientenvektors $\boldsymbol{w_1}$ als Maximierung der Varianz
	\begin{equation}
		\boldsymbol{w_1} = \text{arg max}_{ \lVert \boldsymbol{w} \rVert = 1} E \{ (\boldsymbol{w}_1^T \boldsymbol{X})^2\}
	\end{equation} der ersten Hauptkomponente beschreiben \cite[S. 8]{Clifford.2008}, \cite[S. 2]{Castells.2007}. Unter der Annahme, dass die Zeilen von $\boldsymbol{X}$ (Elektrodenmesskanäle) zentriert (d.h. mittelwertfrei) sind \cite[S. 3]{Wall.2003}, entspricht $\boldsymbol{w_1}$ dem Eigenvektor des größten Eigenwerts der Kovarianzmatrix $\boldsymbol{C} = \boldsymbol{X} \boldsymbol{X}^T$ \cite[S. 1094]{Lovric.2011}. Die weiteren $M-1$ Koeffizientenvektoren $\boldsymbol{w_2}, \ldots, \boldsymbol{w_M}$  werden durch Wiederholung des Vorgangs im jeweils verbleibenden Unterraum \cite[S. 8]{Clifford.2008} für die nächst kleinere Varianz bzw. des nächst kleineren Eigenwerts erzeugt \cite[S. 2]{Castells.2007}. Dabei verringert sich Dimensionalität des Unterraum für jede gefundene Komponente um eine Dimension \cite[S. 8]{Clifford.2008}.\\
	
		In der Praxis lassen sich die Eigenvektoren von der Kovarianzmatrix $\boldsymbol{C}$ mithilfe der Singulärwertzerlegung (engl. singular value decomposition, SVD) berechnen. Dabei wird in Datenmatrix $\boldsymbol{X}$ in 
	
	\begin{align}
	\underset{M\times N}{\mathrm{\boldsymbol{X}}}
	=
	\boldsymbol{U} \boldsymbol{\Sigma} \boldsymbol{V}^T
	= 
	\begin{pmatrix} \boldsymbol{u}_1 & \cdots & \boldsymbol{u}_M \end{pmatrix}
   	\begin{pmatrix}
   	\sigma_1 & \cdots 	& \cdots &\cdots & 0 \\
   	\vdots 	& \ddots 	& 			&			& \vdots\\
   	0 	& \cdots 		& \sigma_M & \cdots 	& 0 \\
   	\end{pmatrix}
	\begin{pmatrix} \boldsymbol{v}_1 & \cdots & \boldsymbol{v}_N \end{pmatrix}
	\text{.}
	\end{align}
	
	\noindent zerlegt. Die Matrix $\boldsymbol{U}$ ergibt dabei eine $M\times M$-Orthogonalmatrix, dessen Spaltenvektoren $\boldsymbol{u}_n$ als Links-Singulärvektoren bezeichnet werden. Analog beschreibt die Matrix $\boldsymbol{V}$ eine $N\times N$-Orthogonalmatrix mit den Rechts-Singulärvektoren als Spaltenvektoren. Die Matrix $\boldsymbol{\Sigma}$ ist eine $M\times N$ positive Diagonalmatrix, die die Singulärwerte $\sigma_1, \ldots , \sigma_M$ enthält \cite[S. 5]{Castells.2007}. Die Matrix $\boldsymbol{U}$ entspricht hierbei den gesuchten Eigenvektoren der Kovarianzmatrix $\boldsymbol{C}$ mit absteigender Varianz \cite[S. 8]{Clifford.2008}.\\
		
	Anhand der so bestimmten Koeffizientenmatrix bzw. Separationsmatrix $\boldsymbol{W}$ lassen sich die gesuchten Hauptkomponenten
	
	\begin{align}
	\hat{\boldsymbol{S}} = \boldsymbol{W}^T \boldsymbol{X}
	\end{align}
	
	mit absteigender Varianz berechnen \cite[S. 2]{Castells.2007}, die später dann als Schablonenfunktionen der unabhängigen Quellsignale verwendet werden. \cite[S. 4]{Deibele.2008}.
	
%	Literatur:
%	\begin{itemize}
%	\item \cite[S. 4]{Castells.2007}
%	\item \cite[S. 2-4]{Castells.2007} $\rightarrow$ Herleitung
%	\item \cite[S. 2]{Jang.2020}
%	\item \cite[S. 44]{Jolliffe.2002}
%	\item \cite[S. 3]{Wodecki.2019}
%	\item \cite[S. 4]{Deibele.2008}
%	\end{itemize}
	
	\subsection{Die Unabhängigkeitsanalyse}
	\label{subsec:ICA}
	Wie auch die Hauptkomponentenanalyse handelt es sich bei der Unabhängigkeitsanalyse um ein Verfahren der multivarianten Statistik \cite[S. 1]{Hyvarinen.2001}. Ihr Ziel ist die Zerlegung von Datensätzen in ihre statistisch unabhängigen Komponenten (ICs). Dabei handelt es sich bei den Datensätzen um eine Linearkombination aus unbeobachteten Quellen \cite[S. 38]{Rahman.2013}. Aufgrund der statistischen Unabhängigkeit der Komponenten eigenen sie sich zur Identifikation und Trennung unabhängiger Quellsignalen (BSS) und werden als Schablonenfunktionen verwendet \cite[S. 39]{Rahman.2013}.
	
	Während die PCA einen Unterraum auf Basis der Varianz (Moment zweiten Ordnung) erstellt, erstellt die ICA hingegen einen Raum auf Basis der Kumulanten (Moment vierten Ordnung). Der daraus neu gewonnene Datensatz erfüllt dann nicht nur, wie im Falle der PCA, die Unkorreliertheit, sondern auch die Unabhängigkeit, welches die stärkere beider statistischen Eigenschaften ist. Somit sollten sich die Quellen unter Verwendung der ICA exakter separieren lassen, als unter Anwendung der PCA. \cite[S. 7]{Hyvarinen.2001} \cite[S. 16f.]{Clifford.2008}\\
	%Während die PCA aus einem großen Datzensatz einen neuen Satz von unkorrelierten Variablen bzw. Hauptkomponenten (PCs) schafft, schafft die ICA hingegen einen neuen Datensatz bestehend aus unabhängigen Variablen bzw. Komponenten (ICs). Dies liegt der Tatsache zugrunde, dass die PCA die einen Unterraum auf Basis der Varianz (Moment zweiter Ordnung) verwendet, um die Quellsignale zu trennen, während hingegen die ICA einen Raum auf Basis der Kumulanten (Moment vierter Ordnung) verwendet. Während bei der PCA die Unabhängigkeit zwischen den Projektionen durch die Orthogonalität der Eigenvektoren erzwungen wird (Unkorreliertheit), ist der durch die ICA gebildete Unterraum nicht unbedingt orthogonal. \cite[S. 16f.]{Clifford.2008} Die Unabhängigkeit ist dabei eine stärkere statistische Eigenschaft, als die Unkorreliertheit. Darum lassen sich theoretisch die Quellen unter Verwenden der ICA exakter trennen, als mit der PCA. \cite[S. 7]{Hyvarinen.2001} \cite[S. 17]{Clifford.2008}
	
	Ausgehend von dem Modell der Blind Source Separation (Abbildung \ref{fig:3-3_BSS}) muss für die Separierung der unabhängigen Quellsignale $\hat{\boldsymbol{S}}$ abermals eine geeignete Separationsmatrix $\boldsymbol{W}$ gefunden werden \cite[S. 89]{Kohler.2005}. Dafür wurden mehrere Algorithmen, wie z.B. Infomax \cite{Bell.1995}, FastICA \cite{Hyvarinen.1999}, Natural Gradient ICA \cite{Amari.1998} und JADE (engl. Joint Approximation Diagonalization of Eigen-matrices) \cite{Cardoso.1993}, auf Basis der ICA entwickelt. 
	
	In der vorliegenden Arbeit konnten die besten Ergebnisse mit dem JADE-Algorithmus erzielt werden. Dieser verfolgt einen statistischen Ansatz zur Berechnung der ICA. Der JADE-Algorithmus lässt sich durch die folgende vier Schritten beschreiben \cite[S. 175]{Cardoso.1999}: 
%(liegt evlt. auch hier dran: \cite[S. 193]{Roberts.2014}?). 
	
	\begin{enumerate}
	\item Vorverarbeitung: Bestimmen der Whitening-Matrix $\hat{\boldsymbol{V}}$ und Berechnen der unkorrelierten Matrix $\boldsymbol{Z} = \hat{\boldsymbol{V}} \boldsymbol{X}$
	\item Statistik: Berechnung der größtmöglichen Menge an Kumulantenmatrizen vierten Ordnung $\{ \hat{\boldsymbol{Q}}_i^Z \}$
	\item Diagonalisierung: Bestimmen der Rotationsmatrix $\hat{\boldsymbol{R}}$, sodass die Kumulantenmatrizen möglichst diagonal sind
	\item Separation: Bestimmen der Mischmatrix $\hat{\boldsymbol{A}} = \hat{\boldsymbol{R}} \hat{\boldsymbol{V}}^{-1}$ und berechnen der gesuchten Quellmatrix $\hat{\boldsymbol{S}} = \hat{\boldsymbol{A}}^{-1} \boldsymbol{X}$ 
	\end{enumerate}
	
	Der erste Schritt besteht aus dem Prozess des \glqq Whitenings\grqq. Whitening beschreibt dabei das Dekorrelieren und das Reduzieren der Dimension der Datenmatrix $\boldsymbol{X}$. Dies lässt sich mit der vorher beschriebenen Methode der PCA realisieren. Um bereits im Vorfeld einen großen Teil der Rauschkomponente zu eliminieren, werden von der ICA nur die Hauptkomponenten verwendet, dessen Varianz ausreichend groß ist. Daraus folgt, dass die Dimensinalität der Matrix $\boldsymbol{Z}$ (Hauptkomponenten) kleiner ist als die der ursprünglichen Datenmatrix $\boldsymbol{X}$. \cite[S. 8974]{Ebmeier.2016}
	
	Im zweite Schritt wird aus der bereits unkorrelierten Matrix $\boldsymbol{Z}$ ein Tensor (Zusammenfassung von Matrizen) \cite[S. 26]{Rutledge.2013}, der die größtmögliche Menge an Kumulantenmatrizen (Kumulanten vierter Ordnung) $\{ \hat{\boldsymbol{Q}}_i^Z \}$ enthält, gewonnen \cite[S. 171 u. 175]{Cardoso.1999}. Kumulanten weisen eine einfache Struktur auf, weswegen sie sich für einfache Identifikationstechniken eignen \cite[S. 168]{Cardoso.1999}. Für jedes unkorrelierte Signal von $\boldsymbol{Z}$ werden die Kumulanten der Signale mit sich selbst (Autokumulanten) sowie die Kumulanten aller Signalkombinationen (Kreuzkumulanten) berechnet \cite[S. 26]{Rutledge.2013}. Die Signalvektoren gelten als unabhängig, wenn die Kreuzkumulanten vierter Ordnung Null ergeben und ihre Autokumulationen maximal sind \cite[S. 26]{Rutledge.2013}.
	
	Der dritte Schritt beschreibt die Diagonalisierung der Kumulantenmatrizen $\hat{\boldsymbol{Q}}_i^Z$ mithilfe einer Rotationsmatrix $\hat{\boldsymbol{R}}$ \cite[S. 369]{Wang.2008}. Die Rotationsmatrix $\hat{\boldsymbol{R}}$ kann mittels des erweiterten Jacobi-Verfahrens gefunden werden \cite[S. 8]{Cardoso.1993b}. 
	
	Im vierten Schritt wird die Mischmatrix $\hat{\boldsymbol{A}} = \hat{\boldsymbol{R}} \hat{\boldsymbol{V}}$ berechnet. Die Invertierung der Mischmatrix ergibt dann die gesuchte Separationsmatrix $\hat{\boldsymbol{W}} = \hat{\boldsymbol{A}}^{-1}$, welche für die Berechnung der unabhängigen Quellen $\hat{\boldsymbol{S}}$ benötigt wird \cite[S. 175]{Cardoso.1999}.\\
	
	Die unter Anwendung von der ICA erzeugten unabhängigen Quellen bzw. Komponenten
	\begin{align}
	\hat{\boldsymbol{S}} = \hat{\boldsymbol{W}} \boldsymbol{X}
	\end{align}
	entsprechen den Schablonenfunktionen, die dann zur Separation des Ventilations- und des Perfusionssignals eingesetzt werden.

%    In Schritt 4 muss die Pseudo-Inverse von W nicht explizit berechnet werden: Die Eigenzersetzung von R kann von W recycelt werden. \cite[S. 8f.]{Cardoso.1993b}
	
	%%%%%%%%
%	Literatur:
%	\begin{itemize}
%	\item \cite[S. 175]{Cardoso.1999}
%	\item \cite[S. 369]{Wang.2008}
%	\item \cite[S. 8f.]{Cardoso.1993b} $\rightarrow$ genau beschrieben
%	\end{itemize}
	
	
	\subsection{Adaptive Zeitbereichsfilterung}
	\label{subsec:Templates}
	Für die Separation der Ventilations- und der Perfusionskomponente mittels der erstellten Schablonenfunktionen wird auf der EIT-Datenmatrix $\boldsymbol{X}$ eine adaptive Filterung im Zeitbereich durchgeführt \cite[S. 3]{Deibele.2008}. Die adaptive Filterung biete den Vorteil, dass sie keine speziellen, unveränderlichen Eigenschaften des Signals, wie z.B. die Energie bei einer bestimmten Frequenz, annimmt. Dadurch kann diese Methode dynamisch auf Signaländerungen reagieren \cite[S. 236]{Kammeyer.2018}.
	
	Die Zeitbereichsfilterung wird mit einem Least-Mean-Square-Algorithmus (LMS) durchgeführt \cite[S. 237]{Kammeyer.2018}. Der Algorithmus löst ein Gleichungssystem in Anlehnung an die Methode der kleinsten Fehlerquadrate (Least-Squares-Lösung) \cite[S. 253]{Kammeyer.2018} und findet auf diese Weise eine Ausgleichskurve, die sich optimal an die gegebenen Messpunkte anpasst \cite[S. 312]{Papula.2017}.\\ 
	\abk{LMS}{Least-Mean-Square-Algorithmus} 
	
	Zur Beschreibung der dazu nötigen Rechenschritte wird zunächst ein einzelner Vektor $\boldsymbol{x}(t)$, entsprechend eines einzelnen EIT-Messkanals, betrachtet. Dabei wird angenommen, dass in dem Zeitintervall $[ t_1 ; t_N]$ das beobachtete Signal $\boldsymbol{x}(t) = (x(t_1), \ldots, x(t_1)) = (x_1, \ldots, x_N)$ ausreichend durch das Signal $\bar{\boldsymbol{x}}(t)$ angenähert werden kann. $\bar{\boldsymbol{x}}(t)$ stellt hierfür eine Linearkombination mit $K < M$ linear unabhängigen Schablonenfunktionen $\hat{\boldsymbol{s}}_k (t)$ dar. Der Zusammenhang lasst sich als das Gleichungssystem 
	
	\begin{align}
	\boldsymbol{x} = \boldsymbol{a} \hat{\boldsymbol{S}}
   	\Leftrightarrow
 	\begin{pmatrix} x_1 & \cdots & x_N \end{pmatrix}
 	=
   	\begin{pmatrix} a_1 & \cdots & a_K \end{pmatrix}
 	\cdot
 	\begin{pmatrix}
   	\hat{s}_{11} & \cdots & \hat{s}_{1N} \\
   	\vdots & \ddots & \vdots \\
   	\hat{s}_{K1} & \cdots & \hat{s}_{KN} \\
   	\end{pmatrix}	
   	  	\label{eq:linKom}
	\end{align}
	
	beschreiben, wobei die $K$ Zeilen der Schablonenmatrix $\hat{\boldsymbol{S}}$ beschreiben jeweils einer Schablonenfunktion entspricht \cite[S. 3]{Deibele.2008}. Der unbekannte Vektor der Gewichte $\boldsymbol{a}$ lässt sich mittels der Moore-Penrose-Inverse (Pseudoinverse) $\hat{\boldsymbol{S}}^{+}$ berechnen zur Bestimmung der Least-Squares-Lösung \cite[S. 253f.]{Kammeyer.2018}, \cite[S. 3]{Deibele.2008} 
	
	\begin{align}
	\boldsymbol{a} = \boldsymbol{x} \hat{\boldsymbol{S}}^{+}
	\Leftrightarrow
	\boldsymbol{a} = \text{arg min}_{\tilde{\boldsymbol{a}}} ( \lVert \tilde{\boldsymbol{a}} \hat{\boldsymbol{S}} - \boldsymbol{x} \rVert_2  )
	\enspace .
	\end{align}
	Durch anschließendes Einsetzen von $\boldsymbol{a}$ in die Gleichung \eqref{eq:linKom} 
	\begin{align}
	\bar{\boldsymbol{x}} = \boldsymbol{x} \hat{\boldsymbol{S}}^{+} \hat{\boldsymbol{S}}
	\label{eq:Xest}
	\end{align} resultiert das angenäherte Signal \cite[S. 3]{Deibele.2008}.
		
	%%%%%   -   WEITER   -   %%%%%
	Wie eingangs erwähnt, wurde die Berechnung nur anhand eines einzelnen EIT-Messkanals $\boldsymbol{x}$ beschrieben. Wird nun der gesamten EIT-Datensatz $\boldsymbol{X}$ betrachtet, dessen  $M$ Zeilen die einzelnen Spannungsmessungen und die $N$ Spalten den zeitlichen Verlauf der Messung beinhaltet, ergibt sich  die Gleichung
	\begin{align}
	\bar{\boldsymbol{X}} = \boldsymbol{X} \hat{\boldsymbol{S}}^{+} \hat{\boldsymbol{S}} \text{.}
	\end{align}
	analog zur Gleichung \eqref{eq:Xest}.
	
	Abhängig von der Wahl der Schablonenfunktionen $\hat{\boldsymbol{s}}_k$ kann durch die Anwendung der adaptiven Zeitbereichsfilterung entlang der $M$ Spannungsmessungen jeweils die Ventilationskomponente $\bar{\boldsymbol{X}}_V$ und die Perfusionskomponente $\bar{\boldsymbol{X}}_C$ separiert werden. Die dazu geeigneten Schablonenmatrix werden als $\hat{\boldsymbol{S}}_V$ und $\hat{\boldsymbol{S}}_C$ bezeichnet \cite[S. 3]{Deibele.2008}.\\
	
	
	
%	Die Zeilen von $\boldsymbol{X}$ stellen den zeitlichen Verlauf, also die aufeinanderfolgenden EIT-Frames, und die Spalten von $\boldsymbol{X}$ unabhängigen Spannungsmessungen dar \cite[S. 4]{Deibele.2008}.
%	
%	Wie bereits im Abschnitt (GRUNDLAGEN EIT) erwähnt, werden die Messungen als Matrix $\boldsymbol{X}_{N \times M}$ aufgezeichnet. Dabei stellen die Spalten $\vec{x}_m  = (x_1, \ldots , x_N)^T$ die einzelnen Spannungsmessungen von der Anzahl $m = 1 , \ldots , M$ dar. Analog zum Gleichungssystem \ref{eq:linKom} ergibt sich dann das lineare Gleichungssystem 
%	
%	\begin{align}
%	\underset{N\times M}{\mathrm{\boldsymbol{X}}} = \underset{N\times K}{\mathrm{\boldsymbol{B}}} \cdot \underset{K\times M}{\mathrm{\boldsymbol{A}}}
%	\end{align}
%	
%	und erhalten die optimale Gewichtsmatrix 
%	
%	\begin{align}
%	\boldsymbol{A} = \boldsymbol{B}^{+} \cdot \boldsymbol{X} \text{.}
%	\end{align}
%	
%	Somit kann Schätzmatrix $\hat{\boldsymbol{X}}$ der Eingangsmatrix $\boldsymbol{X}$ wie in Gleichung \ref{Schaetzung} berechet werden:
%	
%	\begin{align}
%	\hat{\boldsymbol{X}} = \boldsymbol{B} \cdot \boldsymbol{B}^{+} \cdot \boldsymbol{X}
%	\end{align}
%	
%	berechnet werden. Die Differenz $\boldsymbol{X} - \hat{\boldsymbol{X}}$ geht dabei gegen $0$. \\
%	
%	Abhängig von der Wahl der Schablonenfunktion $b_k(t)$ kann eine die Näherung bzw. Schätzung des Ventilations- oder des Durchblutungssignal berechnet werden. Im folgenden werden die berechneten Signale dann als $\hat{\boldsymbol{X}}_V$ und $\hat{\boldsymbol{X}}_C$ und die dazu entsprechenden Schablonenfunktionen $\boldsymbol{B}_V$ und $\boldsymbol{B}_C$ bezeichnet. \\
	
%	Weitere Literatur
%	\begin{itemize}
%	\item \cite{Lovric.2011}
%	\item 
%	\end{itemize}
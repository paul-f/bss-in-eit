\chapter{Die elektrische Impedanztomographie}
\label{cha:Grundlagen_EIT}
	
	Das folgende Kapitel widmet sich der grundlegenden Theorie der elektrischen Impedanztomographie (EIT). Dafür wird zunächst der Weg von der elektrischen Impedanz zur Impedanzmessung von biologischem Gewebe beschrieben. Anschließend erfolgt die technische Erläuterung des Arbeitsprinzips von EIT-Systemen.

	% Abkürzungen
	\abk{EIT}{Elektrische Impedanztomographie}
	
	\section{Von der elektrischen Impedanz zur Bioimpedanz}
	Zunächst wird der allgemeinen Begriff der elektrische Impedanz eingeführt, um diesen anschließend in den die Bioimpedanz, die elektrische Impedanz von biologischem Gewebe, zu überführen.
	
	\subsection{Elektrische Impedanz}
	\label{subsec:impedanz}
	In der Wechselstromlehre repräsentiert die elektrische Impedanz eine der wichtigsten Größen. Dabei wird der Ohmsche Widerstand $R$ (Gleichstromwiderstand) um die komplexe Komponente des Phasenwinkels $\varphi$ erweitert und man erhält die komplexe Impedanz $\underline{Z}$ (Wechselstromwiderstand) \cite[S. 7]{Kaufmann.2015}. Im Gegensatz zum Ohmschen Widerstand, beschreibt die Impedanz
	
	\begin{align}
	\begin{split}
	\underline{Z} 
	= \frac{\underline{u}}{\underline{i}} 
	= \frac{|\underline{u}| \cdot e^{j(\omega t + \varphi_u)}}{|\underline{i}| \cdot e^{j(\omega t + \varphi_i)}} 
	= \Bigl|\frac{\underline{u}}{\underline{i}}\Bigl| \cdot e^{j(\varphi_u - \varphi_i)} 
	= |\underline{Z}| \cdot e^{j \varphi} \\
	\\	
	\text{mit } |\underline{Z}| = \Bigl|\frac{\underline{u}}{\underline{i}}\Bigl| \text{ und } \varphi = \varphi_u - \varphi_i
	\end{split}
	\label{eq:impedanz}
	\end{align}
		 
	zusätzlich zum Betrag $|\underline{Z}|$ auch die Phasenverschiebung $\varphi$ zwischen komplexer Stromstärke $\underline{i}$ und komplexer Spannung $\underline{u}$ \cite[S. 7]{Kaufmann.2015}, \cite[S. 31]{Weigerber.2015}.
	
	Für die Berechnung einer unbekannten Impedanz, muss diese mit einem bekannten Strom oder einer bekannten Spannung angeregt werden. Anschließend lässt sich die unbekannte Spannung messen und anhand dessen die Impedanz mithilfe der Gleichung \ref{eq:impedanz} berechnen \cite[S. 31]{Weigerber.2015}, \cite[S. 7]{Kaufmann.2015}. 
	
	Auch in Bereichen der Medizin spielt die elektrische Impedanz eine Rolle. Zum Beispiel lässt sich menschliches Gewebe mittels der Impedanzspektroskopie, also der Bestimmung der elektrischen Impedanz, in Abhängigkeit der Zeit und Frequenz charakterisieren und klassifizieren \cite[S. 8]{Kaufmann.2015}.
	
	\subsection{Bioimpedanz}
	\label{subsec:bioimpedanz}
	Die elektrische Impedanz von biologischem Gewebe wird im Allgemeinen als Bioimpedanz bezeichnet \cite[S. 8]{Kaufmann.2015}. Der folgende Abschnitt betrachtet ausschließlich die Impedanzmessung von menschlichem bzw. tierischem Gewebe.
	
		\paragraph{Die Gewebezelle} Eine menschliche Gewebezelle lässt sich vereinfacht durch die Zelle an sich und den Raum außerhalb der Zellen darstellen, wie in Abbildung \ref{fig:2-1_Zelle} (links) \cite[S. 399]{Dossel.2016} dargestellt. Ausgehend davon lassen sich die elektrischen Eigenschaften einer solchen Zelle in ein elektrisches Ersatzschaltbild (ESB) \abk{ESB}{Elektrisches Ersatzschaltbild} überführen. Das ESB beschreibt dabei lediglich die empirisch erfassten Eigenschaften und stellt im Hinblick auf der Komplexität des menschlichen Körpers eine starke Vereinfachung dar \cite[S. 10f.]{Kaufmann.2015}.
	
	\begin{figure}[h]
 	\centering
 	\includegraphics[width=0.8\textwidth]{2-1_Zelle}
 	\caption[ESB einer Gewebezelle]{Das empirisch bestimmte ESB einer einzelnen Gewebezelle (links) und die technische Vereinfachung (rechts) (basierend auf \cite[S. 11]{Kaufmann.2015})}
 	\label{fig:2-1_Zelle}
	\end{figure}
	
	Dabei lässt sich die Zellmembran, aufgrund der Ionenkanäle, die die Zellmembran entlang durchziehen, als verlustbehafteter Kondensator $C_M||R_M$ annehmen. Da der intra- als auch der extrazelluläre Raum, also der Raum innerhalb und außerhalb der Zellen, lediglich aus Intra- und Extrazellularflüssigkeit bestehen, können diese Bereiche als die beiden Widerstände $R_I$ und $R_E$ betrachtet werden \cite[S. 10f.]{Kaufmann.2015}.
	
	Das ESB aus Abbildung \ref{fig:2-1_Zelle} (links) lässt sich nach der empirischen Vereinfachung zusätzlich noch technisch weiter vereinfachen. Dies erfolgt durch eine Überführung in ein $R+R||C$-Glied, wie in Abbildung \ref{fig:2-1_Zelle} (rechts) dargestellt \cite[S. 10f.]{Kaufmann.2015}. Während ein Kondensator im Gleichstrombetrieb sperrt ($\lim \limits_{\omega \to 0} \underline{Z}_C (\omega) = \frac{1}{j \omega C} = \SI[parse-numbers = false]{\infty}{\Omega}$), fängt dieser im Wechselstrombetrieb mit zunehmender Frequenz an zu leiten ($\lim \limits_{\omega \to \infty} \underline{Z}_C (\omega) = \frac{1}{j \omega C} = \SI{0}{\Omega}$) \cite[S. 32]{Weigerber.2015}.
	
	
	\paragraph{Impedanz von biologischem Gewebe}
	Anhand des vereinfachen ESB einer Gewebezelle aus Abbildung \ref{fig:2-1_Zelle} (rechts) lässt sich erkennen, dass die Bioimpedanz, bei einer Frequenzerhöhung, bedingt durch die kapazitiven Eigenschaften der Zellmembran, abnimmt. Anhand der frequenzabhängigen Chrarakteristka des zu untersuchenden Gewebes, lassen sich Gewebeveränderungen aufgrund der Beschaffenheit erkennen \cite[S. 51]{Kaufmann.2015}. In der Medizin wird dieser Zusammenhang z.B. im Bereich der Tumorerkennung ausgenutzt, da sich die Impedanz von mutierten gegenüber gesunden Gewebe in Abhängigkeit der Frequenz unterscheidet \cite[S. 51]{Kaufmann.2015}, \cite[S. 415]{Dossel.2016}.

	Die Impedanz-Änderungen vom Gewebe sind jedoch nicht nur auf die Frequenzabhängigkeiten zurückzuführen. Werden unterschiedliche Gewebearten mit einem Strom konstanter Frequenz angeregt, ergeben sich gewebespezifische Impedanzen. Die Tabelle \ref{tab:GewebeImp} stellt einige dieser erwarteten Bioimpedanzen in Abhängigkeit der untersuchten Gewebeart dar \cite[S. 97]{Brown.2003}.
	
	\begin{table}[h]
		\centering
		\begin{tabular}{ll}
			Gewebeart & Impedanz \\
			\hline
			Muskel	& \SIrange{2}{4}{\Omega \meter} \\
			Fett	& \SI{20}{\Omega \meter} \\
			Lunge	& \SI{10}{\Omega \meter} (ändert sich in Abh. der Atmung)\\
			Blut	& \SI{1,6}{\Omega \meter} \\
			Knochen	& > \SI{40}{\Omega \meter} \\
			\hline
		\end{tabular}
		\caption[Impedanzen von Gewebearten]{Vergleich der Impedanzen von verschiedenen Gewebearten bei einer Anregungsfrequenz von $f = \SI{10}{kHz}$ \cite[S. 97]{Brown.2003}}
		\label{tab:GewebeImp}
	\end{table}
	
	Des Weiteren lassen sich Differenzen aufgrund zeitlicher Veränderungen des untersuchten Lebewesens feststellen. Wird dabei die Atmungsaktivität betrachtet, können durch eine ungleiche Lungenventilation bzw. eine inhomogene Verteilung der Luftmenge im Lungengewebe Unterschiede nachgewiesen werden \cite[S. 11]{Kaufmann.2015}. Weitere Schwankungen sind auf die Perfusion (Durchblutung) des zu untersuchenden Gewebekomplexes zurückzuführen \cite[S. 11]{Kaufmann.2015}. Dabei handelt es sich zum einen um die zeitliche Änderung der Blutmenge und zum anderen um die dadurch hervorgerufene Gefäßdehnung im Untersuchungsobjekt. Aber auch aufgrund der eigentlichen mechanischen Bewegungen des Herzens und der Lunge werden weitere Differenzen hervorgerufen, da sich bei jedem Herzschlag und jedem Atemzug die geometrische Gewebeanordnung ändert. \cite[S. 11]{Kaufmann.2015}.

	\newpage
	\section{Elektrische Impedanztomographie}
	Die elektrische Impedanztomographie (EIT) ist ein bildgebendes Verfahren, welches auf die mehrkanalige Messung von Bioimpedanzen innerhalb eines Objektes basiert \cite[S. 51]{Kaufmann.2015}. Mithilfe eines geeigneten mathematischen Algorithmus kann anschließend aus diesen Messungen ein Bild rekonstruiert werden, das die Impedanz- bzw. die Leitwertverteilung \cite[S. 51]{Kaufmann.2015} im Inneren untersuchten Objektes darstellt \cite[S. 9]{Braun.2013}.
	
	\subsection{Messverfahren}
	\label{subsec:messverfahren}
	Um ein Bild rekonstruieren zu können, bedarf es zunächst ausreichender Spannungsmessungen am Untersuchungsobjekt. Dazu stehen eine Vielzahl von verschiedenen EIT-Systeme zur Verfügung, wobei die meisten mit einer Anzahl von acht bis 32 Elektroden bestücktem Gürtel arbeiten \cite[S. 53]{Kaufmann.2015}. Eine höhere Anzahl von Elektroden würde aufgrund von vermehrten Abtastpunkten theoretisch auch zu einer höheren Auflösung führen \cite[S. 46]{Barber.1990}. Jedoch dringen die dafür benötigten Messungen nicht tief genug in das Objekt ein, sodass die Auflösung im Inneren des Körpers im Vergleich zum Randbereich schlecht sind \cite[S. 46]{Barber.1990}. Zudem wird die Anzahl der Elektroden durch den begrenzten Platz an der Objektoberfläche für die Elektroden limitiert \cite[S. 53]{Kaufmann.2015}. In Abbildung \ref{fig:2-2_EIT} wird das wesentliche das Prinzip des Messverfahrens dargestellt.
	
	\begin{figure}[h]
 	\centering
 	\includegraphics[width=0.9\textwidth]{2-2_EIT}
 	\caption[Prinzip der elektrischen Impedanztomographie]{Prinzip der elektrischen Impedanztomographie (basierend auf \cite[S. 53]{Kaufmann.2015}, \cite[S. 40]{Rahman.2013})}
 	\label{fig:2-2_EIT}
	\end{figure}
	
	Die folgenden, theoretischen Betrachtungen beziehen sich auf ein EIT-System, bestehend aus einem Elektrodengürtel mit 16 Elektroden (siehe Abbildung \ref{fig:2-2_EIT}). Als Untersuchungsobjekt dient ein Thorax (Brustkorb), auf dem dieser Elektrodengürtel platziert wird.
	
	\paragraph{Stromeinspeisung}
	Die Messstromeinspeisung und die anschließenden Spannungsmessungen werden via den auf dem Thorax angebrachten Elektroden realisiert \cite[S. 404]{Dossel.2016}. Für die Stromeinspeisung und die Spannungsmessung existieren zwei grundsätzlich verschiedene Ansätze: die Stromeinspeisung und Spannungsmessung über die benachbarten Elektroden oder über die gegenüberliegenden Elektroden \cite[S. 70]{Noor.2007}. Aufgrund der Einspeisung eines Stroms bildet sich im Thorax ein elektrisches Feld aus \cite[S. 31]{Karsten.2010}. Werden diese Punkte des gleichen Potentials miteinander verbunden, ergeben sich Äquipotentiallinien \cite[S. 245]{Plamann.2013}. Die verschiedenen Abstände der Äquipotentiallinien zueinander (bzw. die Dichte) beschreibt dabei das Maß der Sensitivität gegenüber regionalen Impedanzänderungen \cite[S. 405]{Dossel.2016}. 
	
	\begin{figure}[h]
 	\centering
 	\includegraphics[width=0.9\textwidth]{2-3_Einspeisung}
 	\caption[Ausbildung der Äquipotentiallinien in Abhängigkeit der Stromeinspeisung]{Ausgebildete Äquipotentiallinien im Körper für die Stromeinspeisung und Spannungsmessung über die benachbarten Elektroden (links) und über die gegenüberliegenden Elektroden (rechts) (basierend auf \cite[S. 561ff.]{Malmivuo.1995}, \cite[S. 406]{Dossel.2016})}
 	\label{fig:2-3_Einspeisung}
	\end{figure}
	
	Abbildung \ref{fig:2-3_Einspeisung} stellt die genannten beiden Methoden anhand der Äquipotentiallinien unter Annahme einer gleichmäßig verteilten Leitfähigkeit innerhalb des Körpers gegenüber. Bei dem erst genannten Verfahren in Abb. \ref{fig:2-3_Einspeisung} (links) wird der Strom durch zwei benachbarte Elektroden eingespeist und die Spannungen entlang aller benachbarten Elektrodenpaare gemessen. Die Äquipotentiallinien breiten sich nicht gleichmäßig im Körper aus, da sich die Stromflüsse nah im Bereich den Einspeiselektroden konzentrieren. Dies hat zur Folge, dass die Sensitivität mit zunehmenden Abstand zwischen Einspeis- und Messelektroden abnimmt. Es lassen sich dadurch die räumlichen Impedanzsschwankungen in der Objektmitte schlechter bestimmen als im Randgebiet \cite[S. 70]{Noor.2007}.
		
	Bei dem zweiten Verfahren, also Abb. \ref{fig:2-3_Einspeisung} (rechts), wird der Strom durch die sich gegenüberliegenden Elektroden eingespeist und die Spannungen werden in Bezug zu einer Referenzelektrode, welche sich neben der Stromelektrode befindet, gemessen. Im Vergleich zum zur Einspeisung via benachbarter Elektroden breiten sich die Äquipotentiallinien gleichmäßiger im Körper aus. \cite[S. 70]{Noor.2007} Dies führt zu einer höheren Sensitivität im Bereich der Objektmitte \cite[S. 405]{Dossel.2016}.

	In der Praxis bewährt sich deshalb die Kombination der beiden unterschiedlichen Ansätze: das Verfahren der Skip-$n$ Pattern-Einspeisung \cite[S. 100]{Gong.2016}. Es beschreibt das Auslassen einer Anzahl von $n$ Elektroden für jeweils die Stromeinspeisung und der Spannungsmessung \cite[S. 794]{Grychtol.2016}. Unter Verwendung eines Elektrodengürtels in der horizontalen Ebene, garantiert die z.B. die Skip-4 Pattern-Stromeinspeisung eine hohe Empfindlichkeit in dieser Ebene \cite[S. 794]{Grychtol.2019}.
	
	\paragraph{Messwertaufnahme}
	Die Elektroden des Elektrodengürtels sind mit einem Multiplexer verbunden, der die Stromeinspeisung und die dazugehörigen Spannungsmessung steuert \cite[S. 55]{Kaufmann.2015}. Die anschließenden Messungen erfolgen dann, wie in Abbildung \ref{fig:2-2_EIT} angedeutet, rotierend \cite[S. 31]{Karsten.2010}. Bei einem System mit einer Anzahl von $K$ Elektroden ergibt ein vollständiger Messzyklus $M = K \cdot (K - 3)$ Spannungsmessungen \cite[S. 46]{Barber.1990}, \cite[S. 2]{Jang.2020}, der als Rahmen (engl. Frame) bezeichnet wird \cite[S. 55]{Kaufmann.2015}. Anschließend wird der Vorgang wiederholt und es entsteht ein EIT-Datensatz $\boldsymbol{X}$ bestehend aus $N$ Rahmen über die Zeit. Aus dem Datensatz lässt sich dann für jeden Rahmen jeweils ein dazugehöriges EIT-Bild rekonstruieren \cite[S. 56]{Kaufmann.2015}.
	
	%Aufgrund des Reziprozitätsprinzips, das besagt, dass die von einem Elektrodenpaar gemessene Spannung, wenn ein Strom durch ein anderes Elektrodenpaar eingespeist wird, mit der Spannung identisch ist, die gemessen wird, wenn die Spannungsmessung und die Strominjektionselektrodenpaare umgekehrt werden, sind nur die Hälfte, dh 104 Messungen unabhängig. [NOOR, S. 70 -> direkte Übersetzung]
	%Das Elektronenpaar für die Einspeisung und für die Messung kann als Zweitor betrachtet werden. Aus dem Reziprozitätstheorem was besagt, dass bei dem Austauschen der Funktion der Elektrodenpaare für Einspeisung und Messung die gemessene Spannung identisch ist [NOOR, S. 70], ergeben sich $(N \cdot (N - 3)) / 2$ unabhängige Messungen [NOOR, S. 70].
	
	\subsection{Rekonstruktion eines EIT-Bildes}
	Die Aufgabe der Rekonstruktion besteht darin aus den einzelnen aufgezeichneten Rahmen eines EIT-Datensatzes jeweils ein Bild der Impedanz- bzw. Leitwertverteilung zu erzeugen \cite[S. 409]{Dossel.2016}. Dabei lässt sich grundsätzlich zwischen der Rekonstruktion der absoluten (Absolut-EIT) und der Rekonstruktion der relativen Änderung (Differenz-EIT) der Impedanzverteilung unterscheiden. Aufgrund der geringeren Anfälligkeit gegenüber geometrischer Veränderungen (z.B. durch Verrutschen der Elektroden) und Abweichungen der Messungen (z.B. durch Rauschen) liefert die Differenz-EIT im Gegensatz zu der Absolut-EIT zuverlässigere Rekonstruktionsergebnisse. Deswegen konnte sich dieses EIT-Verfahren auch in der Medizin durchsetzen. \cite[S. 51f.]{Kaufmann.2015}
	
	Mithilfe der Differenz-EIT lassen sich sowohl die Zustands- als auch Frequenzdifferenzen innerhalb des untersuchten Objektes darstellen. Dabei dient die Zustandsdifferenz der Darstellung des zeitlichen Verlaufs der Impedanzverteilungsdifferenz, die sich besonders gut für z.B. die Überwachung der Atmungsaktivität eignet (vgl. \ref{subsec:bioimpedanz} und \ref{subsec:messverfahren}). Mit der Frequenzdifferenzen lassen sich hingegen die frequenzabhängigen Chrarakteristka des zu untersuchenden Gewebes bestimmen (vgl. \ref{subsec:bioimpedanz}) \cite[S. 50f.]{Kaufmann.2015}.\\
	
	Wie bereits erwähnt, werden die Spannungen an der Oberfläche des Thorax abgenommen. Daraus ergeben sich aus mathematischer Sicht zwei zu lösende Rekonstruktionprobleme: das Vorwärtsproblem und das inverse Problem \cite[S. 65]{Kaufmann.2015}. Das Vorwärtsproblem berechnet die Grenzspannungen mit einer gegebenen Impedanzverteilung, wogegen das inverse Problem die Impedanzverteilung innerhalb des Objektes anhand der gemessenen Spannungen an dessen Oberfläche berechnet. Beide mathematischen Probleme werden  Die Abbildung \ref{fig:2-4_Rekonstruktionsproblem} vereinfacht, schematisch dargestellt.
	
	%Die Abbildung \ref{fig:2-2_Rekonst-Algo} stellt das Schema der Rekonstruktion von der Impedanzverteilung dar. Dabei verdeutlicht der untere Teil des Schemas das Vorwärtsproblem und der obere Teil das inverse Problem. \cite[S. 69]{Kaufmann.2015}, \cite[S. 11f.]{Braun.2013}
	
	\begin{figure}[h]
 	\centering
 	\includegraphics[width=0.9\textwidth]{2-4_Rekonstruktionsproblem}
 	\caption[Rekonstruktionsprobleme]{Beschreibung des Vorwärtsproblems (links) und des inversen Problems (rechts) (basierend auf \cite[S. 11]{Braun.2013}, \cite[S. 40]{Rahman.2013}, \cite[S. 448]{Yan.2004})}
 	\label{fig:2-4_Rekonstruktionsproblem}
	\end{figure}

	\paragraph{Vorwärtsproblem}
	Wie aus Abbildung \ref{fig:2-4_Rekonstruktionsproblem} zu entnehmen, lässt sich anhand des Vorwärtsproblems die Grenzspannungen mit einer gegebenen bzw. geschätzten Impedanzverteilung im Inneren des Körpers berechnen \cite[S. 448]{Yan.2004}. Dafür muss der Algorithmus zusätzlich in Kenntnis über den verwendeten Messstrom und der geometrischen Anordnung gesetzt werden \cite[S. 66]{Kaufmann.2015}. Zur Lösung des Vorwärtsproblems, wird das numerische Verfahren der Finite-Elemente-Methode (FEM)\abk{FEM}{Finite-Elemente-Methode} eingesetzt. Mittels der FEM lässt sich das Objektinnere in eine endliche Anzahl von kleinen Elemente zerlegen. Die zugewiesene geschätzte Impedanzverteilung innerhalb jeden einzelnen Elementes ist dabei konstant. Die Ecken der Elemente werden als Knoten bezeichnet und sind untereinander durch diese verbunden (die benachbarten). Die Werte der Knoten von jedem Element werden durch eine Interpolationsfunktion bestimmt, die die Werte in Bezug auf die Elementeigenschaften annähert. Dadurch lassen sich dann die Spannungen an jedem FEM-Knoten berechnen und somit auch die Spannungen an der Objektoberfläche \cite[S. 448]{Yan.2004}.

	\paragraph{Inverses Problem}
	Das inverse Problem beschreibt die eigentliche Rekonstruktion eines EIT-Bildes \cite[S. 448]{Yan.2004}. Ziel ist es, die unbekannte Impedanzverteilung im Objektinneren anhand der Einspeisströme, der geometrischen Anordnung und den an der Objektoberfläche aufgezeichneten Messspannungen zu berechnen. Eine analytische Lösung dieses Problems lässt sich allerdings nicht eindeutig bestimmen \cite[S. 65]{Kaufmann.2015}.
	
	Wenn sich die Impedanzverteilung von $\sigma$ zu $\sigma + \Delta \sigma$ ändert, dann führt dies auch zu Änderungen der Transferimpedanzen (Kopplungswiderstand) $\Delta Z$ zwischen den Stromeinspeisungspaare und den Spannungselektroden. Zunächst werden mithilfe des Vorwärtsproblems die zu erwarteten Randspannungen $u$ für eine geschätzte Impedanzverteilung $\sigma$ (a-priori \cite[S. 65]{Kaufmann.2015}) berechnet. Anhand der Differenzen zwischen den berechneten Randspannungen $u$ und den tatsächlich gemessenen Randspannungen $v$ lässt sich die Änderungen der Transferimpedanz $\Delta Z$ bestimmen. Daraus ergeben sich dann die Differenzen der Impedanzverteilung $\Delta \sigma$, von welchen aus auf die tatsächliche Impedanzverteilung $\sigma$ innerhalb des Objekts schließen lässt. \cite[S. 449]{Yan.2004}.\\
	
	Die Abbildung \ref{fig:2-5_Bild} zeigt ein so rekonstruiertes EIT-Bild. Die Spannungen wurde mit einem EIT-System aufgenommen, welches mit einer Anzahl von 32 Elektroden arbeitet. Die Messung erfolgte unter Verwendung des Skip-4 Pattern-Messprotokolls. Anschließend wurde mit dem GREIT-Algorithmus (engl. Graz consensus Reconstruction algorithm for EIT) \cite{Adler.2009} das gesuchte EIT-Bild rekonstruiert \cite[S. 796f.]{Grychtol.2016}.
	
	\begin{figure}[h]
 	\centering
 	\includegraphics[width=0.6\textwidth]{2-5_Bild}
 	\caption[Rekonstruiertes EIT-Bild]{Rekonstruiertes EIT-Bild einer EIT-Messung unter Verwendung der Skip-4 Pattern-Stromeinspeisung und Anwendung des GREIT-Algorithmus \cite[S. 797]{Grychtol.2016}}
 	\label{fig:2-5_Bild}
	\end{figure}
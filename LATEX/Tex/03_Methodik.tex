\chapter{Methoden zur Separation der Signalkomponenten}
\label{cha:Methodik}

	Das nun folgende Kapitel beschreibt die bisher vorgeschlagenen Trennungsalgorithmen unter Verwendung der beschriebenen BSS-Verfahren aus Abschnitt \ref{sec:BSS}. Es wird dafür angenommen, dass sich jeder $M \times N$ EIT-Datensatz aus den unbekannten Komponenten 
	\begin{align}
	\boldsymbol{X} = \boldsymbol{X}_V + \boldsymbol{X}_C + \boldsymbol{X}_N
	\end{align} der Ventilation ($V$, engl. ventilation), der Perfusion ($C$, engl. cardiac) und des Messrauschens ($N$, engl. noise) zusammensetzt \cite[S. 4]{Deibele.2008}. 
		
	Für die Separierung der einzelnen Signalanteile existieren aktuell drei verschiedene statistische Ansätze: die Trennung via PCA \cite{Deibele.2008}, ICA \cite{Rahman.2013} und die Kombination beider \cite{Jang.2020}.

	Im folgenden werden die drei vorgeschlagenen Methoden erklärt. Abschließend erfolgt die Vorstellung der für die Arbeit verwendeten EIT-Signaletypen.

	
	\section{Basierend auf die PCA}
	\label{sec:PCA}
	Der erste Ansatz zur EIT-Signaltrennung via BSS wurde 2008 \cite{Deibele.2008} vorgeschlagen. Dabei wird ausschließlich auf das statistische Verfahren der Hauptkomponentenanalyse zurückgegriffen, visualisiert in Abbildung \ref{fig:4-1_PCA}.\\
	
	\begin{figure}[h]
 	\centering
 	% trim = left bottom right top
 	\includegraphics[trim=0 50 0 50,clip,width=1\textwidth]{Figures/4-1_PCA}
 	\caption[Quellentrennung via PCA]{Prinzipelle Rechenabfolge der Methode zur Quellentrennung unter Verwendung der PCA (basierend auf \cite[S. 5]{Deibele.2008})}
 	\label{fig:4-1_PCA}
	\end{figure}

	Das vorgeschlagene Trennungsverfahren lässt sich in zwei Schritte einteilen. Im ersten Schritt wird die erste Näherung $\bar{\boldsymbol{X}}_V^{\prime}$ des Ventilationssignal $\boldsymbol{X}_V$ berechnet. Dafür wird vom beobachteten EIT-Datensatz $\boldsymbol{X}$ die erste Hauptkomponente $\hat{\boldsymbol{s}}_1 \widehat{=} \hat{\boldsymbol{S}}_V$, also die Hauptkomponente mit der maximalen Varianz, gebildet. Anschließend wird diese als Schablonenfunktion mittels der adaptiven Zeitbereichsfilterung auf $\boldsymbol{X}$ angewandt (s. Abschnitt \ref{subsec:Templates}), woraus die erste Näherung $\bar{\boldsymbol{X}}_V^{\prime}$ resultiert.
		
	Der zweite Schritt wird durch die Berechnung der erste Näherung $\bar{\boldsymbol{X}}_C^{\prime}$ des Perfusionsignals $\boldsymbol{X}_C$ eingeleitet. Dafür wird die Differenz $\bar{\boldsymbol{X}}_C^{\prime} = \boldsymbol{X} - \bar{\boldsymbol{X}}_V^{\prime}$ gebildet. Um die vom Rauschen hervorgerufenen Artefakte zu unterdrücken, wird die erste Näherung $\bar{\boldsymbol{X}}_C^{\prime}$ vor der anschließenden Weiterverarbeitung im Frequenzbereich mit einem FIR-Bandpass-Filter 30. Ordnung gefiltert. Als Durchlassbereich wurde ein Frequenzband von \SIrange{0,92}{4,6}{\hertz} (\SIrange{55}{280}{bpm}) empirisch festgelegt. Von dem so gefilterten Signal $\bar{\boldsymbol{X}}_{C_{BP}}^{\prime}$ werden anschließend die ersten beiden Hauptkomponenten $\hat{\boldsymbol{s}}_1$ und $\hat{\boldsymbol{s}}_2$ extrahiert. Diese dienen als Schablonenfunktionen der Perfusionskomponente. Das Perfusionssignal weist aber in der Elektrodenebene durch den Thorax eine erhebliche Phasenverschiebungen auf, was auf die Ausbreitung des Blutes zurückführen ist. Darum werden zusätzlich phasenverschobene Versionen, die um $\pm 1/3$ einer durchschnittlichen Herzperiode verschoben sind, von $\hat{\boldsymbol{s}}_1$ und $\hat{\boldsymbol{s}}_2$ generiert. Es ergibt sich somit die Schablonenmatrix
	
	\begin{equation}\hat{\boldsymbol{S}}_C = (\hat{\boldsymbol{s}}_1 \; ^{+\frac{1}{3}}\hat{\boldsymbol{s}}_1 \; ^{-\frac{1}{3}}\hat{\boldsymbol{s}}_1 \; \hat{\boldsymbol{s}}_2 \; ^{+\frac{1}{3}}\hat{\boldsymbol{s}}_2 \; ^{-\frac{1}{3}}\hat{\boldsymbol{s}}_2)^T \enspace ,\end{equation} 
	die auf die ersten beiden Näherungen der Signalkomponenten angewandt werden kann. Während die Zeitbereichsfilterung von $\bar{\boldsymbol{X}}_{C}^{\prime}$ die zweite Näherung $\bar{\boldsymbol{X}}_{C1}^{\prime \prime}$ des Durchblutungssignals darstellt, resultiert aus der Filterung der ersten Näherung der Ventilationskomponente $\bar{\boldsymbol{X}}_V^{\prime}$ der in ihr verbliebene Anteil $\bar{\boldsymbol{X}}_{C2}^{\prime \prime}$ des Perfusionssignal.
	
	Somit lassen sich die Signalkomponenten durch Addition bzw. Subtraktion der zweiten Näherungen auf die erste Näherung berechnen. Es resultiert
	
	\begin{align}
	\bar{\boldsymbol{X}}_C = \bar{\boldsymbol{X}}_{C1}^{\prime \prime} + \bar{\boldsymbol{X}}_{C2}^{\prime \prime}
	\end{align}
	\begin{align}
	\bar{\boldsymbol{X}}_V = \bar{\boldsymbol{X}}_V^{\prime} - \bar{\boldsymbol{X}}_{C2}^{\prime \prime}
	\end{align}
	
	als Ventilationssignal $\bar{\boldsymbol{X}}_V$ und Durchblutungssignal $\bar{\boldsymbol{X}}_C$. Aus diesen Komponenten können jeweils die gesuchten EIT-Bilder rekonstruiert werden.
	
	\paragraph{Anpassungen}
	In der vorliegenden Untersuchungen lässt sich feststellen, dass die Anwendung von zeitverschobenen Versionen der Schablonenfunktionen zu einer Verschlechterung des Separationsergebnis führt. Darum wurden auf diese in Gänze verzichtet. Außerdem lassen sich bessere Ergebnisse durch eine Anpassung der Bandpassfilterung erzielen. Um möglichst viele Informationen des Verlaufs der Herzkomponente beizubehalten, wurde die Filterung in einem Durchlassbereich von \SIrange{0,5}{5}{\hertz} durchgeführt. Frequenzen unterhalb der unteren Grenzfrequenz lassen sich nur durch eine sehr große Filterordnung des Bandpasses unterdrücken. Um dieses Problem zu umgehen, kamen ein FIR-Tiefpassfilter und ein FIR-Hochpassfilter mit jeweils 50. Ordnung zum Einsatz. Sie weisen im Gegensatz zum FIR-Bandpassfilter derselben Ordnung eine steilere Flanke und stärkere Unterdrückung im Sperrbereich auf.
	
	

	\section{Basierend auf die ICA}
	\label{sec:ICA}
	Ein weiterer Ansatz wurde 2013 \cite{Rahman.2013} vorgeschlagen, bei dem ausschließlich die Unabhängigkeitsanalyse zurückgegriffen wird, schematisch dargestellt in Abbildung \ref{fig:4-2_ICA}.
		
	\begin{figure}[h]
 	\centering
 	\includegraphics[trim=0 50 0 50,clip,width=1\textwidth]{Figures/4-2_ICA}
 	\caption[Quellentrennung via ICA]{Prinzipelle Rechenabfolge der Methode zur Quellentrennung unter Verwendung der ICA (basierend auf \cite[S. 41]{Rahman.2013}, \cite[S. 5]{Deibele.2008})}
 	\label{fig:4-2_ICA}
	\end{figure}
	
	Im ersten Schritt wird eine geeignete Schablonenfunktion unter Anwendung der ICA gesucht, wobei zunächst der beobachtete EIT-Datensatz $\boldsymbol{X}$ dem Whitening unterzogen wird. Dies lässt sich durch die Anwendung der PCA realisiert. Es entsteht eine unkorrelierte $K \times N$-Matrix $\boldsymbol{Z}$ dessen Zeilen nach absteigender Varianz sortiert sind. Um bereits im Vorfeld das im Signal enthaltende Rauschen zu unterdrücken, wird die Matrix auf die Dimension $K<M$ reduziert, d.h. Signale mit geringer Varianz werden ausgelöscht. Mit Hilfe der ICA auf die vorverarbeitete Version $\boldsymbol{Z}$ lassen sich unabhängige Komponenten $\hat{\boldsymbol{S}}_V^{\prime}$ berechnen. Die ICs ergeben potentielle Schablonenfunktionen der zu extrahierenden Komponente. Es bedarf also der Auswahl einer geeigneten Schablonenfunktion. Um ICs auszuschließen, dessen Frequenzspektren nicht von Relevanz sind, werden diese mittels der Fast Fourier Transformation (FFT) analysiert. Anhand der verbleibenden unabhängigen Komponenten werden anschließend die aus ihnen resultierenden EIT-Bilder rekonstruiert und miteinander verglichen. Die IC, die das beste Rekonstruktionsergebnis liefert, wird schlussendlich als Schablonenfunktion $\hat{\boldsymbol{S}}_V$ für die Ventilationskomponente weiterverwendet. Aus adaptiven Zeitbereichsfilterung des beobachteten Signal resultiert abschließend das Signal der Ventilationskomponente $\bar{\boldsymbol{X}}_V$.
	
	Der zweite Schritt wird durch die Differenzbildung $\bar{\boldsymbol{X}}_C^{\prime} = \boldsymbol{X} - \bar{\boldsymbol{X}}_V$ eingeleitet. Dabei bildet das Ergebnis $\bar{\boldsymbol{X}}_C^{\prime}$ die erste Näherung des Perfusionssignal $\boldsymbol{X}_C$. Um aus der ersten Näherung eine geeignete Schablonenfunktion $\hat{\boldsymbol{S}}_C$ zu extrahieren, wird die Abfolge aus dem ersten Schritt an ihr angewandt. Anschließend lässt sich $\bar{\boldsymbol{X}}_C^{\prime}$ mit der Schablonenfunktion filtern. Das Resultat ist die gesuchte Perfusionskomponente $\bar{\boldsymbol{X}_C}$.
	
	\paragraph{Anpassungen}
	Zunächst erfolgte die Anpassung des verwendeten ICA-Algorithmus: Im vorliegenden Verfahren wurde ein Natural Gradient Algoritmus verwendet \cite[S. 41]{Rahman.2013}. Die Anwendung des Algorithmus von Kitamura \cite{GitHub.15.06.2021} führte dabei allerdings zu keinerlei Erfolg. Das beste Ergebnis lieferte der JADE-Algorithmus von Cardoso \cite{.01.11.2005}.
	
	
	Des Weiteren wurde die Dimensionsreduzierung unter Anwendung des Whitenings angepasst. Um die gewünschte Dimension der jeweiligen Matrix $\boldsymbol{Z}_V$ und $\boldsymbol{Z}_C$ dynamisch festlegen zu können, werden die normalisierten Singulärwerte betrachtet. Diese lassen sich sich unter Verwendung der SVD berechnen \cite[S. 5f.]{Jang.2020}. Im Falle der Ventilationskomponente, werden die Singulärwerte der beobachteten Matrix $\boldsymbol{X}$ bestimmt. Die Anzahl der \SI{90}{\percent} der dominantesten Singulärwerte ergeben dann die gesuchte Dimension der Matrix $\boldsymbol{Z}_V$. Zur Bestimmung der Dimension der Matrix $\boldsymbol{Z}_C$ wird die SVD auf die erste Schätzung der Perfusionskomponente $\bar{\boldsymbol{X}}_C^\prime$ durchgeführt. Hier entsprechen dann die Anzahl der \SI{85}{\percent} der dominantesten Singulärwerte der gesuchten Dimension.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%           -   Abkürzungen   -          %%%
\abk{FFT}{engl. Fast Fourier Transformation} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



	\section{Kombination von PCA und ICA}
	\label{sec:COMB}
	Im aktuellsten Ansatz von 2020 wurde 2020 wird eine Kombination beider vorheriger Methoden beschrieben. Das bedeutet, dass das Ventilationssignal unter Anwendung der PCA und das Perfusionssignal mit der ICA generiert wird, siehe dazu Abbildung \ref{fig:4-3_PCAICA}.

	\begin{figure}[h]
 	\centering
 	\includegraphics[trim=0 90 0 25,clip,width=1\textwidth]{Figures/4-3_PCAICA}
 	\caption[Quellentrennung via PCA und ICA]{Prinzipelle Rechenabfolge der Methode zur Quellentrennung unter Verwendung der PCA und der ICA (basierend auf \cite[S. 3]{Jang.2020}, \cite[S. 5]{Deibele.2008})}
 	\label{fig:4-3_PCAICA}
	\end{figure}

	Der erste Schritt beschreibt die Berechnung des Ventilationssignal $\bar{\boldsymbol{X}}_V$ unter Anwendung der PCA. Die aus dem EIT-Datensatz $\boldsymbol{X}$ berechneten Hauptkomponenten werden als $M \times N$-Matrix $\hat{\boldsymbol{S}}$ dargestellt. Da die größten Spannungsänderungen, ohne übermäßig Bewegungsartefakte zu erzeugen, durch die Atmung verursacht werden, eignet sich die erste PC, also die mit der größten Varianz, als Schablonenfunktion des Ventilationssignal $\hat{\boldsymbol{S}}_V = \hat{\boldsymbol{s}}_1$. Aus der anschließenden Zeitbereichsfilterung von $\boldsymbol{X}$ unter Verwendung der Schablonenfunktion $\hat{\boldsymbol{S}}_V$ resultiert das gesuchte Ventilationssignal $\bar{\boldsymbol{X}}_V$.
		
	Im zweite Schritt wird für die Berechnung des Perfusionssignals $\bar{\boldsymbol{X}}_C$ hingegen die ICA angewendet. Dadurch, dass das Whitening mit der PCA realisiert wird, lassen sich die im vorherigen Schritt berechneten Hauptkomponenten $\hat{\boldsymbol{S}}$ nun weiter verwerten. Zur Festlegung der gewünschten Dimension von $\hat{\boldsymbol{S}^{\prime}}$ werden die normalisierten Singulärwerte durch die Anwendung der SVD betrachtet. Dabei entspricht die Größe der Dimension der Anzahl $K$ der rund \SI{92,5}{\percent} der dominantesten Singulärwerte. Unter Ausschluss der ersten Hauptkomponente (Ventilationskomponente) ergibt sich die unkorrelierten $(K-1)\times N$-Matrix  $\hat{\boldsymbol{S}}_C^{\prime} = (\hat{\boldsymbol{s}}_2 \; \cdots \; \hat{\boldsymbol{s}}_m)^T$. Aus ihr lassen sich anschließend, unter Anwendung der ICA, die unabhängigen Komponenten $\hat{\boldsymbol{S}}_C^{\prime \prime}$ gewinnen. Zur Auswahl der geeigneten Schablonenfunktion für die Perfusionskomponente wird die FFT angewandt. Die IC, die die größte Energie $\Psi(\boldsymbol{s}_m)$ bei der Grundfrequenz des Herzens $f_h$ aufweist, dient als Schablonenfunktion $\hat{\boldsymbol{S}}_C$. Durch die adaptive Filterung des ursprünglichen EIT-Datensatz $\boldsymbol{X}$ unter Verwendung von $\hat{\boldsymbol{S}}_C$ lässt sich das Perfusionssignal $\bar{\boldsymbol{X}}_C$ bestimmen.
	
	\paragraph{Anpassungen}
	Auch hier ließ sich durch eine Bandpassfilterung das Ergebnis der Separation verbessern. Die Filterung wurde auf die erste Schätzung der Schablonenfunktionsmatrix $\hat{\boldsymbol{S}}_C^\prime$ der Perfusionskomponene angewandt. Wieder kamen dafür ein FIR-Tiefpassfilter und FIR-Hochpassfilter 50. Ordnung mit einem gesamten Durchlassbereich von \SIrange{0,5}{5}{\hertz} zum Einsatz.
	
	\section{Digitale Filterung}
	\label{sec:FILT}
	Als Referenz zu den Methoden, die die Separation der Signalkomponenten unter Anwendung von multivarianten, statistischen Verfahren realisieren, wird die digitale Filterung herangezogen. Auch dieses Verfahren kann auf die einzelnen Spannungsmessungen angewandt werden.
	
	Zur Separation der Ventilationskomponente wurde ein FIR-Bandpassfilter 80. Ordnung definiert, dessen Durchlassbereich \SIrange{0,075}{1}{\hertz} (\SIrange{4,5}{60}{bpm}) beträgt. Die Filterung wird anschließend auf die Datenmatrix $\boldsymbol{X}$ angewandt.
	 
	Um die Perfusionskomponente zu trennen, wird zunächst aufgrund des kleinen Signalanteils das Frequenzspektrum, unter Anwendung der FFT, betrachtet. Anhand dessen lässt sich anschließend der Durchlassbereich des benötigten FIR-Bandpassfilters 80. Ordnung definieren. Die gesuchte Herzfrequenz $f_0$, welche dem Filter als Mittenfrequenz dient, kann aus dem Frequenzspektrum entnommen werden. Unter der Annahme, dass die Frequenz, die die maximale Amplitude oberhalb einer Frequenz von \SI{0,667}{\hertz} (\SI{40}{bpm}) aufweist, der der gesuchten Herzfrequenz $f_0$ entspricht, lässt sich anschließend der Durchlassbereich auf $f_0-\SI{0,2}{\hertz}$ bis $f_0+\SI{0,2}{\hertz}$ ($\pm\SI{12}{bpm}$) festlegen. Daraufhin erfolgt abermals die Anwendung der Filterung auf die Datenmatrix $\boldsymbol{X}$.
	
	
	\section{EA}
	\label{sec:MethEA}
	Auch die EA dient als Referenz gegenüber den multivarianten, statistischen Verfahren, wobei sie dem Verlauf eines einzelnen Herzimpulses entsprechen.
	
	Zunächst müssen die zur Berechnung benötigten Triggerzeitpunkt festgelegt werden. Dafür wird die digitale Filterung auf ein einzelnes Signal angewandt. Ziel ist es, ein sinusförmiges Signal zu erhalten, dessen prinzipieller zeitlicher Verlauf sich mit dem der Perfusion gleicht. Daraufhin lassen sich anhand der einzelnen Maxima die Triggerzeitpunkte festgelegen. Die Breite des zur Segmentierung benötigten Rechteckfensters, entspricht dem durchschnittlichem Abstand der einzelnen Trigger. Die Segmente der jeweiligen Signale lassen sich anschließend zum Zeitpunkt der Trigger bestimmen, wodurch sich dann das EA für jedes einzelne Signal ergibt. 
	
	Im Gegensatz zu den anderen vorgestellten Methoden wird das Verfahren des EA auf die bereits rekonstruierten EIT-Bild einer Messung angewandt. Im Folgenden wird ein  einzelnes Pixel innerhalb der Lunge betrachtet, welches als Referenz zur Berechnung der Triggerzeitpunkte dient.  
	
\newpage
	\section{Beschreibung der EIT-Datensätze}
	\label{sec:Signale}
	Für die Untersuchung der einzelnen Methoden stehen zum einen simulierte und zum anderen reale EIT-Daten zur Verfügung. Die simulierten Daten dienen dabei lediglich zum Testen der Methoden und werden nicht weiter betrachtet. Somit stellen die realen EIT-Aufzeichnungen den Hauptschwerpunkt dar.\\
	 
	Als Testobjekte dienten Schweine, an denen verschiedene EIT-Datensätze aufgezeichnet wurden. Dafür kam das EIT Pioneer Set von Swisstom zu Einsatz. Das EIT-System ist mit einem Elektrodengürtel ausgestattet, der eine Anzahl von 32 Elektroden bestückt ist. Es arbeitet mit der Skip-4 Pattern-Einspeisungsprotokoll. In das Testobjekt wurde Strom von $I = \SI{3}{mA}$ mit einer Anregungsfrequenz von $f_I = \SI{195}{kHz}$ eingespeist, die Bildrate beträgt dabei $f_s = \SI{47,68}{Hz}$.
	 
	Die EIT-Messung lässt sich unter Anwendung mehrerer Szenarien durchgeführt. Um eine konstanten Atemfrequenz zu gewährleisten, wird das Schwein künstlich beatmet. Wird die Beatmung über einen kurzen Zeitraum angehalten, handelt es sich um eine Apnoe-Messung. Ziel ist es, lediglich den Blutfluss innerhalb des Thorax sichtbar zu machen. Eine Separation des Ventilationssignals müsste dann nicht mehr durchgeführt werden. Im Falle des Atemstillstands verteilt sich allerdings die Luft innerhalb der Lunge um und das Luftvolumen sinkt kontinuierlich. Mittels eines konstanten Lungendrucks können diese Vorgänge ausgeglichen werden, wobei allerdings ein Luftaustausch zwingend erforderlich ist. Des Weiteren besteht, neben dem Szenario der konstanten Beatmung und der Apnoe-Messung, die Möglichkeit eine Arterie, die für die Durchblutung des rechten oder linken Lungenflügels relevant ist, abzutrennen. Es ergeben innerhalb dieses Zeitraums Änderungen der Perfusion innerhalb der Lunge. Diese Änderungen sollen durch Einsatz der EIT ersichtlich werden.
	
	Die Abbildung \ref{fig:4-4_Signal} stellt als Beispiel ein EIT-Signal aus einem Pixel der Lunge innerhalb eines Zeitraums dar. In dem Zeitraum von \SIrange{125}{160}{\second} wurde eine Apnoe-Messung durchgeführt.
	
	\begin{figure}[h]
 	\centering
 	\includegraphics[width=0.7\textwidth, trim={2cm 12cm 1cm 12cm},clip]{4-4_Signal}
 	\caption[Signalverlauf unter Durchführung der Apnoe-Messung]{Signalverlauf eines Pixels aus der Linken Lungenregion unter zeitweiser Durchführung einer Apnoe-Messung}
 	\label{fig:4-4_Signal}
	\end{figure}

	
%	\paragraph{EIT-Messungen mit EKG}
%	Außerdem stehen reale EIT-Messungen, welche aufgezeichnet mit dem EIT Pioneer Set von Swisstom aufgezeichnet wurden, zur Verfügung. Das EIT Pioneer Set arbeitet mit einer Bildrate von $f_s = \SI{47,68}{Hz}$. Die eingespeiste Stromstärke beträgt $I = \SI{3}{mA}$ bei einer Anregungsfrequenz von $f_I = \SI{195}{kHz}$ Die EIT-Messung erfolgt unter der Verwendung von 32 Elektroden mittels \glqq Skip-4 Pattern\grqq-Einspeisung. Als Testobjekte dienten hierbei Schweinen, welche parallel mit einem EKG-Gerät ausgestattet wurden. Das EKG wird zur Bestimmung des genauen Zeitpunkts eines Herzimpulses eingesetzt. Außerdem beinhaltet die zur Verfügung gestellte Datei Messungen mit künstlicher Beatmung und mit angehaltener Atmung. Die einzelnen Bereiche wurden wurden kurz hintereinander aufgenommen. Dabei wurden Messungen mit relativ konstanten Herzfrequenz und Messungen mit stark variierenden Herzfrequenzen aufgezeichnet.
%	
%	Die Abbildung \ref{fig:3-4_EKG} (rechts) stellt die eben beschrieben Signale dar. Dabei werden die aufgezeichneten EIT-Signale für die beatmete Messung (blau) und die Messung unter Atemstillstand (rot) oben dargestellt. Zusätzlich wird das dazu entsprechende EIT-Signal und den als Kreuzen dargestellten Trigger-Zeitpunkte für die EA unten dargestellt. Abbildung \ref{fig:3-4_EKG} (links) Zeit das rekonstruierte Bild der Impedanzverteilung zu einem bestimmten Zeitpunkt. Die Rekonstruktion erfolgt mir der vom Hersteller zur Verfügung gestellten Eidors Software. Diese Software rekonstruiert unter Verwendung des GREIT-Algorithmus das Bild der Impedanzverteilung. 
%	
%%	\begin{figure}[h]
%% 	\centering
%% 		\includegraphics[width=0.7\textwidth, trim={2cm 10.5cm 1cm 10.5cm},clip]{3-4_EKG}
%% 	\caption[Bild von einer beatmeten EIT-Messung inklusive der dazugehörigen Signale mit paralleler EKG-Aufzeichnung]{Rekonstruiertes EIT-Bild einer beatmeten Messung zum Zeitpunkt $t = 24,7 s$ (links). EIT-Messungen (oben) von unter beatmeter (blau) und unbeatmeter (rot) Bedingung, sowie das dazugehörige EKG-Signal des beatmeten Signals (unten)}
%% 	\label{fig:3-4_EKG}
%%	\end{figure}
%	
%	\paragraph{Apnoe-Messungen}
%	Weitere reale Daten -- wieder dienten Schweine als Testobjekte -- zielen auf verschiedene Techniken der Apnoe-Messungen ab. Im ersten Fall wird der Beatmungsschlauch abgeklemmt, wodurch kein Luft mehr in die Lunge einströmen kann. Die noch vorhandene Luft wird aber dadurch in der Lunge umverteilen. Zeitgleich sinkt das Luftvolumen mit der Zeit. Im zweiten Fall wird die Atmung angehalten und zudem der Druck in der Lunge möglichst konstant gehalten. Zum Druckausgleich muss aber zwangsläufig ein Luftaustausch zugelassen werden.
%	
%	Die Abbildung \ref{fig:3-4_Apnoe} stellt die beiden besprochenen EIT-Messungen dar. Dabei entspricht der blauen Signalverlauf dem EIT-Signal unter einfachem Anhalten der Atmung und der rote Verlauf unter Anhalten mit konstantem Lungendruck.
%	
%	\begin{figure}[h]
% 	\centering
% 		\includegraphics[width=0.7\textwidth, trim={2cm 10.5cm 1cm 10.5cm},clip]{3-4_Apnoe}
% 	\caption[Signale verschiedene Messmethoden von Apnoe-Messungen]{Signal einer Apnoe-Messung mittels einfacher angehaltenen Beatmung (blau) und mittels angehaltener Atmung bei konstant gehaltenem Lungendruck (rot)}
% 	\label{fig:3-4_Apnoe}
%	\end{figure}
	
	
	
%	\paragraph{Simuliertes EIT-Signal}
%	Die Simulation der EIT-Messungen wurden mit dem EIT Advanced Interface der Firma Swisstom erstellt \cite{Swisstom}. Die Messungen unterscheiden sich sowohl in Frequenz als auch in Betrag der Signale von Atmung und Herzaktivität. Außerdem kann ein additives Störsignal eingestellt werden. 
%	Die Abbildung \ref{fig:3-4_Simu} (rechts) stellt ein so aufgenommenes Signal mit einer eingestellten Herzfrequenz von $f_{Herz} = \SI{50}{Hz}$ und einer eingestellten Atemfrequenz von $f_{Atmung} = \SI{12}{Hz}$ dar. Beide Amplituden sollen einen laut Hersteller \glqq normalen\grqq Verlauf aufweisen. Die Abbildung \ref{fig:3-4_Simu} (links) stellt das rekonstruierte Bild der Impedanzverteilung zu einem bestimmten Zeitpunkt dar. 
%	
%	\begin{figure}[h]
% 	\centering
% 		\includegraphics[width=0.7\textwidth, trim={2cm 11.5cm 1cm 11.5cm},clip]{3-4_Simu}
% 	\caption[Bild einer simulierten EIT-Messung und ein dazugehöriges Signal]{Rekonstruiertes EIT-Bild zum Zeitpunkt $t = \SI{19,9}{s}$ (links) und der zeitliche Verlauf eines Pixels aus der Lungenregion (blau) und der Herzregion (rot) einer EIT-Messung (rechts)}
% 	\label{fig:3-4_Simu}
%	\end{figure}
	
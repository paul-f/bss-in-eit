%% INIT
if 0
% aktuell sind: 7 (kurz), 8 (apnoe), 12 (Arterie des linken Lungenflügels)
clear
addpath(genpath('C:\Users\Universität\Documents\Masterarbeit\MATLAB\'));
data = chooseEIT; 
end

% clearvars -except data
imgs = data.imgs;

% Festlegen des Signals im Herz (y,x,frame)
%   Mensch:  Lunge (17,10,:), Herz (10,17,:)
%   Schwein: Lunge ( 12,13,:), Herz (18,18,:)

y = 12; x = 13;
frames= 1:length(imgs.raw);
xRAW=       squeeze(imgs.raw(y,x,frames));        xRAW= xRAW-mean(xRAW);
xPCA.ven=   squeeze(imgs.pca.ven(y,x,frames));    xPCA.ven= xPCA.ven-mean(xPCA.ven);
xPCA.car=   squeeze(imgs.pca.car(y,x,frames));    xPCA.car= xPCA.car-mean(xPCA.car);
xICA.ven=   squeeze(imgs.ica.ven(y,x,frames));    xICA.ven=xICA.ven-mean(xICA.ven);
xICA.car=   squeeze(imgs.ica.car(y,x,frames));    xICA.car= xICA.car-mean(xICA.car);
xCOMB.ven=  squeeze(imgs.icapca.ven(y,x,frames)); xCOMB.ven= xCOMB.ven-mean(xCOMB.ven);
xCOMB.car=  squeeze(imgs.icapca.car(y,x,frames)); xCOMB.car= xCOMB.car-mean(xCOMB.car);
xFILT.ven=  squeeze(imgs.filt.ven(y,x,frames));   xFILT.ven= xFILT.ven-mean(xFILT.ven);
xFILT.car=  squeeze(imgs.filt.car(y,x,frames));   xFILT.car= xFILT.car-mean(xFILT.car);
xEA=        squeeze(imgs.ea(y,x,:));
xPCA.comp.ven1=     squeeze(imgs.pca.comp.ven1(y,x,frames));    
xPCA.comp.ven1=     xPCA.comp.ven1-mean(xPCA.comp.ven1);
xPCA.comp.car1=     squeeze(imgs.pca.comp.car1(y,x,frames));    
xPCA.comp.car1=     xPCA.comp.car1-mean(xPCA.comp.car1);
xPCA.comp.car2=     squeeze(imgs.pca.comp.car2(y,x,frames));    
xPCA.comp.car2=     xPCA.comp.car2-mean(xPCA.comp.car2);
xPCA.comp.car2ven= squeeze(imgs.pca.comp.car2ven(y,x,frames));    
xPCA.comp.car2ven= xPCA.comp.car2ven-mean(xPCA.comp.car2ven);

fs=     47.68;
time=   (0:length(xRAW)-1)/fs;

N = length(xRAW);
w = window(@blackman,N);                            % window
[XRAW, fAxis]=      ffts(xRAW.*w,N*10,fs);
[xPCA.fftven, ~]=   ffts(xPCA.ven.*w,N*10,fs);
[xPCA.fftcar, ~]=   ffts(xPCA.car.*w,N*10,fs);
[xICA.fftven, ~]=   ffts(xICA.ven.*w,N*10,fs);
[xICA.fftcar, ~]=   ffts(xICA.car.*w,N*10,fs);
[xCOMB.fftven, ~]=  ffts(xCOMB.ven.*w,N*10,fs);
[xCOMB.fftcar, ~]=  ffts(xCOMB.car.*w,N*10,fs);
[xFILT.fftven, ~]=  ffts(xFILT.ven.*w,N*10,fs);
[xFILT.fftcar, ~]=  ffts(xFILT.car.*w,N*10,fs);
[xPCA.fftcomp.ven1, ~]=     ffts(xPCA.comp.ven1.*w,N*10,fs);
[xPCA.fftcomp.car1, ~]=     ffts(xPCA.comp.car1.*w,N*10,fs);
[xPCA.fftcomp.car2, ~]=     ffts(xPCA.comp.car2.*w,N*10,fs);
[xPCA.fftcomp.car2ven, ~]= ffts(xPCA.comp.car2ven.*w,N*10,fs);
bpmAxis = fAxis*60;

imgRAW=     imgs.raw;
imgPCA=     imgs.pca;
imgICA=     imgs.ica;
imgCOMB=    imgs.icapca;
imgFILT=    imgs.filt;
imgEA=      imgs.ea;


%% SIGNALE
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.294, 0.365, 0.5862]);
% signals
subplot(4,5,1:3)
plot(time,xRAW,':k'); hold on; plot(time,xPCA.ven,'Color',[0, 0.4470, 0.7410])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('[AU]')
title('PCA','Position',[-2.9 -1.75],'HorizontalAlignmen','left');
xlim([0 10])
subplot(4,5,6:8)
plot(time,xRAW,':k'); hold on; plot(time,xICA.ven,'Color',[0, 0.4470, 0.7410])
xlim([0 10])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('[AU]')
title('ICA','Position',[-2.9 -1.75],'HorizontalAlignmen','left');
subplot(4,5,11:13)
plot(time,xRAW,':k'); hold on; plot(time,xCOMB.ven,'Color',[0, 0.4470, 0.7410])
xlim([0 10])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('[AU]')
title('Kombo','Position',[-2.9 -1.75],'HorizontalAlignmen','left');
subplot(4,5,16:18)
plot(time,xRAW,':k'); hold on; plot(time,xFILT.ven,'Color',[0, 0.470, 0.7410])
xlim([0 10])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('[AU]')
title('Filter','Position',[-2.9 -1.75],'HorizontalAlignmen','left');
subplot(4,5,4:5)
plot(bpmAxis,xPCA.fftven(1:length(fAxis)))
xlim([0 150])%;ylim([0 ymax])
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / bpm'); % ylabel('[AU]')
subplot(4,5,9:10)
plot(bpmAxis,xICA.fftven(1:length(fAxis)))
xlim([0 150])%;ylim([0 ymax])
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / bpm'); % ylabel('[AU]')
subplot(4,5,14:15)
plot(bpmAxis,xCOMB.fftven(1:length(fAxis)))
xlim([0 150])%;ylim([0 ymax])
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / bpm'); % ylabel('[AU]')
subplot(4,5,19:20)
plot(bpmAxis,xFILT.fftven(1:length(fAxis)))
xlim([0 150])%;ylim([0 ymax])
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / bpm'); % ylabel('[AU]')

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.294, 0.365, 0.5862]);
% signals
subplot(4,5,1:3)
plot(time,xRAW,':k'); hold on; plot(time,xPCA.car,'Color',[0.6350, 0.0780, 0.1840])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('[AU]')
title('PCA','Position',[-2.9 -1.75],'HorizontalAlignmen','left');
xlim([0 10])
subplot(4,5,6:8)
plot(time,xRAW,':k'); hold on; plot(time,xICA.car,'Color',[0.6350, 0.0780, 0.1840])
xlim([0 10])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('[AU]')
title('ICA','Position',[-2.9 -1.75],'HorizontalAlignmen','left');
subplot(4,5,11:13)
plot(time,xRAW,':k'); hold on; plot(time,xCOMB.car,'Color',[0.6350, 0.0780, 0.1840])
xlim([0 10])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('[AU]')
title('Kombo','Position',[-2.9 -1.75],'HorizontalAlignmen','left');
subplot(4,5,16:18)
plot(time,xRAW,':k'); hold on; plot(time,xFILT.car,'Color',[0.6350, 0.0780, 0.1840])
xlim([0 10])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('[AU]')
title('Filter','Position',[-2.9 -1.75],'HorizontalAlignmen','left');
subplot(4,5,4:5)
plot(bpmAxis,xPCA.fftcar(1:length(fAxis)),'Color',[0.6350, 0.0780, 0.1840])
xlim([0 150])%;ylim([0 ymax])
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / bpm'); % ylabel('[AU]')
subplot(4,5,9:10)
plot(bpmAxis,xICA.fftcar(1:length(fAxis)),'Color',[0.6350, 0.0780, 0.1840])
xlim([0 150])%;ylim([0 ymax])
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / bpm'); % ylabel('[AU]')
subplot(4,5,14:15)
plot(bpmAxis,xCOMB.fftcar(1:length(fAxis)),'Color',[0.6350, 0.0780, 0.1840])
xlim([0 150])%;ylim([0 ymax])
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / bpm'); % ylabel('[AU]')
subplot(4,5,19:20)
plot(bpmAxis,xFILT.fftcar(1:length(fAxis)),'Color',[0.6350, 0.0780, 0.1840])
xlim([0 150])%;ylim([0 ymax])
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / bpm'); % ylabel('[AU]')


%% Global Signal (max - min) [DATA 7]
imgGlobalSig(imgRAW, 'car');
imgGlobalSig(imgPCA.car, 'car');
imgGlobalSig(imgICA.car, 'car');
imgGlobalSig(imgCOMB.car, 'car');
imgGlobalSig(imgFILT.car, 'car');
imgGlobalSig(imgEA, 'car');



%% PCA-Componenten (Xc1_2, Xc2_2)
imgGlobalSig(imgPCA.ven, 'ven'); colorbar
imgGlobalSig(imgPCA.comp.car2ven, 'ven'); colorbar
imgGlobalSig(imgPCA.car, 'car'); colorbar
imgGlobalSig(imgPCA.comp.car2, 'car'); colorbar
%
figure
subplot(3,1,1)
plot(time, xPCA.ven)
xlim([0 10])
subplot(3,1,2)
hold on
plot(time, xPCA.comp.ven1)
plot(time, xPCA.comp.car2ven)      % SIND DAS OBERWELLEN? -> FFT
xlim([0 10])
subplot(3,1,3)
plot(fAxis,xPCA.fftven)
hold on
plot(fAxis,xPCA.fftcomp.car2ven)
xlim([0 2])
%
figure
subplot(2,1,1)
plot(time, xPCA.car)
xlim([0 10])
subplot(2,1,2)
hold on
plot(time, xPCA.comp.car2)
plot(time, xPCA.comp.car2ven)
xlim([0 10])


%% Zeitlicher Verlauf einer Periode [DATA 7]
% Ventilation
imgsPeriod(imgRAW, xRAW, 'ven', 1984, 26, 'row');
imgsPeriod(imgPCA.ven, xPCA.ven, 'ven', 1984, 26, 'row');
imgsPeriod(imgICA.ven, xICA.ven, 'ven', 1984, 26, 'row');
imgsPeriod(imgCOMB.ven, xCOMB.ven, 'ven', 1984, 26, 'row');
imgsPeriod(imgFILT.ven, xFILT.ven, 'ven', 1984, 26, 'row');

% Perfusion
imgsPeriod(imgRAW, xRAW, 'car', 2060, 7, 'row');
imgsPeriod(imgPCA.car, xPCA.car, 'car', 2063, 7, 'row');
imgsPeriod(imgICA.car, xICA.car, 'car', 2062, 5, 'row');
imgsPeriod(imgCOMB.car, xCOMB.car, 'car', 2048, 3, 'row');
imgsPeriod(imgFILT.car, xFILT.car, 'car', 2065, 6, 'row');
imgsPeriod(imgEA, xEA, 'car', 23, 6, 'row');


%% max Ampl.
measureMeth = 'full';
switch measureMeth
    case 'full';    frames = 1:length(imgRAW);
    case 'first';   frames = 1:5000;
    case 'second';  frames = length(imgRAW)/2-2499:length(imgRAW)/2+2500;
    case 'end';     frames = length(imgRAW)-4999:length(imgRAW);
end
% frames = 6001:7500;
% for fac = (1:10)/10

fac = 1;
for x = 1:size(imgRAW,1)
    for y = 1:size(imgRAW,2)
        maxRaw(y,x) = max(imgRAW(y,x,frames));
        maxVen(y,x) = max(imgPCA.ven(y,x,frames));
        maxCar(y,x) = max(imgPCA.car(y,x,frames));
        minRaw(y,x) = min(imgRAW(y,x,frames));
        minVen(y,x) = min(imgPCA.ven(y,x,frames));
        minCar(y,x) = min(imgPCA.car(y,x,frames));
    end
end

maxRaw= maxRaw - min(min(maxRaw));
maxVen= maxVen - min(min(maxVen));
maxCar= maxCar - min(min(maxCar));
minRaw= minRaw - min(min(minRaw));
minVen= minVen - min(min(minVen));
minCar= minCar - min(min(minCar));

for x = 1:size(imgRAW,1)
    for y = 1:size(imgRAW,2)
        if maxRaw(y,x) >= max(max(maxRaw))*fac
            maxRaw(y,x)= max(max(maxRaw))*fac;
        end
        if maxVen(y,x) >= max(max(maxVen))*fac
            maxVen(y,x)= max(max(maxVen))*fac;
        end
        if maxCar(y,x) >= max(max(maxCar))*fac
            maxCar(y,x)= max(max(maxCar))*fac;
        end
    end
end

%
figure
subplot(1,3,1)
showimg(maxVen,'maxcar0')%,'off')
title('max ventilation')
subplot(1,3,2)
showimg(maxRaw,'maxcar0','off')
title('max raw')
subplot(1,3,3)
showimg(maxCar,'maxcar0','off')
title('max cardiac')
% end

%%
figure
subplot(2,3,1); showimg(maxVen)
subplot(2,3,2); showimg(maxRaw)
subplot(2,3,3); showimg(maxCar)
subplot(2,3,4); showimg(-minVen)
subplot(2,3,5); showimg(-minRaw)
subplot(2,3,6); showimg(-minCar)
colormap hot


%% global Sig
maxValue = 0;
minValue = 0;
for x = 1:size(imgRAW,1)
    for y = 1:size(imgRAW,2)
        [maxValue_, maxM] = max(imgRAW(y,x,:));
        if maxValue_ > maxValue
            maxValue = maxValue_;
            imgMax = maxM;
        end
        [minValue_, minM] = min(imgRAW(y,x,:));
        if minValue_ < minValue
            minValue = minValue_;
            imgMin = minM;
        end
    end 
end
clear maxM minM maxValue_ minValue_

figure
subplot(1,3,1)
imagesc(imgRAW(:,:,imgMax))
axis square
subplot(1,3,2)
imagesc(imgRAW(:,:,imgMin))
axis square
subplot(1,3,3)
imagesc(imgRAW(:,:,imgMax)+imgRAW(:,:,imgMin))
axis square


%% global Sig (HIER MAL MIT APNOE)

globalRaw = squeeze(nansum(nansum(imgRAW ,2),1)); globalRaw = globalRaw - mean(globalRaw);
globalVen = squeeze(nansum(nansum(imgPCA.ven,2),1)); globalVen = globalVen - mean(globalVen);
globalCar = squeeze(nansum(nansum(imgPCA.car,2),1)); globalCar = globalCar - mean(globalCar);

figure
plot(globalRaw)
hold on 
plot(globalVen)
plot(globalCar)
xlim([0 483])
%
[~,maxposV] = max(globalVen); [~,minposV] = min(globalVen);
figure
subplot(1,3,1)
showimg(imgPCA.ven(:,:,maxposV))
% colorbar
caxis([-6.5 4.5])
set(gca, 'xtick',[], 'ytick',[])
title('max ventilation')
subplot(1,3,2)
showimg(imgPCA.ven(:,:,maxposV)-(imgPCA.ven(:,:,minposV)))
caxis([-6.5 4.5])
% colorbar
% set(gca, 'xtick',[], 'ytick',[])
title('max - min')
subplot(1,3,3)
showimg((imgPCA.ven(:,:,minposV)))
caxis([-6.5 4.5])
% colorbar
colormap(colormap(flipud(setcolormap('car1'))))
set(gca, 'xtick',[], 'ytick',[])
title('min ventilation')


[~,maxposC] = max(globalCar); [~,minposC] = min(globalCar);
figure
subplot(1,3,1)
showimg(imgPCA.car(:,:,maxposC))
% colorbar
caxis([-3.5 2.5])
% set(gca, 'xtick',[], 'ytick',[])
title('max cardiac')
subplot(1,3,2)
showimg(imgPCA.car(:,:,maxposC)-(imgPCA.car(:,:,minposC)))
caxis([-3.5 2.5])
% colorbar
set(gca, 'xtick',[], 'ytick',[])
title('max - min')
subplot(1,3,3)
showimg((imgPCA.car(:,:,minposC)))
caxis([-3.5 2.5])
% colorbar
colormap(colormap(flipud(setcolormap('car1'))))
set(gca, 'xtick',[], 'ytick',[])
title('min cardiac')

%% (DATA 7)
imgsPeriod(imgRAW, xRAW, 'car', 1021, 3);
imgsPeriod(imgPCA.car, xPCA.car, 'car', 1020, 3);
imgsPeriod(imgICA.car, xICA.car, 'car', 1013, 4);
imgsPeriod(imgCOMB.car, xCOMB.car, 'car', 1021, 3);
imgsPeriod(imgFILT.car, xFILT.car, 'car', 1015, 5);
imgsPeriod(imgEA, xEA, 'car', 10, 3);

%% APNOE (DATA 8)
imgsPeriod(imgRAW, xRAW, 'car', 6462, 5);
imgsPeriod(imgPCA.car, xPCA.car, 'car', 6466, 3);
imgsPeriod(imgICA.car, xICA.car, 'car', 6468, 4);
imgsPeriod(imgCOMB.car, xCOMB.car, 'car', 6466, 3);



%% Arterie dicht (DATA 12)
% PCA
% links dicht
figure
subplot(2,1,1)
plot(xPCA.car)                                      % links
subplot(2,1,2)
plot(squeeze(imgs.pca.car(15,22,frames)) ...        % rechts
    -mean(squeeze(imgs.pca.car(15,22,frames))))
imgsPeriod(imgPCA.car, xPCA.car, 'car', 1019, 3);
imgsPeriod(imgPCA.car, xPCA.car, 'car', 11127, 3);
%%
% imgsPeriod(imgPCA.ven, xPCA.ven, 'ven', 9781, 27);   % DATA 6
imgsPeriod(imgRAW, xRAW, 'ven', 1480, 29);           % DATA 7
imgsPeriod(imgPCA.ven, xPCA.ven, 'ven', 1984, 26);   % DATA 7
imgsPeriod(imgICA.ven, xICA.ven, 'ven', 1984, 26);   % DATA 7
imgsPeriod(imgCOMB.ven, xCOMB.ven, 'ven', 1984, 26); % DATA 7
imgsPeriod(imgFILT.ven, xFILT.ven, 'ven', 1984, 26); % DATA 7
% imgsPeriod(imgPCA.ven, xPCA.ven, 'ven', 989, 30);    % DATA 12

%% ICA
% links dicht
figure
subplot(2,1,1)
plot(xICA.car)                                      % links
ylim([-1.5 1.5])
subplot(2,1,2)
plot(squeeze(imgs.ica.car(17,21,frames)) ...        % rechts
    -mean(squeeze(imgs.ica.car(17,21,frames))))
ylim([-1.5 1.5])

imgsPeriod(imgICA.car, xICA.car, 'car', 1021, 3);
imgsPeriod(imgICA.car, xICA.car, 'car', 11129, 3);

%% COMB
% links dicht
figure
subplot(2,1,1)
plot(xCOMB.car)                                      % links
ylim([-1.75 1.75])
subplot(2,1,2)
plot(squeeze(imgs.icapca.car(17,21,frames)) ...      % rechts
    -mean(squeeze(imgs.icapca.car(17,21,frames))))
ylim([-1.75 1.75])

imgsPeriod(imgCOMB.car, xCOMB.car, 'car', 1020, 3);
imgsPeriod(imgCOMB.car, xCOMB.car, 'car', 11129, 3);


%% video (vllt. für Verteidigung)
% % Quelle: https://de.mathworks.com/matlabcentral/answers/453364-create-a-video-with-imagesc-videowriter
% video = VideoWriter('newfile.avi', 'Uncompressed AVI'); 
% open(video)
% for k = 1:length(imgs.raw)
% %    .%calculations
% %    .%calculations
% %    W=reshape(w,y,x); %Where W is a Matrix
% %    lsd=imagesc(imgs.raw);
% %    drawnow
%    F(k) = getframe(imgs.raw(:,:,k));
% %    writeVideo(video,F)
% end
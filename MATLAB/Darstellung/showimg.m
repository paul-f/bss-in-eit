function showimg(img, colorm, tick, varargin)

pcolor(img')
camroll(-90)
axis square
shading interp

if exist('colorm','var') == 1
    colormap(colormap(flipud(setcolormap(colorm))))
end

if exist('tick','var') == 1
    set(gca, 'xtick',[], 'ytick',[])
end

end
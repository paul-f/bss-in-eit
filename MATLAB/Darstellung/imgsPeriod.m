function imgsPeriod(img, sig, comp, frame, frameRate, mod)
%%% Durchblutung
% frame = 8; % DATA 7 (EA)
% frame = 1019; % DATA 7
% frame = 6324; % DATA 8
% frame = 1997; % DATA 12 (normal)
% frame = 8091; % DATA 12 (links ab)
% frame = 54; % DATA 12 (EA)

x = 4; y = 1;
cMin = min(img(:,:,frame:frame+frameRate*x*y-1),[],'all');
cMax = max(img(:,:,frame:frame+frameRate*x*y-1),[],'all');
cLim = [cMin cMax];

switch mod
    case 'all'
        figure
        subplot(2,x+1,x+2:2*(x+1))
        % % subplot(1,2,1)
        plot(sig(frame:frame+frameRate*x*y-1))
        plot
        % subplot(1,2,2)
        % plot(xCar(frame:frame+frameRateCar*x*y-1))
        
        switch comp
            case 'ven'
                for k = 1:x*y+1
                    subplot(2,x+1,k)
                    showimg((img(:,:,frame+frameRate*(k-1))-mean(cLim)))
                    caxis([1*cMin 1*cMax])
                    set(gca, 'xtick',[], 'ytick',[])
                    colormap(flipud(setcolormap('ven')))
                end
            case 'car'
                for k = 1:x*y+1
                    subplot(2,x+1,k)
                    showimg((img(:,:,frame+frameRate*(k-1))-mean(cLim)))
                    caxis([cMin 1.2*cMax])
                    set(gca, 'xtick',[], 'ytick',[])
                    colormap(flipud(setcolormap('car')))
                end
        end
        
    case 'only'
        figure
        switch comp
            case 'ven'
                for k = 1:x*y+1
                    subplot(1,x+1,k)
                    showimg((img(:,:,frame+frameRate*(k-1))-mean(cLim)))
                    caxis([1*cMin 1*cMax])
                    set(gca, 'xtick',[], 'ytick',[])
                    colormap(flipud(setcolormap('ven')))
                end
            case 'car'
                for k = 1:x*y+1
                    subplot(1,x+1,k)
                    showimg((img(:,:,frame+frameRate*(k-1))-mean(cLim)))
                    caxis([cMin 1.2*cMax])
                    set(gca, 'xtick',[], 'ytick',[])
                    colormap(flipud(setcolormap('car')))
                end
        end
        
    case 'row'
        figure
        set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*1.7, 0.4862/2]);
        switch comp
            case 'ven'
                x = 4; y = 1;
                subplot(1,x+2,x+2)
                plot(sig(frame:frame+frameRate*x*y), ...
                    'Color',[0, 0.4470, 0.7410])
                hold on
                for points = 1:(x+1)
                    plot(frameRate*(points-1)+1, ...
                        sig(frame+frameRate*(points-1)), ...
                        'Color', [0, 0.4470, 0.7410], ...
                        'Marker', '.', 'MarkerSize', 15)
                end
                xlim([1 frameRate*x+1])
                axis square
                set(gca, 'FontName', 'Times New Roman')
                xlabel('Rahmen / s^{-1}'); ylabel('Amplitude / AU')
                for k = 1:x*y+1
                    subplot(1,x+2,k)
                    showimg((img(:,:,frame+frameRate*(k-1))-mean(cLim)))
                    caxis([1*cMin 1*cMax])
                    set(gca, 'xtick',[], 'ytick',[])
                    colormap(flipud(setcolormap('ven')))
                end
            case 'car'
                x = 4; y = 1;
                subplot(1,x+2,x+2)
                plot(sig(frame:frame+frameRate*x*y), ...
                    'Color',[0.6350, 0.0780, 0.1840])
                hold on
                for points = 1:(x+1)
                    plot(frameRate*(points-1)+1, ...
                        sig(frame+frameRate*(points-1)), ...
                        'Color', [0.6350, 0.0780, 0.1840], ...
                        'Marker', '.', 'MarkerSize', 15)
                end
                xlim([1 frameRate*x+1])
                axis square
                set(gca, 'FontName', 'Times New Roman')
                xlabel('Rahmen / s^{-1}'); ylabel('Amplitude / AU')
                for k = 1:x*y+1
                    subplot(1,x+2,k)
                    showimg((img(:,:,frame+frameRate*(k-1))-mean(cLim)))
                    caxis([cMin 1.2*cMax])
                    set(gca, 'xtick',[], 'ytick',[])
                    colormap(flipud(setcolormap('car')))
                end
        end
end

% for t = 1:size(imgsVen,3)
%     for x = 1:size(imgs.raw,1)
%         for y = 1:size(imgs.raw,2)
% %             [val, maxM] = max(imgsVen(y,x,t));
%             if imgsVen(y,x,t) < 0
%                 imgsVen(y,x,t) = 0;
%             end
%         end
%     end
% end
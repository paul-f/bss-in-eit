%% INIT
if 0
    % aktuell sind: 7 (kurz), 8 (apnoe), 12 (Arterie des linken Lungenflügels)
    clear
    addpath(genpath('C:\Users\Universität\Documents\Masterarbeit\MATLAB\'));
    data = chooseEIT;
end

% clearvars -except data
imgs = data.imgs;

% Festlegen des Signals im Herz (y,x,frame)
%   Mensch:  Lunge (17,10,:), Herz (10,17,:)
%   Schwein: Lunge ( 12,13,:), Herz (18,18,:)

y = 12; x = 13;
xRAW=           squeeze(imgs.raw(y,x,:));
xRAW=           xRAW-mean(xRAW);

fs=             47.68;
time=           (0:length(xRAW)-1)/fs;
N =             length(xRAW);
w =             window(@blackman,N);                            % window
[XRAW, fAxis]=  ffts(xRAW.*w,N*10,fs);
bpmAxis=        fAxis*60;

imgRAW=         imgs.raw;

if isfield(imgs,'pca') == 1
    xPCA.ven=           squeeze(imgs.pca.ven(y,x,:));
    xPCA.ven=           xPCA.ven-mean(xPCA.ven);
    xPCA.car=           squeeze(imgs.pca.car(y,x,:));
    xPCA.car=           xPCA.car-mean(xPCA.car);
    N =                 length(xPCA.ven);
    w =                 window(@blackman,N);
    [xPCA.fftven, ~]=   ffts(xPCA.ven.*w,N*10,fs);
    [xPCA.fftcar, ~]=   ffts(xPCA.car.*w,N*10,fs);
    imgPCA=             imgs.pca;
    if isfield(imgs.pca, 'comp') == 1
        xPCA.comp.ven1=     squeeze(imgs.pca.comp.ven1(y,x,:));
        xPCA.comp.ven1=     xPCA.comp.ven1-mean(xPCA.comp.ven1);
        xPCA.comp.car1=     squeeze(imgs.pca.comp.car1(y,x,:));
        xPCA.comp.car1=     xPCA.comp.car1-mean(xPCA.comp.car1);
        xPCA.comp.car2=     squeeze(imgs.pca.comp.car2(y,x,:));
        xPCA.comp.car2=     xPCA.comp.car2-mean(xPCA.comp.car2);
        xPCA.comp.car2ven=  squeeze(imgs.pca.comp.car2ven(y,x,:));
        xPCA.comp.car2ven=  xPCA.comp.car2ven-mean(xPCA.comp.car2ven);
        N =                         length(xPCA.comp.ven1);
        w =                         window(@blackman,N);
        [xPCA.fftcomp.ven1, ~]=     ffts(xPCA.comp.ven1.*w,N*10,fs);
        [xPCA.fftcomp.car1, ~]=     ffts(xPCA.comp.car1.*w,N*10,fs);
        [xPCA.fftcomp.car2, ~]=     ffts(xPCA.comp.car2.*w,N*10,fs);
        [xPCA.fftcomp.car2ven, ~]=  ffts(xPCA.comp.car2ven.*w,N*10,fs);
    end
end

if isfield(imgs,'ica') == 1
    xICA.ven=           squeeze(imgs.ica.ven(y,x,:));
    xICA.ven=           xICA.ven-mean(xICA.ven);
    xICA.car=           squeeze(imgs.ica.car(y,x,:));
    xICA.car=           xICA.car-mean(xICA.car);
    N =                 length(xICA.ven);
    w =                 window(@blackman,N);
    [xICA.fftven, ~]=   ffts(xICA.ven.*window(@blackman,length(xICA.car)),length(xICA.car)*10,fs);
    [xICA.fftcar, ~]=   ffts(xICA.car.*window(@blackman,length(xICA.car)),length(xICA.car)*10,fs);
    imgICA=             imgs.ica;
end

if isfield(imgs,'icapca') == 1
    xCOMB.ven=          squeeze(imgs.icapca.ven(y,x,:));
    xCOMB.ven=          xCOMB.ven-mean(xCOMB.ven);
    xCOMB.car=          squeeze(imgs.icapca.car(y,x,:));
    xCOMB.car=          xCOMB.car-mean(xCOMB.car);
    N =                 length(xCOMB.ven);
    w =                 window(@blackman,N);
    [xCOMB.fftven, ~]=  ffts(xCOMB.ven.*w,N*10,fs);
    [xCOMB.fftcar, ~]=  ffts(xCOMB.car.*w,N*10,fs);
    imgCOMB=            imgs.icapca;
end

if isfield(imgs,'filt') == 1
    xFILT.ven=          squeeze(imgs.filt.ven(y,x,:));
    xFILT.ven=          xFILT.ven-mean(xFILT.ven);
    xFILT.car=          squeeze(imgs.filt.car(y,x,:));
    xFILT.car=          xFILT.car-mean(xFILT.car);
    N =                 length(xFILT.ven);
    w =                 window(@blackman,N);
    [xFILT.fftven, ~]=  ffts(xFILT.ven.*w,N*10,fs);
    [xFILT.fftcar, ~]=  ffts(xFILT.car.*w,N*10,fs);
    imgFILT=            imgs.filt;
end

if isfield(imgs,'ea') == 1
    xEA=                squeeze(imgs.ea(y,x,:));
    imgEA=              imgs.ea;
end



%% Apnoe [DATA 8] (Vgl. Perfusionskomponente mit Atmung vs. Raw Apnoe)
time1 = 140.1; time2 = 145.1;
apnoe_= find(time<=time2); apnoe_= find(time(apnoe_)>=time1);
xRAWap = xRAW - mean(xRAW(apnoe_));
xRAWHeart=      squeeze(imgs.raw(18,18,:));
xRAWHeart=      xRAWHeart-mean(xRAWHeart(apnoe_)); % Wie Pixel aus Lunge bestimmen? Kann es nicht genau Vorhersagen
xPCA.carHeart=	squeeze(imgs.pca.car(18,18,:));
xPCA.carHeart=	xPCA.carHeart-mean(xPCA.carHeart);
xICA.carHeart=	squeeze(imgs.ica.car(18,18,:));
xICA.carHeart=	xICA.carHeart-mean(xICA.carHeart);
xCOMB.carHeart=	squeeze(imgs.icapca.car(18,18,:));
xCOMB.carHeart=	xCOMB.carHeart-mean(xCOMB.carHeart);
xFILT.carHeart= squeeze(imgs.filt.car(18,18,:));
xFILT.carHeart=	xFILT.carHeart-mean(xFILT.carHeart);
%
time1 = 125; time2 = 160;
apnoe= find(time<=time2); apnoe= find(time(apnoe)>=time1);
Nap = length(apnoe);
wap = window(@blackman,Nap);
[XRAWap, fAxisap]=  ffts((xRAW(apnoe)-mean(xRAW(apnoe))).*wap,Nap*10,fs);
[XRAWapHeart, fAxisap]=  ffts((xRAWHeart(apnoe)-mean(xRAWHeart(apnoe))).*wap,Nap*10,fs);

[xPCA.fftcarHeart, fAxisven]=   ffts(xPCA.carHeart.*window(@blackman,length(xPCA.carHeart)),length(xPCA.carHeart)*10,fs);
[xICA.fftcarHeart, ~]=   ffts(xICA.carHeart.*window(@blackman,length(xICA.carHeart)),length(xICA.carHeart)*10,fs);
[xCOMB.fftcarHeart, ~]=   ffts(xCOMB.carHeart.*window(@blackman,length(xCOMB.carHeart)),length(xCOMB.carHeart)*10,fs);
[xFILT.fftcarHeart, ~]=   ffts(xFILT.carHeart.*window(@blackman,length(xFILT.carHeart)),length(xFILT.carHeart)*10,fs);

% [XRAWAp.fftven, ~]=  ffts(xRAW*w,N*10,fs);
% (1:length())
figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.194, 0.365, 0.6862]);
subplot(5,5,1:3)
plot(time,xRAWap); hold on; plot(time,xRAWHeart); xlim([time1 time2])
xlim([140.1 145.1])
ax = gca;
ax.YAxis.Exponent = -1;
set(gca, 'FontName', 'Times New Roman')
title('Apnoe','Position',[138.65 -0.4 1],'HorizontalAlignmen','left');
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,5,4:5)
plot(fAxisap,XRAWap)
hold on 
plot(fAxisap,XRAWapHeart)
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlim([0 2.5])%;ylim([0 ymax])
xlabel('Frequenz / Hz'); % ylabel('[AU]')
subplot(5,5,6:8)
plot(time(1:length(xPCA.car)),xPCA.car); hold on; plot(time(1:length(xPCA.car)),xPCA.carHeart); xlim([100 105])
set(gca, 'FontName', 'Times New Roman')
title('PCA','Position',[98.55 -0.2 1],'HorizontalAlignmen','left');
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,5,9:10)
plot(fAxisven,xPCA.fftcar)
hold on 
plot(fAxisven,xPCA.fftcarHeart)
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlim([0 2.5])%;ylim([0 ymax])
xlabel('Frequenz / Hz'); % ylabel('[AU]')
subplot(5,5,11:13)
plot(time(1:length(xICA.carHeart)),xICA.car); hold on; plot(time(1:length(xICA.carHeart)),xICA.carHeart); xlim([100 105])
set(gca, 'FontName', 'Times New Roman')
title('ICA','Position',[98.55 -0.05 1],'HorizontalAlignmen','left');
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,5,14:15)
plot(fAxisven,xICA.fftcar)
hold on 
plot(fAxisven,xICA.fftcarHeart)
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlim([0 2.5])%;ylim([0 ymax])
xlabel('Frequenz / Hz'); % ylabel('[AU]')
subplot(5,5,16:18)
plot(time(1:length(xCOMB.car)),xCOMB.car); hold on;plot(time(1:length(xCOMB.car)),xCOMB.carHeart); xlim([100 105])
set(gca, 'FontName', 'Times New Roman')
title('Combi','Position',[98.55 -0.2 1],'HorizontalAlignmen','left');
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,5,19:20)
plot(fAxisven,xCOMB.fftcar)
hold on 
plot(fAxisven,xCOMB.fftcarHeart)
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlim([0 2.5])%;ylim([0 ymax])
xlabel('Frequenz / Hz'); % ylabel('[AU]')
subplot(5,5,21:23)
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')
plot(time(1:length(xFILT.car)),xFILT.car); hold on; plot(time(1:length(xFILT.car)),xFILT.carHeart); xlim([100 105])
set(gca, 'FontName', 'Times New Roman')
title('Filter','Position',[98.55 -0.175 1],'HorizontalAlignmen','left');
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,5,24:25)
plot(fAxisven,xFILT.fftcar)
hold on 
plot(fAxisven,xFILT.fftcarHeart)
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlim([0 2.5])%;ylim([0 ymax])
xlabel('Frequenz / Hz'); % ylabel('[AU]')


%
% frameAp = apnoe(1)+90;
% frameCar = 4854;
% frameRate = 11;
% 
% imgsPeriod(imgRAW, xRAWap, 'car', frameAp, frameRate, 'row');
% imgsPeriod(imgPCA.car, xPCA.car, 'car', frameCar, frameRate, 'row');
% imgsPeriod(imgICA.car, xICA.car, 'car', frameCar, frameRate, 'row');
% imgsPeriod(imgCOMB.car, xCOMB.car, 'car', frameCar, frameRate, 'row');
% imgsPeriod(imgFILT.car, xFILT.car, 'car', frameCar, frameRate, 'row');
% 
% % max - min 
% imgGlobalSig(imgRAW, 'car');
% imgGlobalSig(imgPCA.car, 'car');
% imgGlobalSig(imgICA.car, 'car');
% imgGlobalSig(imgCOMB.car, 'car');
% imgGlobalSig(imgFILT.car, 'car');

%% Apnoe [DATA 8] (Vgl. PCA-Komponenten Xc1 Xc2 bei Apnoe vs. Raw Apnoe?)
imgGlobalSig(imgRAW(:,:,6001:7600), 'apnoe');

%% Ventilation

[xPCA.comp.fftven1, fAxisComp]=   ffts((xPCA.comp.ven1).*window(@blackman,length(xPCA.comp.ven1)),length(xPCA.comp.ven1)*10,fs);
[xPCA.comp.fftcar2ven, ~]=   ffts((xPCA.comp.car2ven).*window(@blackman,length(xPCA.comp.car2ven)),length(xPCA.comp.car2ven)*10,fs);
[xPCA.comp.fftcarinlung, ~]=   ffts((xPCA.comp.car2ven+xPCA.comp.ven1).*window(@blackman,length(xPCA.comp.car2ven)),length(xPCA.comp.car2ven)*10,fs);

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.294, 0.365, 0.5862]);
subplot(5,5,1:3)
plot(time(1:length(xPCA.comp.ven1)),xPCA.comp.ven1,'Color',[0.6350, 0.0780, 0.1840])
% hold on
% plot(time(1:length(xPCA.comp.ven1)),xPCA.comp.car2ven)
xlim([0 5])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,5,4:5)
plot(fAxisComp,xPCA.comp.fftven1,'Color',[0.6350, 0.0780, 0.1840])
% hold on
% plot(fAxisComp,xPCA.comp.fftcar2ven)
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlim([0 2.5])%;ylim([0 ymax])
xlabel('Frequenz / Hz'); % ylabel('[AU]')

imgGlobalSig(imgPCA.comp.ven1, 'apnoe'); 
% imgGlobalSig(imgPCA.comp.car2ven, 'apnoe');           % Xc2_2: Verbleibende Perfusionskomponente in Ventilation Xv_1 (Xv = Xv_1 - Xc2_2)

% figure
% subplot(5,5,1:3)
% plot(time(1:length(xPCA.comp.ven1)),xPCA.comp.ven1+xPCA.comp.car2ven)
% xlim([0 5])
% set(gca, 'FontName', 'Times New Roman')
% xlabel('Zeit / s'); ylabel('Amplitude / AU')
% subplot(5,5,4:5)
% plot(fAxisComp,xPCA.comp.fftcarinlung)
% ax = gca;
% ax.YAxis.Exponent = -2;
% set(gca, 'FontName', 'Times New Roman')
% xlim([0 2.5])%;ylim([0 ymax])
% xlabel('Frequenz / Hz'); % ylabel('[AU]')
% 
% imgGlobalSig(imgPCA.comp.ven1+imgPCA.comp.car2ven, 'apnoe');      % Ventilation

% imgsPeriod(imgPCA.comp.ven1+imgPCA.comp.car2ven,xPCA.comp.ven1+xPCA.comp.car2ven, 'ven', 14,11,'row')
%% Perfusion
[xPCA.comp.fftcar1, fAxisComp]=   ffts((xPCA.comp.car1).*window(@blackman,length(xPCA.comp.ven1)),length(xPCA.comp.car1)*10,fs);
[xPCA.comp.fftcar2, ~]=   ffts((xPCA.comp.car2).*window(@blackman,length(xPCA.comp.car2)),length(xPCA.comp.car2)*10,fs);
[xPCA.comp.fftcarinheart, ~]=   ffts((xPCA.comp.car2+xPCA.comp.car1).*window(@blackman,length(xPCA.comp.car2ven)),length(xPCA.comp.car2ven)*10,fs);

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.294, 0.365, 0.5862]);
subplot(5,5,1:3)
plot(time(1:length(xPCA.comp.car1)),xPCA.comp.car1,'Color',[0.6350, 0.0780, 0.1840])
% hold on
% plot(time(1:length(xPCA.comp.car2)),xPCA.comp.car2)
xlim([0 5])
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,5,4:5)
plot(fAxisComp,xPCA.comp.fftcar1,'Color',[0.6350, 0.0780, 0.1840])
% hold on
% plot(fAxisComp,xPCA.comp.fftcar2)
ax = gca;
ax.YAxis.Exponent = -2;
set(gca, 'FontName', 'Times New Roman')
xlim([0 2.5])%;ylim([0 ymax])
xlabel('Frequenz / Hz'); % ylabel('[AU]')

imgGlobalSig(imgPCA.comp.car1, 'apnoe');              % Xc1_2: 2nd Näherung der Perfusionskomponente Xc (Xc = Xc1_1 + Xc2_2)
% imgGlobalSig(imgPCA.comp.car2, 'apnoe');           % Xc2_2: Verbleibende Perfusionskomponente in Ventilation Xv_1 (Xv = Xv_1 - Xc2_2)
% 
% 
% figure
% subplot(5,5,1:3)
% plot(time(1:length(xPCA.comp.car1)),xPCA.comp.car1+xPCA.comp.car2)
% xlim([0 5])
% set(gca, 'FontName', 'Times New Roman')
% xlabel('Zeit / s'); ylabel('Amplitude / AU')
% subplot(5,5,4:5)
% plot(fAxisComp,xPCA.comp.fftcarinheart)
% ax = gca;
% ax.YAxis.Exponent = -2;
% set(gca, 'FontName', 'Times New Roman')
% xlim([0 2.5])%;ylim([0 ymax])
% xlabel('Frequenz / Hz'); % ylabel('[AU]')
% %
% imgGlobalSig(imgPCA.comp.car1+imgPCA.comp.car2, 'apnoe');      % Perfusion


%% Signale
figure
plot(xRAW(6001:7600)-mean(xRAW(6001:7600))+0.5)
hold on
plot(xPCA.comp.car2)
plot(xPCA.comp.car2ven)
xlim([0 500])
legend('Apnoe','2. Näherung der Perfusionskomponente Xc','Verbleibende Perfusionskomponente in Ventilation Xv_1')

%% Arterienverschluss [DATA 12 (links)] (nur Signale oder auch Bilder?)

non = find(time<=124);
blocked = find(time>=124); blocked =find(find(time<=240)>=blocked(1));

%RAW
figure
[globalL, globalR] = splitLung(imgRAW);
ymax = max(max([globalL globalR]));
ymin = min(min([globalL globalR]));
subplot(5,4,1:2)
plot(time(non),globalL(non))
hold on
plot(time(blocked),globalL(blocked))
xlim([0 240]); ylim([ymin ymax])
ax = gca;
ax.YAxis.Exponent = 2;
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,4,3:4)
plot(time(non),globalR(non))
hold on
plot(time(blocked),globalR(blocked))
xlim([0 240]); ylim([ymin ymax])
ax = gca;
ax.YAxis.Exponent = 2;
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')


% Quantifizieren
[art.raw.LnonMin, art.raw.LnonMax] = meanAmp(globalL, non, 100);
[art.raw.LblockedMin, art.raw.LblockedMax] = meanAmp(globalL, blocked, 100);
[art.raw.RnonMin, art.raw.RnonMax] = meanAmp(globalR, non, 100);
[art.raw.RblockedMin, art.raw.RblockedMax] = meanAmp(globalR, blocked, 100);
[art.raw.LnonDiff, art.raw.LblockedDiff, art.raw.Lrel] = ...
    relDiff(art.raw.LnonMax, art.raw.LnonMin, ...
    art.raw.LblockedMax, art.raw.LblockedMin);
[art.raw.RnonDiff, art.raw.RblockedDiff, art.raw.Rrel] = ...
    relDiff(art.raw.RnonMax, art.raw.RnonMin, ...
    art.raw.RblockedMax, art.raw.RblockedMin);

% PCA
figure
[globalL, globalR] = splitLung(imgPCA.car);
ymax = max(max([globalL globalR]));
ymin = min(min([globalL globalR]));
subplot(5,4,1:2)
plot(time(non),globalL(non))
hold on
plot(time(blocked),globalL(blocked))
xlim([0 240]); ylim([ymin ymax])
ax = gca;
ax.YAxis.Exponent = 2;
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,4,3:4)
plot(time(non),globalR(non))
hold on
plot(time(blocked),globalR(blocked))
xlim([0 240]); ylim([ymin ymax])
ax = gca;
ax.YAxis.Exponent = 2;
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')

[art.pca.LnonMin, art.pca.LnonMax] = meanAmp(globalL, non, 50);
[art.pca.LblockedMin, art.pca.LblockedMax] = meanAmp(globalL, blocked, 50);
[art.pca.RnonMin, art.pca.RnonMax] = meanAmp(globalR, non, 30);
[art.pca.RblockedMin, art.pca.RblockedMax] = meanAmp(globalR, blocked, 30);
[art.pca.LnonDiff, art.pca.LblockedDiff, art.pca.Lrel] = ...
    relDiff(art.pca.LnonMax, art.pca.LnonMin, ...
    art.pca.LblockedMax, art.pca.LblockedMin);
[art.pca.RnonDiff, art.pca.RblockedDiff, art.pca.Rrel] = ...
    relDiff(art.pca.RnonMax, art.pca.RnonMin, ...
    art.pca.RblockedMax, art.pca.RblockedMin);


% ICA
figure
[globalL, globalR] = splitLung(imgICA.car);
ymax = max(max([globalL globalR]));
ymin = min(min([globalL globalR]));
subplot(5,4,1:2)
plot(time(non),globalL(non))
hold on
plot(time(blocked),globalL(blocked))
xlim([0 240]); ylim([ymin ymax])
ax = gca;
ax.YAxis.Exponent = 1;
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,4,3:4)
plot(time(non),globalR(non))
hold on
plot(time(blocked),globalR(blocked))
xlim([0 240]); ylim([ymin ymax])
ax = gca;
ax.YAxis.Exponent = 1;
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')

[art.ica.LnonMin, art.ica.LnonMax] = meanAmp(globalL, non, 35);
[art.ica.LblockedMin, art.ica.LblockedMax] = meanAmp(globalL, blocked, 35);
[art.ica.RnonMin, art.ica.RnonMax] = meanAmp(globalR, non, 30);
[art.ica.RblockedMin, art.ica.RblockedMax] = meanAmp(globalR, blocked, 30);
[art.ica.LnonDiff, art.ica.LblockedDiff, art.ica.Lrel] = ...
    relDiff(art.ica.LnonMax, art.ica.LnonMin, ...
    art.ica.LblockedMax, art.ica.LblockedMin);
[art.ica.RnonDiff, art.ica.RblockedDiff, art.ica.Rrel] = ...
    relDiff(art.ica.RnonMax, art.ica.RnonMin, ...
    art.ica.RblockedMax, art.ica.RblockedMin);

% COMB
figure
[globalL, globalR] = splitLung(imgCOMB.car);
ymax = max(max([globalL globalR]));
ymin = min(min([globalL globalR]));
subplot(5,4,1:2)
plot(time(non),globalL(non))
hold on
plot(time(blocked),globalL(blocked))
xlim([0 240]); ylim([ymin ymax]/20)
ax = gca;
ax.YAxis.Exponent = 1;
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,4,3:4)
plot(time(non),globalR(non))
hold on
plot(time(blocked),globalR(blocked))
xlim([0 240]); ylim([ymin ymax])
ax = gca;
ax.YAxis.Exponent = 1;
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')
%
[art.comb.LnonMin, art.comb.LnonMax] = meanAmp(globalL, non, 1.5);
[art.comb.LblockedMin, art.comb.LblockedMax] = meanAmp(globalL, blocked, 1.5);
[art.comb.RnonMin, art.comb.RnonMax] = meanAmp(globalR, non, 39);
[art.comb.RblockedMin, art.comb.RblockedMax] = meanAmp(globalR, blocked, 45);
[art.comb.LnonDiff, art.comb.LblockedDiff, art.comb.Lrel] = ...
    relDiff(art.comb.LnonMax, art.comb.LnonMin, ...
    art.comb.LblockedMax, art.comb.LblockedMin);
[art.comb.RnonDiff, art.comb.RblockedDiff, art.comb.Rrel] = ...
    relDiff(art.comb.RnonMax, art.comb.RnonMin, ...
    art.comb.RblockedMax, art.comb.RblockedMin);

% FILTER
figure
[globalL, globalR] = splitLung(imgFILT.car);
ymax = max(max([globalL globalR]));
ymin = min(min([globalL globalR]));
subplot(5,4,1:2)
plot(time(non),globalL(non))
hold on
plot(time(blocked),globalL(blocked))
xlim([0 240]); ylim([ymin ymax])
ax = gca;
ax.YAxis.Exponent = 2;
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')
subplot(5,4,3:4)
plot(time(non),globalR(non))
hold on
plot(time(blocked),globalR(blocked))
xlim([0 240]); ylim([ymin ymax])
ax = gca;
ax.YAxis.Exponent = 2;
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('Amplitude / AU')

[art.filt.LnonMin, art.filt.LnonMax] = meanAmp(globalL, non, 100);
[art.filt.LblockedMin, art.filt.LblockedMax] = meanAmp(globalL, blocked, 100);
[art.filt.RnonMin, art.filt.RnonMax] = meanAmp(globalR, non, 100);
[art.filt.RblockedMin, art.filt.RblockedMax] = meanAmp(globalR, blocked, 100);
[art.filt.LnonDiff, art.filt.LblockedDiff, art.filt.Lrel] = ...
    relDiff(art.filt.LnonMax, art.filt.LnonMin, ...
    art.filt.LblockedMax, art.filt.LblockedMin);
[art.filt.RnonDiff, art.filt.RblockedDiff, art.filt.Rrel] = ...
    relDiff(art.filt.RnonMax, art.filt.RnonMin, ...
    art.filt.RblockedMax, art.filt.RblockedMin);

disp('RAW:  left     right');disp([art.raw.Lrel, art.raw.Rrel])
disp('PCA:  left     right');disp([art.pca.Lrel, art.pca.Rrel])
disp('ICA:  left     right');disp([art.ica.Lrel, art.ica.Rrel])
disp('COMB: left     right');disp([art.comb.Lrel, art.comb.Rrel])
disp('FILT: left     right');disp([art.filt.Lrel, art.filt.Rrel])

% frames = find(time>=125);   % max
% frames = find(find(time<=160)>=frames(1));
% 
% imgGlobalSig(imgRAW(:,:,find(time<=100)), 'car', 'artery',imgRAW(:,:,blocked));
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/3]);
% imgGlobalSig(imgPCA.car(:,:,find(time<=100)), 'car', 'artery',imgPCA.car(:,:,blocked));
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/3]);
% imgGlobalSig(imgICA.car(:,:,find(time<=100)), 'car', 'artery',imgICA.car(:,:,blocked));
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/3]);
% imgGlobalSig(imgCOMB.car(:,:,find(time<=100)), 'car', 'artery',imgCOMB.car(:,:,blocked));
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/3]);
% imgGlobalSig(imgFILT.car(:,:,find(time<=100)), 'car', 'artery',imgFILT.car(:,:,blocked));
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365, 0.4862/3]);



%% Instabile Herzfrequenz [DATA 6]

figure
set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.294, 0.365, 0.5862]);
subplot(4,5,1:3)
plot(time,xRAW)
xlim([0 10]);
ylim([min(xRAW(find(time<=10))) max(xRAW(find(time<=10)))])
% hold on
% fill([time(frameVen) time(frameVen) ...
%     time(frameVen+4*frameRateVen) time(frameVen+4*frameRateVen)], ...
%     [min(xRAW(find(time<=10))) max(xRAW(find(time<=10)))...
%     max(xRAW(find(time<=10))) min(xRAW(find(time<=10)))], ...
%     'green', ...
%     'FaceColor', [0.5 0.5 1], ...
%     'FaceAlpha',0.2, ...
%     'LineStyle','none'); 
% fill([time(frameCar) time(frameCar) ...
%     time(frameCar+4*frameRateCar) time(frameCar+4*frameRateCar)], ...
%     [min(xRAW(find(time<=10))) max(xRAW(find(time<=10)))...
%     max(xRAW(find(time<=10))) min(xRAW(find(time<=10)))], ...
%     'green', ...
%     'FaceColor', [1 0.5 0.5], ...
%     'FaceAlpha',0.2, ...
%     'LineStyle','none'); 
set(gca, 'FontName', 'Times New Roman')
xlabel('Zeit / s'); ylabel('[AU]')
subplot(4,5,4:5)
plot(bpmAxis,XRAW(1:length(fAxis)))
xlim([0 150])%;ylim([0 ymax])
set(gca, 'FontName', 'Times New Roman')
xlabel('Frequenz / bpm'); % ylabel('[AU]')





function [diff1, diff2, rel] = relDiff(max1, min1, max2, min2)

diff1 = max1 - min1;
diff2 = max2 - min2;
rel = diff2/diff1;

end
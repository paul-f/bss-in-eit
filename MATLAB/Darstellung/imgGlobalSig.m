function imgGlobalSig(img, comp, mod, img2)

if exist('mod','var') ~= 1
    globalSig = squeeze(nansum(nansum(img,2),1));
    globalSig = globalSig - mean(globalSig);
    
    [~,maxpos] = max(globalSig);
    [~,minpos] = min(globalSig);
    
    cMin = min(img(:,:,maxpos)-(img(:,:,minpos)),[],'all');
    cMax = max(img(:,:,maxpos)-(img(:,:,minpos)),[],'all');
    
    figure
    showimg(img(:,:,maxpos)-(img(:,:,minpos)))
    set(gca, 'xtick',[], 'ytick',[])
    switch comp
        case 'ven'
            colormap(colormap(flipud(setcolormap('maxven'))))
            cbh=colorbar;
            t=get(cbh,'Limits');
            set(cbh,'Ticks',linspace(t(1),t(2),2))
            cbh.TickLabels = ['min';'max'];
            set(cbh, 'FontName', 'Times New Roman','FontSize', 24)
        case 'car'
            colormap(colormap(flipud(setcolormap('car1'))))
            cbh=colorbar;
            t=get(cbh,'Limits');
            set(cbh,'Ticks',linspace(t(1),t(2),2))
            cbh.TickLabels = ['min';'max'];
            set(cbh, 'FontName', 'Times New Roman','FontSize', 24)
        case 'apnoe'
            colormap(colormap(flipud(setcolormap('apnoe'))))
            cbh=colorbar;
            t=get(cbh,'Limits');
            set(cbh,'Ticks',linspace(t(1),t(2),2))
            cbh.TickLabels = ['min';'max'];
            set(cbh, 'FontName', 'Times New Roman','FontSize', 24)
    end
elseif exist('mod','var') == 1
    figure
    
    globalSig = squeeze(nansum(nansum(img,2),1));
    globalSig = globalSig - mean(globalSig);
    
    [~,maxpos] = max(globalSig);
    [~,minpos] = min(globalSig);
    
    cMin = min(img(:,:,maxpos)-(img(:,:,minpos)),[],'all');
    cMax = max(img(:,:,maxpos)-(img(:,:,minpos)),[],'all');
    
    subplot(1,2,1)
    showimg(img(:,:,maxpos)-(img(:,:,minpos)))
    set(gca, 'xtick',[], 'ytick',[])
    switch comp
        case 'ven'
            colormap(colormap(flipud(setcolormap('maxven'))))
            cbh=colorbar;
            t=get(cbh,'Limits');
            set(cbh,'Ticks',linspace(t(1),t(2),2))
            cbh.TickLabels = ['min';'max'];
            set(cbh, 'FontName', 'Times New Roman')
        case 'car'
            colormap(colormap(flipud(setcolormap('car1'))))
            cbh=colorbar;
            t=get(cbh,'Limits');
            set(cbh,'Ticks',linspace(t(1),t(2),2))
            cbh.TickLabels = ['min';'max'];
            set(cbh, 'FontName', 'Times New Roman')
    end
    
    globalSig = squeeze(nansum(nansum(img2,2),1));
    globalSig = globalSig - mean(globalSig);
    
    [~,maxpos] = max(globalSig);
    [~,minpos] = min(globalSig);
    
    cMin = min(img2(:,:,maxpos)-(img2(:,:,minpos)),[],'all');
    cMax = max(img2(:,:,maxpos)-(img2(:,:,minpos)),[],'all');
    
    subplot(1,2,2)
    showimg(img2(:,:,maxpos)-(img2(:,:,minpos)))
    set(gca, 'xtick',[], 'ytick',[])
    switch comp
        case 'ven'
            colormap(colormap(flipud(setcolormap('maxven'))))
            cbh=colorbar;
            t=get(cbh,'Limits');
            set(cbh,'Ticks',linspace(t(1),t(2),2))
            cbh.TickLabels = ['min';'max'];
            set(cbh, 'FontName', 'Times New Roman')
        case 'car'
            colormap(colormap(flipud(setcolormap('car1'))))
            cbh=colorbar;
            t=get(cbh,'Limits');
            set(cbh,'Ticks',linspace(t(1),t(2),2))
            cbh.TickLabels = ['min';'max'];
            set(cbh, 'FontName', 'Times New Roman')
    end
end

% figure
% subplot(1,3,1)
% showimg(img(:,:,maxpos))
% set(gca, 'xtick',[], 'ytick',[])
% title('max')
% caxis([cMin cMax])
% subplot(1,3,2)
% showimg(img(:,:,maxpos)-(img(:,:,minpos)))
% title('max - min')
% set(gca, 'xtick',[], 'ytick',[])
% subplot(1,3,3)
% showimg((img(:,:,minpos)))
% set(gca, 'xtick',[], 'ytick',[])
% title('min')
% switch comp
%     case 'ven'
%         colormap(colormap(flipud(setcolormap('car1'))))
%     case 'car'
%         colormap(colormap(flipud(setcolormap('car1'))))
% end
%%
%
% globalRaw = squeeze(nansum(nansum(imgRAW ,2),1)); globalRaw = globalRaw - mean(globalRaw);
% globalVen = squeeze(nansum(nansum(imgPCA.ven,2),1)); globalVen = globalVen - mean(globalVen);
% globalCar = squeeze(nansum(nansum(imgPCA.car,2),1)); globalCar = globalCar - mean(globalCar);
%
%
% %
% [~,maxposV] = max(globalVen); [~,minposV] = min(globalVen);
% figure
% subplot(1,3,1)
% showimg(imgPCA.ven(:,:,maxposV))
% % colorbar
% caxis([-6.5 4.5])
% set(gca, 'xtick',[], 'ytick',[])
% title('max ventilation')
% subplot(1,3,2)
% showimg(imgPCA.ven(:,:,maxposV)-(imgPCA.car(:,:,minposV)))
% caxis([-6.5 4.5])
% % colorbar
% % set(gca, 'xtick',[], 'ytick',[])
% title('max - min')
% subplot(1,3,3)
% showimg((imgPCA.ven(:,:,minposV)))
% caxis([-6.5 4.5])
% % colorbar
% colormap(colormap(flipud(setcolormap('car1'))))
% set(gca, 'xtick',[], 'ytick',[])
% title('min ventilation')
% 
% 
% [~,maxposC] = max(globalCar); [~,minposC] = min(globalCar);
% figure
% subplot(1,3,1)
% showimg(imgPCA.car(:,:,maxposC))
% % colorbar
% caxis([-3.5 2.5])
% % set(gca, 'xtick',[], 'ytick',[])
% title('max cardiac')
% subplot(1,3,2)
% showimg(imgPCA.car(:,:,maxposC)-(imgPCA.car(:,:,minposC)))
% caxis([-3.5 2.5])
% % colorbar
% set(gca, 'xtick',[], 'ytick',[])
% title('max - min')
% subplot(1,3,3)
% showimg((imgPCA.car(:,:,minposC)))
% caxis([-3.5 2.5])
% % colorbar
% colormap(colormap(flipud(setcolormap('car1'))))
% set(gca, 'xtick',[], 'ytick',[])
% title('min cardiac')
end
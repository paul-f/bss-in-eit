function [globalLeft, globalRight] = splitLung(imgs)

right= imgs; left= imgs;
left(:,(size(imgs,2)/2+1):size(imgs,2),:)=0;
right(:,1:size(imgs,2)/2,:)=0;


globalLeft=     squeeze(nansum(nansum(left,2),1));
globalRight=    squeeze(nansum(nansum(right,2),1));
end
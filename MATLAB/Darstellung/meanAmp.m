function [minAmp, maxAmp] = meanAmp(sig, frames, prom)

% max
xPeaksMax = islocalmax(sig(frames),'MinProminence',prom);%*max(globalL(non));
locsPeaksMax = frames(find(xPeaksMax==1));
maxAmp = mean(sig(locsPeaksMax));

% figure
% subplot(2,1,1)
% plot(sig(frames))
% hold on
% plot(xPeaksMax*max(sig))

% min
xPeaksMin = islocalmin(sig(frames),'MinProminence',prom);%*min(globalL(non));
locsPeaksMin = frames(find(xPeaksMin==1));
minAmp = mean(sig(locsPeaksMin));

% subplot(2,1,2)
% plot(sig(frames))
% hold on
% plot(xPeaksMin*min(sig))

end
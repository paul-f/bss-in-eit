function [cmap] = setcolormap(mod)

bit = 256;
colors = zeros(bit,3);

switch mod
    case 'ven'
        fac = 4;
        facWhite = 2;
        %% red
        % for white
        for k = 1:round(bit/(fac*facWhite))-1
            colors(k,1) = (1-(k-1)/round((bit/(fac*facWhite))))*0.9;
        end
        % for purple
        for k = round(bit/fac)+1:bit
            colors(k,1) = ((k-round(bit/fac))/(bit-round(bit/fac)))*0.5;
        end
        %% green
        % for white
        for k = 1:round(bit/(fac*facWhite))-1
            colors(k,2) = (1-(k-1)/round((bit/(fac*facWhite))))*0.9;
        end
        %% blue
        % for white
        for k = 1:round(bit/(fac*facWhite))-1
            colors(k,3) = 1;
        end
        colors(k,2) = (1-(k-1)/round((bit/(fac*facWhite))));
        % black to blue
        for k = round(bit/(fac*facWhite)):round(bit/fac)
            colors(k,3) = 1 - (k-round(bit/(fac*facWhite)))/(round(bit/(fac*facWhite)));
        end
        % for purple
        for k = round(bit/fac):bit
            colors(k,3) = ((k-round(bit/fac))/(bit-round(bit/fac)))*0.5;
        end
        debug = 1;
    case 'ven1'
        %% red
        %         for k = 1:bit/2
        %             colors(k,1) = 1 - (k-1)/(bit/2);
        %         end
        %
        %         %% green
        %         for k = 1:bit/2
        %             colors(k,2) = 1 - (k-1)/(bit/2);
        %         end
        %
        for k = bit/2+1:bit
            colors(k,1) = ((k-bit/2)/(bit/2))*0.5;
        end
        %% blue
        for k = 1:bit/2
            colors(k,3) = 1-(k-1)/(bit/2);
        end
        for k = bit/2+1:bit
            colors(k,3) = ((k-bit/2)/(bit/2))*0.5;
        end
        
  	case 'ven2'
        %% red
        %         for k = 1:bit/2
        %             colors(k,1) = 1 - (k-1)/(bit/2);
        %         end
        %
        %         %% green
        %         for k = 1:bit/2
        %             colors(k,2) = 1 - (k-1)/(bit/2);
        %         end
        %% blue
        for k = 1:bit/2
            colors(k,3) = 1-(k-1)/(bit/2);
        end
        
    case 'maxven'
        %% red
        for k = 1:bit/2
            colors(k,1) = 1 - (k-1)/(bit/2);
        end
        
        %% green
        for k = 1:bit/2
            colors(k,2) = 1 - (k-1)/(bit/2);
        end
        
        %% blue
        for k = 1:bit/2
            colors(k,3) = 1;
        end
        for k = bit/2+1:3*bit/4
            colors(k,3) = 1 - (k-bit/2)/(bit/4);
        end
     
    case 'apnoe'
        
        %% red
        colors = zeros(bit,3);
        for k = 1:bit/2
            colors(k,1) = 1;
        end
        
        for k = bit/2+1:3*bit/4
            colors(k,1) = 1 - (k-bit/2)/(length(bit/2+1:3*bit/4));
        end
        
        
        
        %% green
        for k = 1:bit/2
            colors(k,2) = 1 - (k-1)/(bit/2);
        end
        
        %% blue
        for k = 1:bit/2
            colors(k,3) = 1 - (k-1)/(bit/2);
        end
        
    case 'car'
        %% red
        for k = 1:50
            colors(k,1) = 1;
        end
        for k = 51:154
            colors(k,1) = 1-(k-51)/(154-51);
        end
        
        %% green
        for k = 1:51
            colors(k,2) = 1 - (k-1)/(50);
        end
        
        %% blue
        for k = 1:51
            colors(k,3) = 1 - (k-1)/(50);
        end
        for k = 154:bit
            colors(k,3) = (k-154)/(bit-154);
        end

   	case 'car1'
        %% red
        for k = 1:0.125*bit
            colors(k,1) = 1;
        end
        for k = 0.125*bit:0.375*bit
            colors(k,1) = 1-(k-0.125*bit)/(0.375*bit-0.125*bit);
        end
%         for k = 0.875*bit:bit
%             colors(k,1) = (k-0.875*bit)/(bit-0.875*bit);
%         end
        %% green
        for k = 1:0.125*bit
            colors(k,2) = 1 - (k-1)/(0.125*bit);
        end
        
        %% blue
        for k = 1:0.125*bit
            colors(k,3) = 1 - (k-1)/(0.125*bit);
        end
        for k = 0.5*bit:bit
            colors(k,3) = (k-0.5*bit)/(bit-0.5*bit);
        end
        
   	case 'maxcar'
        %% red
%         for k = 1:84
%             colors(k,1) = (k-1)/84;
%         end
%         for k = 84:170
%             colors(k,1) = 1 - (k-84)/86;
%         end
%         for k = 1:0.25*bit
%             colors(k,1) = (k-1)/(0.25*bit-1);
%         end 
%         for k = 0.25*bit:0.375*bit
%             colors(k,1) = (k-0.25*bit)/(0.375*bit-0.25*bit);
%         end
        for k = 0.25*bit:0.625*bit
            colors(k,1) = (k-0.25*bit)/(0.625*bit-0.25*bit);
        end
        for k = 0.625*bit:0.75*bit
            colors(k,1) = 1 - (k-0.625*bit)/(0.75*bit-0.625*bit);
        end
        %% green
%         for k = 0.5*bit:0.625*bit
%             colors(k,2) = (k-0.5*bit)/(0.625*bit-0.5*bit);
%         end
        for k = 0.625*bit:0.75*bit
            colors(k,2) = (k-0.625*bit)/(0.75*bit-0.625*bit);
        end
        for k = 0.75*bit:0.875*bit
            colors(k,2) = 1 - (k-0.75*bit)/(0.875*bit-0.75*bit);
        end
        %% blue
%         for k = 0.25*bit:0.5*bit
%             colors(k,3) = (k-0.25*bit)/(0.5*bit-0.25*bit);
%         end
        for k = 0.5*bit:0.75*bit
            colors(k,3) = (k-0.5*bit)/(0.75*bit-0.5*bit);
        end
        for k = 0.75*bit:0.875*bit
            colors(k,3) = 1;
        end
        for k = 0.875*bit:bit
            colors(k,3) = 1 - (k-0.875*bit)/(bit-0.875*bit);
        end
        
   	case 'maxcar0'
        %% red
%         for k = 1:84
%             colors(k,1) = (k-1)/84;
%         end
%         for k = 84:170
%             colors(k,1) = 1 - (k-84)/86;
%         end
%         for k = 1:0.25*bit
%             colors(k,1) = (k-1)/(0.25*bit-1);
%         end        
        for k = 0.25*bit:0.5*bit
            colors(k,1) = (k-0.25*bit)/(0.5*bit-0.25*bit);
        end
        for k = 0.5*bit:0.75*bit
            colors(k,1) = 1 - (k-0.5*bit)/(0.75*bit-0.5*bit);
        end
        %% green
        for k = 0.375*bit:0.5*bit
            colors(k,2) = (k-0.375*bit)/(0.5*bit-0.375*bit);
        end
        for k = 0.5*bit:0.9375*bit
            colors(k,2) = 1 - (k-0.5*bit)/(0.9375*bit-0.5*bit);
        end
        %% blue
%         for k = 0.25*bit:0.5*bit
%             colors(k,3) = (k-0.25*bit)/(0.5*bit-0.25*bit);
%         end
        for k = 0.5*bit:0.75*bit
            colors(k,3) = (k-0.5*bit)/(0.75*bit-0.5*bit);
        end
        for k = 0.75*bit:bit
            colors(k,3) = 1 - (k-0.75*bit)/(bit-0.75*bit);
        end
        
    case 'maxcar1'
        %% red
        for k = 1:84
            colors(k,1) = (k-1)/84;
        end
        for k = 84:170
            colors(k,1) = 1 - (k-84)/86;
        end
        %% green
        
        %% blue
        for k = 84:170
            colors(k,3) = (k-84)/86;
        end
        for k = 170:bit
            colors(k,3) = 1 - (k-170)/86;
        end
        
    case 'maxcar2'
        %% red
        for k = 1:bit/2
            colors(k,1) = 1 - (k-1)/(bit/2);
        end
        %% green
        
        %% blue
        for k = 84:170
            colors(k,3) = (k-84)/86;
        end
        for k = 170:bit
            colors(k,3) = 1 - (k-170)/86;
        end
    case 'maxcar3'
        %% red
        for k = 1:bit/2
            colors(k,1) = 1 - (k-1)/(bit/2);
        end
        %% green
        
        %% blue
        for k = 1:bit/2
            colors(k,3) = (k-1)/(bit/2);
        end
        for k = bit/2:bit
            colors(k,3) = 1 - (k-bit/2)/(bit/2);
        end
        
    case 'maxcar4'
        %% red
        for k = 1:84
            colors(k,1) = 1;
        end
        for k = 84:170
            colors(k,1) = 1 - (k-84)/86;
        end
        %% green
        for k = 1:83
            colors(k,2) = 1 - (k-1)/(84);
        end
        %% blue
        for k = 1:84
            colors(k,3) = 1 - (k-1)/(84);
        end
        for k = 84:170
            colors(k,3) = (k-84)/86;
        end
        for k = 170:bit
            colors(k,3) = 1 - (k-170)/86;
        end
end

cmap = colors;
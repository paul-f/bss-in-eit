function pcaInput(X,time)
%% test Inputsignale
% ffts berechnen
% signale mit hoher energie auswählen
% signale plotten 
% signale auswählen, die nur gering phasenverschoben sind
% ausgewählten signale in pca stecken
X=       data.voltages.raw;
X =         X-mean(X,2);                % Mittelwertfrei: Wikipedia: Spalten müssen mittelwertfrei sein? (s. Details, Abs. 2) - https://en.wikipedia.org/wiki/Principal_component_analysis

%% FFT: Calc
Fs = 47.68;
w = window(@blackman,size(X,2));
for k = 1:size(X,1)
    [Xfft(k,:), fAxis]   = ffts(X(k,:).*w',size(X,2)*10,Fs);
end

%% FFT: Plots
for j = [0 0.5 0.9]
clear Xinp Xfft_
Xfftmax=    max(Xfft,[],'all');
index=      1;
for k = 1:size(X,1)
    if max(Xfft(k,:),[],'all') >= Xfftmax*j           % 10% von maxFFT
        Xfft_(index,:) = Xfft(k,:);
        Xinp(index,:) = X(k,:);
        ind(index) = k;
        index = index + 1;
    end
end

% figure
% subplot(2,1,1)
% plot(fAxis*60,Xfft_)
% xlim([0 100])
% 
% % PHASE ANSCHAUEN
% subplot(2,1,2)
% hold on
% xlim([0 483])
% for k = 1:size(Xinp,1)
%     plot(Xinp(k,:))
% end
% PCA
Sv = Xinp'*pca(Xinp','NumComponents',4); Sv = Sv';

if j == 0
    Smax = max(Sv(1,:));
end
%
figure(1)
% set(gcf, 'Units', 'normalized', 'Position', [0.317, 0.394, 0.365*0.95, 0.4862]);
for l = 1:size(Sv,1)
    subplot(4,2,l*2)
    hold on; plot(time,Sv(l,:))
    xlim([0 15]) %%ylim([-Smax Smax])
    ax = gca;
    ax.YAxis.Exponent = -4;
    set(gca, 'FontName', 'Times New Roman')
    xlabel('Zeit/ s'); ylabel('[AU]')
    pc = sprintf('%i. Hauptkomponente', l);
    title(pc,'Position',[-12.5 0 ],'HorizontalAlignmen','left')
    box on
end 

end
function [Xv, Xc] = bss_ica_of_pcs(X)
%%  BLIND SOURCE SEPARATION IN EIT:   --- ICA of PCs ---
%%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%%% step 1 - calculate template function of ventilation signal
%%% step 2 - calculate template function of cardiac signal
%%% step 3 - calculate ventilation and cadiac signal


%% step 1 - calculate template function of ventilation signal

X = X-mean(X,2);                                   	% calculate mean free data

S = X'*pca(X');                                     % template function matrix (principal component scores of X)
Sv = S(:,1)';                                       % ventilation template function (1st principal component score of X)

% Xv = X*pinv(Sv)*Sv;

%% step 2 - calculate template function of cardiac signal
a = svd(X);                                         % compute singular values
aNorm = a/a(1);                                     % normalize singular values - plot: figure; bar(aNorm); xlim([0 30])
numPC = 0;
while aNorm(numPC+1) > 0.075
    numPC = numPC+1;                                % set number of principal components
end
% figure
% bar(aNorm)
% xlim([0 30])

S_1 = S(:,2:numPC)';

fs = 47.68;                                         % sampling rate
fl = 0.5;
fh = 5;                                           % low cutoff frequency

% bpFilt = designfilt('bandpassfir', ...
%             'FilterOrder', 50, ...
%             'CutoffFrequency1', fl, ...
%             'CutoffFrequency2', fh, ...
%             'SampleRate', fs);
%         % fvtool(bpFilt)
%         S_1 = filtfilt(bpFilt, S_1');

hpFilt = designfilt('highpassfir', ...
    'FilterOrder', 50, ...
    'StopbandFrequency', 0.35, ...
    'PassbandFrequency', fl, ...
    'SampleRate', fs, ...
    'DesignMethod', 'equiripple');
lpFilt = designfilt('lowpassfir', ...
    'FilterOrder', 50, ...
    'PassbandFrequency', fh, ...
    'StopbandFrequency', 7.5, ...
    'SampleRate', fs, ...
    'DesignMethod', 'equiripple');
%         fvtool(hpFilt)
%         fvtool(lpFilt)

S_1 = filtfilt(lpFilt, ...           % zero-phase filtering
    filtfilt(hpFilt,S_1'));
S_1 = S_1';

[~,Sc_2] = jade(S_1);  
% numIC = numPC-1;
% rng default
% Sc_2 = fastica(S_'/(max(max(S_))), 'numOfIC', numIC);


% choose template function with the largest energy
Fs = 47.68;
w = window(@blackman,size(Sc_2,2));
% figure
for k = 1:size(Sc_2,1)
%     subplot(size(Sc_2,1),2,2*k-1); plot(Sc_2(k,:)); xlim([0 483*2])
    [Scfft(k,:), fAxis]   = ffts(Sc_2(k,:).*w',size(Sc_2,2)*10,Fs);
%     subplot(size(Sc_2,1),2,2*k); plot(fAxis*60,Scfft(k,:)); xlim([0 100]);% ylim([0 0.025])
end
[argvalue, ~] = max(Scfft(:,find(fAxis>40/60))');   % for fh_min = 40 bpm
[~, argmax] = max(argvalue);

Sc = Sc_2(argmax,:);                               % cardiac template function


%% step 3 - calculate ventilation and cadiac signal
% B = [Sv; Sc; ones(1,length(S))];                    % ?
% C = X*pinv(B);                                      % matrix of scale factors and offsets C = inv(B'*B)*B'*X = [ai; bi; ci];
% 
% Xv = C(:,1).*Sv + 0.5*C(:,3);                       % ventilation signal Xv
% Xc = C(:,2).*Sc + 0.5*C(:,3);                       % cardiac signal Xv

Xv = X*pinv(Sv)*Sv;
Xc = X*pinv(Sc)*Sc;
end
function [Xv, Xc] = v2008_bss_pca(X)

%%  BLIND SOURCE SEPARATION IN EIT:   --- PCA ---
%%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%%% step 1    - 1st approximation of ventilation signal
%%% step 2    - approximation of cardiac signal components
%%%      2.1  - 1st approximation of cardiac signal
%%%      2.2  - filtering 1st approximation of cardiac signal
%%%      2.3  - 2nd approximation of cardiac signal and approximation of the small remaining cardiac component in the ventilation signal
%%% step 3    - calculate final approximations of ventilation and cardiac signals


%% step 1 - 1st approximation of ventilation signal Xv_1
X = X';
X = X-mean(X);                                  % calculate mean free data
Tv = X*pca(X,'NumComponents',1);                % ventilation template function (1st principal component score of X)

Xv_1 = Tv*pinv(Tv)*X;                           % 1st approximation of the ventilation signal Xv_1


%% step 2 - 1st and 2nd approximation of cardiac signal

Xc_1 = X - Xv_1;                                % 1st approximation of the cardiac signal Xc_1

% filtering
Fs = 47.68;                                     % sampling rate
Fl = 0.92;                                      % low cutoff frequency
Fh = 4.6;                                       % high cutoff frequency
bpFilt = designfilt('bandpassfir', ...
    'FilterOrder', 30, ...
    'CutoffFrequency1', Fl, ...
    'CutoffFrequency2', Fh, ...
    'SampleRate', Fs);
% fvtool(bpFilt)
Xcbp_1 = filtfilt(bpFilt, Xc_1);                % zero-phase filtering

Tc_ = Xcbp_1*pca(Xcbp_1,'NumComponents',2);     % cardiac template functions (1st and 2nd principal component score of Xcbp_1)
N = size(Tc_,1);
w = window(@hamming,N);
for k = 1:size(Tc_,2)
    [Tcfft(:,k), Faxis] = ffts(Tc_(:,k).*w, N*10, Fs);
%     figure
%     plot(Faxis*60, Tcfft(:,k)); xlim([0 280])
end

% [argvalue, ~] = max(Scfft(:,find(fAxis>40/60))');   % for fh_min = 40 bpm

[argvaluec, M] = max(Tcfft);                    % find the heart frequency
[~, argmaxc] = max(argvaluec);
Fheart = Faxis(M(argmaxc));
heartperiod = Fs/Fheart;
ps = round(heartperiod/3);                      % phase-shift: 1/3 heart period
Tc = [Tc_(:,1) wshift('1D',Tc_(:,1),ps) wshift('1D',Tc_(:,1),-ps) ...
    Tc_(:,2) wshift('1D',Tc_(:,2),ps) wshift('1D',Tc_(:,2),-ps)];       % final Tc = [ s1 s1(+1/3) s1(-1/3) s2 s2(+1/3) s2(-1/3) ]

% Tc = Tc_;
Xc1_2 = Tc*pinv(Tc)*Xc_1;                       % 2nd approximation of the cardiac signal Xc1_2
Xc2_2 = Tc*pinv(Tc)*Xv_1;                       % approximation of the small remaining cardiac component Xc2_2 in the ventilation signal Xv_1


%% step 3 - calculate final approximations of ventilation and cardiac signals Xv and Xc

Xc = Xc1_2 + Xc2_2;                             % cardiac signal Xc
Xv = Xv_1 - Xc2_2;                              % ventilation signal Xv


end
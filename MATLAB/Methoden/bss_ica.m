function [Xv, Xc] = bss_ica(X, imdl)
%%  BLIND SOURCE SEPARATION IN EIT:   --- ICA ---
%%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%%% step 1 - calculate the ventilation signal Xv
%%% step 2 - step 2 - calculate the cardiac signal Xc (and noise signal Xn)


%% step 1 - calculate the ventilation signal Xv

X = X-mean(X,2);                                      % calculate mean free data

% XRAW = X;
% X = XRAW;
%%
% fs = 47.68;                                         % sampling rate
% fc = 5;                                           % low cutoff frequency
% lpFilt = designfilt('lowpassfir', ...
%     'FilterOrder', 50, ...
%     'PassbandFrequency', fc, ...
%     'StopbandFrequency', 8, ...
%     'SampleRate', fs, ...
%     'DesignMethod', 'equiripple');
% fvtool(lpFilt)
% X = filtfilt(lpFilt, X');
% X = X';
%%
% X = XRAW;
a = svd(X);                                         % compute singular values
aNorm = a/a(1);                                     % normalize singular values - plot: figure; bar(aNorm); xlim([0 30])
% figure
% bar(aNorm(1:50))
numPCv = 0;
while aNorm(numPCv+1) > 0.1                        % 0.01, wenn bei 1 Hz gefiltert wurde, 0.1 bei ungefiltert
    numPCv = numPCv+1;                              % set number of principal components
end
%%
Xwhv = X'*pca(X','NumComponents',numPCv);             % whitening and reduce the dimensionality (numPC) of the data

[~,Sv_1] = jade(Xwhv');  
% numICv = numPCv;                                    % number of independent components to be calculated
% rng default                                 % set the random number generator to the default seed (0) -> fastica() returns always the same results
% Sv_ = jade(Xwhv'/(max(max(Xwhv))), ...    % source signal matrix (template funcions (numIC))
%     'numOfIC', numICv, 'g', 'skew');

% choose the ventilation template function
Fs = 47.68;
w = window(@blackman,size(Sv_1,2));
figure
for k = 1:size(Sv_1,1)
    subplot(size(Sv_1,1),3,3*k-2); plot(Sv_1(k,:)); xlim([0 483])
    [Svfft(k,:), fAxis]   = ffts((Sv_1(k,:)).*w',size(Sv_1,2)*10,Fs);
    subplot(size(Sv_1,1),3,3*k-1); plot(fAxis*60,Svfft(k,:)); xlim([0 100]);
    Xv_v    = X*pinv(Sv_1(k,:))*Sv_1(k,:);
    Xv_h    = mean(Xv_v,2);    % Referenz
    dZ      = inv_solve(imdl, Xv_h,Xv_v);
    imgs	= calc_slices(dZ);
    subplot(size(Sv_1,1),3,3*k); imagesc(imgs(:,:,162)); axis square
end
%%
% [argvaluev, ~] = max(Svfft');                        % find the template function with the largest energy
% [~, argmaxv] = max(argvaluev);
argmaxv = input('Wähle die gewünschte Template Funktion der Atmung: ');
close

Sv = Sv_1(argmaxv,:);                                % ventilation template function

Xv = X*pinv(Sv)*Sv;                                 % ventilation signal Xv


%% step 2 - calculate the cardiac signal Xc (and noise signal Xn)

Xc_1 = X - Xv;                                      % 1st approximation of the cardiac signal Xc_1

% fs = 47.68;                                         % sampling rate
% fl = 0.5;
% fh = 8;                                           % low cutoff frequency
% 
% % lpFilt = designfilt('lowpassfir', ...
% %     'FilterOrder', 50, ...
% %     'PassbandFrequency', fh, ...
% %     'StopbandFrequency', 10, ...
% %     'SampleRate', fs, ...
% %     'DesignMethod', 'equiripple');
% % fvtool(lpFilt)
% % Xc_1 = filtfilt(lpFilt, Xc_1');
% % Xc_1 = Xc_1';
% 
% hpFilt = designfilt('highpassfir', ...
%     'FilterOrder', 50, ...
%     'StopbandFrequency', 0.35, ...
%     'PassbandFrequency', fl, ...
%     'SampleRate', fs, ...
%     'DesignMethod', 'equiripple');
% lpFilt = designfilt('lowpassfir', ...
%     'FilterOrder', 50, ...
%     'PassbandFrequency', fh, ...
%     'StopbandFrequency', 15, ...
%     'SampleRate', fs, ...
%     'DesignMethod', 'equiripple');
% %         fvtool(hpFilt)
% %         fvtool(lpFilt)
% Xc_1 = filtfilt(lpFilt, Xc_1');
% % Xc_1 = filtfilt(lpFilt, ...           % zero-phase filtering
% %     filtfilt(hpFilt,Xc_1'));
% Xc_1 = Xc_1';
%%
a = svd(Xc_1);                                      % compute singular values
aNorm = a/a(1);                                     % normalize singular values - plot: figure; bar(aNorm); xlim([0 30])
% figure
% bar(aNorm(1:50))
numPCc = 0;
while aNorm(numPCc+1) > 0.15
    numPCc = numPCc+1;                              % set number of principal components
end

%%
% numPCc = 2;
Xwhc = Xc_1'*pca(Xc_1','NumComponents',numPCc);      	% [~,score] = pca(X) -> Datei wird automatisch mittelwertfrei gemacht

[~,Sc_1] = jade(Xwhc');  
% numICc = numPCc;                                    % number of independent components to be calculated
% rng default                                 % set the random number generator to the default seed (0) -> fastica() returns always the same results
% Sc_ = fastica(Xwhc'/(max(max(Xwhc))), ...    % source signal matrix (template funcions (numIC))
%     'numOfIC', numICc);

% choose the cardiac template function
w = window(@blackman,size(Sc_1,2));
figure
for k = 1:size(Sc_1,1)
    subplot(size(Sc_1,1),3,3*k-2); plot(Sc_1(k,:)); xlim([0 483])
    [Scfft(k,:), fAxis]   = ffts(Sc_1(k,:).*w',size(Sc_1,2)*10,Fs);
    subplot(size(Sc_1,1),3,3*k-1); plot(fAxis*60,Scfft(k,:)); xlim([0 100]);
    Xc_v    = X*pinv(Sc_1(k,:))*Sc_1(k,:);
    Xc_h    = mean(Xc_v,2);    % Referenz
    dZ      = inv_solve(imdl, Xc_h,Xc_v);
    imgs	= calc_slices(dZ);
    subplot(size(Sc_1,1),3,3*k); imagesc(imgs(:,:,180)); axis square
    clear Xv_v Xv_h dZ imgs
end
%%
% [argvaluec, ~] = max(Scfft(:,1:2500)');
% [~, argmaxc] = max(argvaluec);
argmaxc = input('Wähle die gewünschte Template Funktion der Durchblutung: ');
close

Sc = Sc_1(argmaxc,:);                               	% cardiac template function

Xc = Xc_1*pinv(Sc)*Sc;                                 % cardiac signal Xc

Xn = Xc_1 - Xc;                                     % noise signal Xn


end
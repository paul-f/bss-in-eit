% Quelle: http://arnauddelorme.com/ica_for_dummies/

%%Time specifications:
Fs = 1000;                          % samples per second
dt = 1/Fs;                          % seconds per sample
StopTime = 1;                       % seconds
t = (0:dt:StopTime-dt)';            % seconds

Fa = 2.5;
Fb = 4;
Fc = 7;
Fd = 30;
A = sin(2*pi*Fa*t);                 % A
B = sin(2*pi*Fb*t);                 % B
C = sin(2*pi*Fc*t);                 % C
D = sin(2*pi*Fd*t);                 % D

figure; 
subplot(4,1,1); plot(A);            % plot A
subplot(4,1,2); plot(B, 'r');       % plot B
subplot(4,1,3); plot(C, 'g');       % plot C
subplot(4,1,4); plot(D, 'black');   % plot C

%
x(1,:) =      A -    2*B +      C + 0.12*D; % mixing 1
x(2,:) = 1.73*A + 3.41*B - 1.21*C + 0.43*D; % mixing 2
x(3,:) = 2.33*A - 1.41*B + 2.65*C - 0.23*D; % mixing 3
x(4,:) = 0.54*A - 1.12*B + 8.65*C + 0.75*D; % mixing 4
x(5,:) = 0.33*A - 2.01*B + 4.65*C + 0.33*D; % mixing 5
x(6,:) = 6.63*A - 2.41*B + 1.65*C + 0.95*D; % mixing 6
figure;
subplot(3,2,1); plot(x(1,:));               % plot mixing 1
subplot(3,2,2); plot(x(2,:), 'r');          % plot mixing 2
subplot(3,2,3); plot(x(3,:), 'g');          % plot mixing 3
subplot(3,2,4); plot(x(4,:));               % plot mixing 4
subplot(3,2,5); plot(x(5,:), 'r');          % plot mixing 5
subplot(3,2,6); plot(x(6,:), 'g');          % plot mixing 6

%%
figure;
[y] = fastica(x);%,'lastEig', 6, 'numOfIC', 10);              % compute and plot unminxing using fastICA	
subplot(4,1,1); plot(y(1,:)/max(y(1,:)));
subplot(4,1,2); plot(y(4,:)/max(y(4,:)), 'r');
subplot(4,1,3); plot(y(3,:)/max(y(3,:)), 'g');
subplot(4,1,4); plot(y(2,:)/max(y(2,:)), 'black');

%% fft 

N = size(y,2);
M = size(y,1);
w = window(@blackman,N);
for k = 1:M
    [Y(k,:), fAxis]   = ffts(y(k,:).*w',N*10,Fs);
end
clear M N k

figure
subplot(4,1,1); plot(fAxis,Y(1,:)); xlim([0 50]);
subplot(4,1,2); plot(fAxis,Y(2,:)); xlim([0 50]);
subplot(4,1,3); plot(fAxis,Y(3,:)); xlim([0 50]);
subplot(4,1,4); plot(fAxis,Y(4,:)); xlim([0 50]);

[~,F1] = max(Y(1,:));
[~,F2] = max(Y(2,:));
[~,F3] = max(Y(3,:));
[~,F4] = max(Y(4,:));
F = [fAxis(F1) fAxis(F2) fAxis(F3) fAxis(F4)]

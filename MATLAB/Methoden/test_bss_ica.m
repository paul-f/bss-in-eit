%% - BLIND SOURCE SEPARATION IN EIT: ICA - %%
%% init
clearvars -except data imdl

addpath(genpath('C:\Users\Universität\Documents\Masterarbeit\MATLAB\'));
load('20190925_Interface_75HiHR_12HiBR_reconstructed');
load('ImdlHuman32by32')

load('PulHyp_20191030_PIG06__01-1__2D-EIT_reconstructed');
load('ARDSrampe_20210303_PIG14_healthy_P14_SO-2-re_reconstructed');
load('Pig_Model')

X = data.voltages.raw;                   % Signal muss NxM-Matrix sein! -> Transponieren
X = X-mean(X,2);                                      % Mittelwertfrei: Wikipedia: Spalten müssen mittelwertfrei sein? (s. Details, Abs. 2) - https://en.wikipedia.org/wiki/Principal_component_analysis
frame = 928/2;
Xraw = X;

%% ica algorithmen
numPCv = numPCv + 3;
Xwhv = X'*pca(X','NumComponents',numPCv);             % whitening and reduce the dimensionality (numPC) of the data

rng 'default'
Sfast       =fastica(Xwhv'/(max(max(Xwhv))));
% SnatGrad    =naturalGradIca(Xwhv', 0.01,1000,'sech');
[A,Sjade]   =jade(Xwhv');                             % Quelle: http://www2.iap.fr/users/cardoso/compsep_classic.html
SjadeR      =jadeR(Xwhv')*Xwhv';

figure
for k = 1:numPCv
    subplot(numPCv,1,k); plot(Sfast(k,:)); xlim([0 483])
end
% figure
% for k = 1:numPCv
%     subplot(numPCv,1,k); plot(SnatGrad(k,:)); xlim([0 483])
% end
figure
for k = 1:numPCv
    subplot(numPCv,1,k); plot(Sjade(k,:)); xlim([0 483])
end
figure
for k = 1:numPCv
    subplot(numPCv,1,k); plot(SjadeR(k,:)); xlim([0 483])
end


%% denoising
% filtering
Fs = 47.68;
Fl = 0.92;          % low cutoff frequency
Fh = 3;           % high cutoff frequency
% bpFilt = designfilt('bandpassfir', ...
%     'FilterOrder', 30, ...
%     'CutoffFrequency1', Fl, ...
%     'CutoffFrequency2', Fh, ...
%     'SampleRate', Fs);
% hpFilt = designfilt('highpassfir', ...
%     'FilterOrder', 30, ...
%     'StopbandFrequency', 0.025, ...
%     'PassbandFrequency', Fl/(Fs/2), ...
%     'DesignMethod', 'equiripple');
lpFilt = designfilt('lowpassfir', ...
    'FilterOrder', 30, ...
    'PassbandFrequency', Fh/(Fs/2), ...
    'StopbandFrequency', 0.3, ...
    'DesignMethod', 'equiripple');
% fvtool(bpFilt)
% fvtool(hpFilt)
% fvtool(lpFilt)
if exist('bpFilt','var') == 1                   % Zero-phase Filtering -> filtfilt()
    X = filtfilt(bpFilt, Xraw);
elseif exist('hpFilt','var') == 1
    X = filtfilt(hpFilt, Xraw);
elseif exist('lpFilt','var') == 1
    X = filtfilt(lpFilt, Xraw);
end

%% choose voltages
Fs = 47.68;
w = window(@blackman,size(Xraw,2));
for k = 1:size(Xraw,1)
    [Xfft(k,:), fAxis]   = ffts(Xraw(k,:).*w',size(Xraw,2)*10,Fs);
end

%
clear X Xfft_
Xfftmax = max(max(Xfft));
index = 1;
for k = 1:1%size(Xraw,2)
%     max(Xfft(:,k)) <= Xfftmax*0.3
    if max(Xfft(k,:)) >= Xfftmax*0.9
        Xfft_(index,:) = Xfft(k,:);
        X(index,:) = Xraw(k,:);
        index = index + 1;
    end
end

% PHASE ANSCHAUEN
%
% figure
% plot(fAxis*60,Xfft_)
% xlim([0 100])

%% ventilation signal
% [a_, scores, v] = pca(X);

a = svd(X);
% a = a.*a;
aNorm = a/a(1);
figure
bar(aNorm)
xlim([0 50])
numPCv = 0;
while aNorm(numPCv+1) > 0.1
    numPCv = numPCv+1;
end
%%
% numPCv = numPCv + 3;
%X = Xraw;
Xwhv = X'*pca(X','NumComponents',numPCv);             % whitening and reduce the dimensionality (numPC) of the data

icaMeth = 'old';                                    % 'old': fastica() - 'new': runica()
numICv = numPCv;                                    % number of independent components to be calculated
switch icaMeth
    case 'old'
        rng default                                 % set the random number generator to the default seed (0) -> fastica() returns always the same results
%         Sv = fastica(Xwhv'/(max(max(Xwhv))), ...    % source signal matrix (template funcions (numIC))
%            'numOfIC', numICv);%, 'g', 'skew');%, 'initGuess', randn(size(Xwhv,2),size(Xwhv,1))/10);
%         Sv    =naturalGradIca(Xwhv', 0.01,1000,'sech');
        [A,Sv_]   =jade(Xwhv');                             % Quelle: http://www2.iap.fr/users/cardoso/compsep_classic.html
%         Sv      =jadeR(Xwhv')*Xwhv';
    case 'new'
        [Wv,~] = runica(Xwhv','verbose','off');     % unmixing matrix
        Sv_ = Wv*Xwhv';                              % source signal matrix (template funcions (numIC))
end
%%
% Vielleicht mit Option zur Auswahl der richtigen Template-Fkt.?
Fs = 47.68;
w = window(@blackman,size(Sv_,2));
figure
for k = 1:size(Sv_,1)
    subplot(size(Sv_,1),3,3*k-2); plot(Sv_(k,:)); xlim([0 483*2])
    [Svfft(k,:), fAxis]   = ffts(Sv_(k,:).*w',size(Sv_,2)*10,Fs);
    subplot(size(Sv_,1),3,3*k-1); plot(fAxis*60,Svfft(k,:)); xlim([0 120]);
    Xv_v    = Xraw*pinv(Sv_(k,:))*Sv_(k,:);
    Xv_h    = mean(Xv_v,2);    % Referenz
    dZ      = inv_solve(imdl, Xv_h, Xv_v);
    imgs	= calc_slices(dZ);
    subplot(size(Sv_,1),3,3*k); imagesc(imgs(:,:,180+90)); axis square
    clear Xv_v Xv_h dZ imgs
end
%%
% [argvaluev, ~] = max(Svfft');
% [~, argmaxv] = max(argvaluev);

% Auswahl der richtigen Template Funktion
argmaxv = input('Wähle die gewünschte Template Funktion der Atmung: ');
Sv = Sv_(argmaxv,:);
Xv = Xraw*pinv(Sv)*Sv;
Xc_1 = Xraw - Xv;

figure
plot(Xraw(frame,:))
hold on
plot(Xv(frame,:))
plot(Xc_1(frame,:))
xlim([0 483])


%% cardic signal
% muss hier eigentlich auch nochmal Xc_1 Mittelwertfrei gemacht werden?
a = svd(Xc_1);
aNorm = a/a(1);
figure
bar(aNorm)
xlim([0 50])
numPCc = 0;
while aNorm(numPCc+1) > 0.1
    numPCc = numPCc+1;
end
%%
Xwhc = Xc_1'*pca(Xc_1','NumComponents',numPCc);      	% [~,score] = pca(X) -> Datei wird automatisch mittelwertfrei gemacht

icaMeth = 'old';                                    % 'old': fastica() - 'new': runica()
numICc = numPCc;                                    % number of independent components to be calculated
switch icaMeth
    case 'old'
        rng default                                 % set the random number generator to the default seed (0) -> fastica() returns always the same results
%         Sc = fastica(Xwhc'/(max(max(Xwhc))), ...    % source signal matrix (template funcions (numIC))
%            'numOfIC', numICc);
        [~,Sc_1]   =jade(Xwhc');                             % Quelle: http://www2.iap.fr/users/cardoso/compsep_classic.html
    case 'new'
        [Wc,~] = runica(Xwhc','verbose','off');     % unmixing matrix
        Sc_1 = Wc*Xwhc';                              % source signal matrix (template funcions (numIC))
end
%
w = window(@blackman,size(Sc_1,2));
figure
for k = 1:size(Sc_1,1)
    subplot(size(Sc_1,1),3,3*k-2); plot(Sc_1(k,:)); xlim([0 483])
    [Scfft(k,:), fAxis]   = ffts(Sc_1(k,:).*w',size(Sc_1,2)*10,Fs);
    subplot(size(Sc_1,1),3,3*k-1); plot(fAxis*60,Scfft(k,:)); xlim([0 100]);
    Xc_v    = X*pinv(Sc_1(k,:))*Sc_1(k,:);
    Xc_h    = mean(Xc_v,2);    % Referenz
    dZ      = inv_solve(imdl, Xc_h,Xc_v);
    imgs	= calc_slices(dZ);
    subplot(size(Sc_1,1),3,3*k); imagesc(imgs(:,:,180)); axis square
    clear Xv_v Xv_h dZ imgs
end
%
% [argvaluec, ~] = max(Scfft');
% [~, argmaxc] = max(argvaluec);

argmaxc = input('Wähle die gewünschte Template Funktion der Durchblutung: ');
Sc = Sc_1(argmaxc,:);
Xc = X*pinv(Sc)*Sc;
Xn = Xc_1 - Xc;

%%
figure
subplot(2,1,1)
plot(Xraw(frame+60,:))
xlim([0 483])
subplot(2,1,2)
plot(Xv(frame+60,:))
hold on
% plot(Xc_1(:,frame))
plot(Xc(frame+60,:))
% plot(Xn(:,frame))
xlim([0 483])



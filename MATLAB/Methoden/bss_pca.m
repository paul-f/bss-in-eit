function [Xv, Xc, Xv_1, Xcbp_1, Xc1_2, Xc2_2] = bss_pca(X)
%%  BLIND SOURCE SEPARATION IN EIT:   --- PCA ---
%%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%%% step 1 - 1st approximation of ventilation signal
%%% step 2 - 1st and 2nd approximation of cardiac signal
%%% step 3 - calculate final approximations of ventilation and cardiac signals

%% step 1 - 1st approximation of ventilation signal Xv_1

X = X-mean(X,2);                                  % calculate mean free data

Sv = X'*pca(X','NumComponents',1);                % ventilation template function (1st principal component score of X)

Xv_1 = X*pinv(Sv')*Sv';                           % 1st approximation of the ventilation signal Xv_1


%% step 2 - 1st and 2nd approximation of cardiac signal

Xc_1 = X - Xv_1;                                % 1st approximation of the cardiac signal Xc_1

% filtering
fs = 47.68;                                     % sampling rate
fl = 0.5;                                      % low cutoff frequency
fh = 5;                                       % high cutoff frequency
filtMethod = 'hplp';                            % filter method
switch filtMethod
    case 'bp'
        bpFilt = designfilt('bandpassfir', ...
            'FilterOrder', 30, ...
            'CutoffFrequency1', fl, ...
            'CutoffFrequency2', fh, ...
            'SampleRate', fs);
        % fvtool(bpFilt)
        Xcbp_1 = filtfilt(bpFilt, Xc_1');        % zero-phase filtering
    case 'hplp'
        hpFilt = designfilt('highpassfir', ...
            'FilterOrder', 50, ...
            'StopbandFrequency', 0.35, ...
            'PassbandFrequency', fl, ...
            'SampleRate', fs, ...
            'DesignMethod', 'equiripple');
        lpFilt = designfilt('lowpassfir', ...
            'FilterOrder', 50, ...
            'PassbandFrequency', fh, ...
            'StopbandFrequency', 7.5, ...
            'SampleRate', fs, ...
            'DesignMethod', 'equiripple');
%         fvtool(hpFilt)
%         fvtool(lpFilt)
         Xcbp_1 = filtfilt(lpFilt, ...           % zero-phase filtering
            filtfilt(hpFilt,Xc_1'));
    case 'without'
        Xcbp_1 = Xc_1';                          % without filtering
end
% load('hpFilt.mat')
% load('lpFilt.mat')

Xcbp_1 = Xcbp_1';
Sc = Xcbp_1'*pca(Xcbp_1','NumComponents',2);      % cardiac template functions (1st and 2nd principal component score of Xcbp_1)

% Fs = 47.68;
% w = window(@blackman,size(Sc,1));
% Sc = Sc-mean(Sc,1);
% [Scfft, fAxis]   = ffts(Sc(:,1).*w,size(Sc,1)*10,Fs);
% figure; plot(60*fAxis,Scfft); xlim([0 60*3.5])

% [argvaluec, M] = max(Scfft);                    % find the heart frequency
% [~, argmaxc] = max(argvaluec);
% fheart = Faxis(M(argmaxc));
% heartperiod = fs/fheart;
% ps = round(heartperiod/3);                                        % phase-shift: 1/3 heart period
% Sc = [Sc(:,1) wshift('1D',Sc(:,1),ps) wshift('1D',Sc(:,1),-ps) ...
%     Sc(:,2) wshift('1D',Sc(:,2),ps) wshift('1D',Sc(:,2),-ps)];       % final Tc = [ s1 s1(+1/3) s1(-1/3) s2 s2(+1/3) s2(-1/3) ]

Xc1_2 = Xc_1*pinv(Sc')*Sc';                       % 2nd approximation of the cardiac signal Xc1_2
Xc2_2 = Xv_1*pinv(Sc')*Sc';                       % approximation of the small remaining cardiac component Xc2_2 in the ventilation signal Xv_1

% step 3 - calculate final approximations of ventilation and cardiac signals Xv and Xc

Xc = Xc1_2 + Xc2_2;                             % cardiac signal Xc
Xv = Xv_1 - Xc2_2;                              % ventilation signal Xv
Xn = X - Xv - Xc;

end
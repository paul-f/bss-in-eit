function [Xv, Xc] = v2013_bss_ica(X, imdl)
%%  BLIND SOURCE SEPARATION IN EIT:   --- ICA ---
%%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%%% step 1 - calculate the ventilation signal Xv
%%% step 2 - step 2 - calculate the cardiac signal Xc (and noise signal Xn)


%% step 1 - calculate the ventilation signal Xv
X = X';
X = X-mean(X);                                      % calculate mean free data

numPCv = 8;                                        % number of principal components to be calculated
Xwhv = X*pca(X,'NumComponents',numPCv);             % whitening and reduce the dimensionality (numPC) of the data
[~,Sv] = jade(Xwhv');

% choose the ventilation template function
Fs = 47.68;
w = window(@blackman,size(Sv,2));
run startup             % EIDORS
figure
for k = 1:size(Sv,1)
    subplot(size(Sv,1),3,3*k-2); plot(Sv(k,:)); xlim([0 483])
    [Svfft(k,:), fAxis]   = ffts(Sv(k,:).*w',size(Sv,2)*10,Fs);
    subplot(size(Sv,1),3,3*k-1); plot(fAxis*60,Svfft(k,:)); xlim([0 100]);
    Xv_v    = Sv(k,:)'*pinv(Sv(k,:)')*X;
    Xv_h    = mean(Xv_v',2);    % Referenz
    dZ      = inv_solve(imdl, Xv_h,Xv_v');
    imgs	= calc_slices(dZ);
    subplot(size(Sv,1),3,3*k); imagesc(imgs(:,:,162)); axis square
    clear Xv_v Xv_h dZ imgs
end

argmaxv = input('Wähle die gewünschte Template Funktion der Atmung: ');
close

Tv = Sv(argmaxv,:)';                                % ventilation template function

Xv = Tv*pinv(Tv)*X;                                 % ventilation signal Xv


%% step 2 - calculate the cardiac signal Xc (and noise signal Xn)

Xc_1 = X - Xv;                                      % 1st approximation of the cardiac signal Xc_1

numPCc = 8;
Xwhc = Xc_1*pca(Xc_1,'NumComponents',numPCc);
[~,Sc] = jade(Xwhc');


% choose the cardiac template function
w = window(@blackman,size(Sc,2));
figure
for k = 1:size(Sc,1)
    subplot(size(Sc,1),3,3*k-2); plot(Sc(k,:)); xlim([0 483])
    [Scfft(k,:), fAxis]   = ffts(Sc(k,:).*w',size(Sc,2)*10,Fs);
    subplot(size(Sc,1),3,3*k-1); plot(fAxis*60,Scfft(k,:)); xlim([0 100]);
    Xc_v    = Sc(k,:)'*pinv(Sc(k,:)')*X;
    Xc_h    = mean(Xc_v',2);    % Referenz
    dZ      = inv_solve(imdl, Xc_h,Xc_v');
    imgs	= calc_slices(dZ);
    subplot(size(Sc,1),3,3*k); imagesc(imgs(:,:,180)); axis square
    clear Xv_v Xv_h dZ imgs
end

argmaxc = input('Wähle die gewünschte Template Funktion der Durchblutung: ');
close

Tc = Sc(argmaxc,:)';                               	% cardiac template function

Xc = Tc*pinv(Tc)*Xc_1;                                 % cardiac signal Xc


end
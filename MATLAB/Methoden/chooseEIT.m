function data = chooseEIT()

datafolder = 'C:\Users\Universität\Documents\Masterarbeit\MATLAB\Messdaten\EIT Test Adapter\';
filenames1 = dir(fullfile(datafolder,'*.mat'));
datafolder = 'C:\Users\Universität\Documents\Masterarbeit\MATLAB\Messdaten\Pig06\';
filenames2 = dir(fullfile(datafolder,'*.mat'));
datafolder = 'C:\Users\Universität\Documents\Masterarbeit\MATLAB\Messdaten\Pig14\';
filenames3 = dir(fullfile(datafolder,'*.mat'));

for k = 1:length(filenames1)
    disp([num2str(k), ':  ', char(filenames1(k).name)])
end
disp(' ');
for k = 1:length(filenames2)
    disp([num2str(k+length(filenames1)), ':  ', char(filenames2(k).name)])
end
disp(' ');
for k = 1:length(filenames3)
    disp([num2str(k+length(filenames1)+length(filenames2)), ':  ', char(filenames3(k).name)])
end

nfile = input('Wähle den gewünschten Datensatz: ');

switch nfile
    % SIMULATION
    case 1
        %
        load('20190925_Interface_50NmHR_40NmBR_reconstructed');
    case 2
        %
        load('20190925_Interface_72HiHR_12LoBR_reconstructed');
    case 3
        %
        load('20190925_Interface_75HiHR_12HiBR_reconstructed');
    case 4
        %
        load('20190925_Interface_75NmHR_12HiBR_reconstructed');
    case 5
        %
        load('20190925_Interface_NoHR_12HiBR_reconstructed');
        
        
    % SCHWEIN 1
    case 6
        % ca. 600 s
        load('PulHyp_20191030_PIG06__01-0__2D-EIT_reconstructed');
    case 7
        % ca. 52 s
        load('PulHyp_20191030_PIG06__01-1__2D-EIT_reconstructed');
    case 8
        % ca. 280 s (Mitte mit Apnoe)
        load('PulHyp_20191030_PIG06__01-3__2D-EIT_reconstructed');
        
        
    % SCHWEIN 2
    case 9
        % ca. 850 s
        load('PulHyp_20191030_PIG06__02-0__2D-EIT_reconstructed');
    case 10
        % ca. 60 s
        load('PulHyp_20191030_PIG06__02-1__2D-EIT_reconstructed');
    case 11
        % ca. 280 s (Mitte mit Apnoe)
        load('PulHyp_20191030_PIG06__02-3__2D-EIT_reconstructed');
        
        
    % SCHWEIN MIT ARTERIE
    case 12
        % ca. 370 s
        load('ARDSrampe_20210303_PIG14_healthy_P14_SO-1-li_reconstructed'); % ca. 370 s
    case 13
        % ca. 370 s
        load('ARDSrampe_20210303_PIG14_healthy_P14_SO-2-re_reconstructed'); % ca. 370 s
end

end
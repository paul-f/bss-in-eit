%% - BLIND SOURCE SEPARATION IN EIT: Combination of PCA and ICA - %%
%% init
clearvars -except data

addpath(genpath('C:\Users\Universität\Documents\Masterarbeit\MATLAB\'));
load('PulHyp_20191030_PIG06__01-0__2D-EIT_reconstructed');
% load('20190925_Interface_75HiHR_12HiBR_reconstructed');

clearvars -except data

X = data.voltages.raw;                   % Signal muss NxM-Matrix sein! -> Transponieren
% X = X(1+100:end-100,:);                           % für das Schwein
X = X-mean(X,2);                                      % Mittelwertfrei: Wikipedia: Spalten müssen mittelwertfrei sein? (s. Details, Abs. 2) - https://en.wikipedia.org/wiki/Principal_component_analysis

%% compute template function of ventilation signal
% [Usvd,~,~] = svd(X,'econ');
% [~,~,W]    = fastica(Usvd');
% S'         = W*Usvd';

% % vielleicht noch filtern?
% fs = 47.68;
% fc = 4.6;
% lpFilt = designfilt('lowpassfir', ...
%     'FilterOrder', 100, ...
%     'PassbandFrequency', fc/(fs/2), ...
%     'StopbandFrequency', fc/(fs/2)+0.1, ...
%     'DesignMethod', 'equiripple');
% fvtool(lpFilt)
% X = filtfilt(lpFilt, Xraw);
% % X = Xraw;



S = X'*pca(X');
Sv = S(:,1)';

%% template function of cardic signal
a = svd(S);
aNorm = a/a(1);
% figure
% bar(aNorm)
% xlim([0 10])
numPC = 0;
while aNorm(numPC+1) > 0.075
    numPC = numPC+1;
end

S_1 = S(:,2:numPC);

numIC = numPC-1;
[~,Sc_2] = jade(S_1');  
% rng default
% Sc_2 = fastica(S_1'/(max(max(S_1))), 'numOfIC', numIC);


Fs = 47.68;
w = window(@blackman,size(Sc_2,2));
figure
for k = 1:numIC
    subplot(numIC,2,2*k-1); plot(Sc_2(k,:)); xlim([0 483*2])
    [Scfft(k,:), fAxis]   = ffts(Sc_2(k,:).*w',size(Sc_2,2)*10,Fs);
    subplot(numIC,2,2*k); plot(fAxis*60,Scfft(k,:)); xlim([0 100]);
end

[argvalue, ~] = max(Scfft(:,find(fAxis>40/60))');   % for fh_min = 40 bpm
[~, argmax] = max(argvalue);

Sc = Sc_2(argmax,:);


%% compute ventilation and cadiac signal
B = [Sv; Sc; ones(1,length(S))];      % wenn man Bc und Bv tauscht, verändern sich die Amplituden von Xv und Xc
C = X*pinv(B);                        % C = inv(B'*B)*B'*X = [ai; bi; ci];
% C_ = X*inv(B'*B)*B;
                                    % matrix of scale factors and offsets C = inv(B'*B)*B'*X = [ai; bi; ci];

Xv = C(:,1).*Sv + 0.5*C(:,3);
Xc = C(:,2).*Sc + 0.5*C(:,3);

% B = [Tv'; Tc'];
% C = pinv(B')*X;
% Xv = C(1,:).*Tv;
% Xc = C(2,:).*Tc;
%
% Xv = Tv*pinv(Tv)*X;
% Xc = Tc*pinv(Tc)*X;

%
figure
subplot(2,1,1)
plot(X(800,:))
xlim([0 483])
% ylim([-1.75 1.75]/1e5)
subplot(2,1,2)
plot(Xv(800,:))
hold on
plot(Xc(800,:))
xlim([0 483])
% ylim([-1.75 1.75]/1e5)

%%
Fs = 47.68;
w = window(@blackman,size(X,2));
for k = 1:size(X,1)
[Xvfft(k,:), fAxis]   = ffts(Xv(k,:).*w',size(Xv,2)*10,Fs);
[Xcfft(k,:), fAxis]   = ffts(Xc(k,:).*w',size(Xc,2)*10,Fs);
end

[argvalueXv, ~] = max(Xvfft);
[~, frameXv] = max(argvalueXv);
[argvalueXc, ~] = max(Xcfft);
[~, frameXc] = max(argvalueXc);

figure
subplot(2,1,1); plot(fAxis*60,Xvfft(frameXv,:)); xlim([0 100]);
subplot(2,1,2); plot(fAxis*60,Xcfft(frameXc,:); xlim([0 100]);


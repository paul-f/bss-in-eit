T  = X*pca(X);
Tv = T(:,1);
T_ = T(:,2:numPC);
[W,~] = runica(T_');

Sc = W*T_';

Fs = 47.68;
w = window(@blackman,size(Sc,2));
figure
for k = 1:numPC
    subplot(numPC,2,2*k-1); plot(Sc(k,:)); xlim([0 483*2])
    [Scfft(k,:), fAxis]   = ffts(Sc(k,:).*w',size(Sc,2)*10,Fs);
    subplot(numPC,2,2*k); plot(fAxis*60,Scfft(k,:)); xlim([0 100]);
end

[argvalue, ~] = max(Scfft');
[~, argmax] = max(argvalue);

Tc = Sc(argmax,:)';

B = [Tv, Tc, ones(length(T),1)];      % wenn man Bc und Bv tauscht, verändern sich die Amplituden von Xv und Xc
C = pinv(B)*X;                        % C = inv(B'*B)*B'*X = [ai; bi; ci];
C_ = inv(B'*B)*B'*X;

Xv = C(1,:).*Tv + 0.5*C(3,:);
Xc = C(2,:).*Tc + 0.5*C(3,:);

figure
subplot(2,1,1)
plot(X(:,800))
xlim([0 483])
% ylim([-1.75 1.75]/1e5)
subplot(2,1,2)
plot(Xv(:,800))
hold on
plot(Xc(:,800))
xlim([0 483])
% ylim([-1.75 1.75]/1e5)
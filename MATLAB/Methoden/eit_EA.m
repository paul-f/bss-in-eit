function EA = eit_EA(X, y, x)

%% peaks (trigger) erzeugen
fs=             47.68;
X_=              squeeze(X(y,x,:));
w=              window(@blackman,length(X_));
[Xfft, fAxis]=  ffts(X_.*w,length(X_)*10,fs);
% figure;plot(fAxis*60,Xfft);xlim([0 120])
[~, M]=         max(Xfft(find(fAxis>40/60)));   % for fh_min = 40 bpm
M=              length(Xfft)-length(Xfft(find(fAxis>40/60)))+M;
fc=             fAxis(M);

bpFilt = designfilt('bandpassfir', ...
    'FilterOrder', 80, ...
    'CutoffFrequency1', fc-0.2, ...
    'CutoffFrequency2', fc+0.2, ...
    'SampleRate', fs);
Xc = filtfilt(bpFilt, X_);               % zero-phase filtering
% Xc = Xc'-mean(Xc',2);

%% find peaks
[~,locs] = findpeaks(Xc);%,'MinPeakHeight',10);     % minimale peakamplitude angeben
R_Peaks = zeros(1,length(X));
for k = 1:length(locs)
    R_Peaks(locs(k)) = max(Xc(928/2,:));
end
clear k

% time = (0:length(X)-1)/fs;
% figure
% plot(time, Xc(928/2,:))
% hold on
% plot(time, R_Peaks)
% plot(time, X(928/2,:))
% xlim([0 10])

% Zeitfenster bestimmen
lim = zeros(1,length(locs)-1);
for k = 2:length(locs)
    lim(k-1) = locs(k)-locs(k-1);
end
lim = ceil(mean(lim)/2);

%% EA
EA = zeros(size(X,1),size(X,2),2*lim+1);
for y = 1:size(X,1)
    for x = 1:size(X,1)
        for k = 2:length(locs)-1
            EA_(k-1,:) = X(y,x,(locs(k)-lim):((locs(k)+lim)));       % Signal bei R-Peaks
        end
        EA(y,x,:) = mean(EA_);
    end
end

% for l = 1:size(X,1)
%     EA_ = zeros(length(locs)-2,2*lim+1);                        % -2, damit gesichert wird, dass der erste und letzte wert nicht berücksichtigt wird, um zu verhindern, dass das Fenster zu groß ist
%     for k = 2:length(locs)-1
%         EA_(k-1,:) = X(l,(locs(k)-lim):((locs(k)+lim)));       % Signal bei R-Peaks
%     end
%     EA(l,:) = mean(EA_);
%     clear k
% end

% figure
% subplot(2,1,1)
% hold on
% plot(EA_')                                        % Ensemble Averaging (Mittelwert über die Signale der R-Peaks bei einem Zeitpunkt)
% subplot(2,1,2)
% plot(EA)

end
function [Xv, Xc] = v2020_bss_ica_of_pcs(X)
%%  BLIND SOURCE SEPARATION IN EIT:   --- ICA of PCs ---
%%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%%% step 1 - calculate template function of ventilation signal
%%% step 2 - calculate template function of cardic signal
%%% step 3 - calculate ventilation and cadiac signal


%% step 1 - calculate template function of ventilation signal
X = X';
X = X-mean(X);                                      % calculate mean free data

U = X*pca(X);                                       % template function matrix (principal component scores of X)
Tv = U(:,1);                                        % ventilation template function (1st principal component score of X)


%% step 2 - calculate template function of cardic signal
a = svd(X);                                         % compute singular values
aNorm = a/a(1);                                     % normalize singular values - plot: figure; bar(aNorm); xlim([0 30])
numPC = 0;
while aNorm(numPC+1) > 0.075
    numPC = numPC+1;                                % set number of principal components
end
U_ = U(:,2:numPC);
[~,Sc] = jade(U_');                                % source signal matrix (template funcions (numIC))

% find the template function with the largest energy
Fs = 47.68;
w = window(@blackman,size(Sc,2));
for k = 1:size(Sc,1)
    [Scfft(k,:), fAxis]   = ffts(Sc(k,:).*w',size(Sc,2)*10,Fs);
end

[argvalue, ~] = max(Scfft(find(fAxis>40/60))');
[~, argmax] = max(argvalue);

Tc = Sc(argmax,:)';                                 % cardiac template function


%% step 3 - calculate ventilation and cadiac signal

B = [Tv, Tc, ones(length(U),1)];                    % ?
C = pinv(B)*X;                                      % matrix of scale factors and offsets C = inv(B'*B)*B'*X = [ai; bi; ci];

Xv = C(1,:).*Tv + 0.5*C(3,:);                       % ventilation signal Xv
Xc = C(2,:).*Tc + 0.5*C(3,:);                       % cardiac signal Xv


end
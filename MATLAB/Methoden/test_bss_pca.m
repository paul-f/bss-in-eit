%% - BLIND SOURCE SEPARATION IN EIT: PCA - %%
%% init
% clearvars -except frameData X

addpath(genpath('C:\Users\Universität\Documents\Masterarbeit\MATLAB\'));
% frameData = load('20190925_Interface_NoHR_12HiBR_reconstructed');

% Schwein
% load('PulHyp_20191030_PIG06__01-0__2D-EIT_reconstructed');  % ca. 600 s
% load('PulHyp_20191030_PIG06__01-1__2D-EIT_reconstructed');  % ca. 52 s
load('PulHyp_20191030_PIG06__01-3__2D-EIT_reconstructed');  % ca. 280 s (Mitte mit Apnoe)

% Aterie abgeklemmt und Apnoe
% load('ARDSrampe_20210303_PIG14_healthy_P14_SO-1-li_reconstructed'); % ca. 370 s
% load('ARDSrampe_20210303_PIG14_healthy_P14_SO-2-re_reconstructed'); % ca. 370 s

frame = 800;

X = data.voltages.raw;               % Signal muss NxM-Matrix sein! -> Transponieren;
X = X-mean(X,2);                                  % Mittelwertfrei: Wikipedia: Spalten müssen mittelwertfrei sein? (s. Details, Abs. 2) - https://en.wikipedia.org/wiki/Principal_component_analysis
Xraw = X;
fs = 47.68;
time = (0:length(X)-1)/fs;

figure
plot(time,X(frame,:))

%% test Inputsignale
% ffts berechnen
% signale mit hoher energie auswählen
% signale plotten 
% signale auswählen, die nur gering phasenverschoben sind
% ausgewählten signale in pca stecken
Xraw = X;
Xraw = Xraw-mean(Xraw,2);                                  % Mittelwertfrei: Wikipedia: Spalten müssen mittelwertfrei sein? (s. Details, Abs. 2) - https://en.wikipedia.org/wiki/Principal_component_analysis

Fs = 47.68;
w = window(@blackman,size(Xraw,2));
for k = 1:size(Xraw,2)
    [Xfft(k,:), fAxis]   = ffts(Xraw(k,:).*w',size(Xraw,2)*10,Fs);
end

%
clear Xinp Xfft_
Xfftmax = max(max(Xfft));
index = 1;
for k = 1:size(Xraw,1)
%     max(Xfft(:,k)) <= Xfftmax*0.3
    if max(Xfft(k,:)) >= Xfftmax*0.9
        Xfft_(index,:) = Xfft(k,:);
        Xinp(index,:) = Xraw(k,:);
        ind(index) = k;
        index = index + 1;
    end
end

figure
plot(fAxis*60,Xfft_)
xlim([0 100])

% PHASE ANSCHAUEN
figure
hold on
xlim([0 483])
for k = 1:size(Xinp,1)
    plot(Xinp(k,:))
end

%% step 1 - 1st estimation of ventilation signals
% clearvars -except frameData frame
% Tv = Xinp*pca(Xinp,'NumComponents',1);                % Tv = PCA(X,1) = s1 = (s11, s21 ... sN1)^T = X*c1 [2008_Deibele, S. 5]
Sv = X'*pca(X','NumComponents',1);                % Tv = PCA(X,1) = s1 = (s11, s21 ... sN1)^T = X*c1 [2008_Deibele, S. 5]
% Tv = pca(Xraw','NumComponents',1);

Xv_1 = X*pinv(Sv')*Sv';                           % pinv(Tv) = inv(Tv'*Tv)*Tv';   % (Quelle) - https://www.mathworks.com/content/dam/mathworks/mathworks-dot-com/moler/leastsquares.pdf

%%
figure
plot(X(frame,:))
hold on
plot(Xv_1(frame,:))
plot(X(frame,:)-Xv_1(frame,:))
xlim([0 483])

%% step 2 - estimation of cardiac signal and final estimation of ventilation signal
clearvars -except frameData X Tv Xv_1 frame

% first approximation of Xc
Xc_1 = X - Xv_1;

%% inputs bestimmen
Fs = 47.68;
w = window(@blackman,size(Xc_1,2));
for k = 1:size(Xc_1,1)
    [Xfft(k,:), fAxis]   = ffts(Xc_1(k,:).*w',size(Xc_1,2)*10,Fs);
end

%
clear Xinp Xfft_
Xfftmax = max(max(Xfft));
index = 1;
for k = 1:size(Xc_1,1)
%     max(Xfft(k,:)) <= Xfftmax*0.3
    if max(Xfft(k,:)) >= Xfftmax*0.9
        Xfft_(index,:) = Xfft(k,:);
        Xinp(index,:) = Xc_1(k,:);
        index = index + 1;
    end
end
%
figure
plot(fAxis*60,Xfft_)
xlim([0 100])

% PHASE ANSCHAUEN
figure
hold on
xlim([0 483])
for k = 1:size(Xinp,2)
    plot(Xinp(k,:))
end
%%  sollten Funktionen nicht funktionieren, Filter im Remote-Desktop erstellen, speichern und in die Cloud hauen

% filtering
fs = 47.68;         % sampling rate
fl = 0.92;          % low cutoff frequency
fh = 4.6;           % high cutoff frequency
filtMethod = 'hplp';                            % filter method
switch filtMethod
    case 'bp'
        bpFilter = designfilt('bandpassfir', ...
            'FilterOrder', 30, ...
            'CutoffFrequency1', fl, ...
            'CutoffFrequency2', fh, ...
            'SampleRate', fs);
        % fvtool(bpFilt)
        Xcbp_1 = filtfilt(bpFilter, Xc_1);        % zero-phase filtering
    case 'hplp'
        hpFilter = designfilt('highpassfir', ...
            'FilterOrder', 30, ...
            'StopbandFrequency', 0.025, ...
            'PassbandFrequency', fl/(fs/2), ...
            'DesignMethod', 'equiripple');
        lpFilter = designfilt('lowpassfir', ...
            'FilterOrder', 30, ...
            'PassbandFrequency', fh/(fs/2), ...
            'StopbandFrequency', 0.25, ...
            'DesignMethod', 'equiripple');
        % fvtool(hpFilt)
        % fvtool(lpFilt)
        Xcbp_1 = filtfilt(lpFilter, ...           % zero-phase filtering
            filtfilt(hpFilter,Xc_1));
    case 'without'
        Xcbp_1 = Xc_1;                          % without filtering
end

% grpdelay(lpFilter)
% grpdelay(hpFilter)
% grpdelay(bpFilt)



%%
figure
plot(Xc_1(frame,:))
hold on
plot(Xcbp_1(frame,:))
xlim([0 483])

%%
Sc = Xcbp_1'*pca(Xcbp_1','NumComponents',2);       % first and second component (vielleicht auch noch 3.?)

% S = Xinp*pca(Xinp,'NumComponents',2);       % first and second component
% fheart = 60;
% ffac = fheart/60;
% heartperiod = fs/ffac;
% ps = round(heartperiod/3);                                        % phase-shift: 1/3 heart period
% Sc = [Sc(:,1) wshift('1D',Sc(:,1),ps) wshift('1D',Sc(:,1),-ps) ...
%     Sc(:,2) wshift('1D',Sc(:,2),ps) wshift('1D',Sc(:,2),-ps)];       % final Tc = [ s1 s1(+1/3) s1(-1/3) s2 s2(+1/3) s2(-1/3) ]

% a = svd(Xc_1);
% aNorm = a/a(1);
% figure
% bar(aNorm)
% xlim([0 50])
% SCHAUEN, OB SIGNALE DER MESSKANÄLE SICH IN DER PHASE VERSCHIEBEN ANDERS SIND UND REKONSTRUKTION
Xc1_2 = Xc_1*pinv(Sc')*Sc';
Xc2_2 = Xv_1*pinv(Sc')*Sc';

Xc = Xc1_2 + Xc2_2;
Xv = Xv_1 - Xc2_2;
%%
figure
hold on
plot(Sc(:,1))
plot(Sc(:,2))
plot(Sc(:,3))
plot(Sc(:,4))
xlim([0 483/4])


%% plot
frame = 928/2;
figure
subplot(2,1,1)
plot(X(frame,:))
% xlim([0 483])
legend('$X$','Interpreter','Latex')
subplot(2,1,2)
plot(Xv(frame,:))
hold on
plot(Xc(frame,:))
% xlim([0 483])
legend('$\bar{X_{V}}$', ...
    '$\bar{X_{C}}$', ...
    'Interpreter','Latex')

%% plot all
figure
frame = 230;             % 10 könnte Lunge sein, 20 Herz
plot(X(frame,:))
hold on
plot(Xv_1(frame,:))
plot(Xc_1(frame,:))
plot(Xcbp_1(frame,:))
plot(Xc1_2(frame,:))
plot(Xc2_2(frame,:))
plot(Xv(frame,:))
plot(Xc(frame,:))
xlim([0 483])
legend('$X$', ...
    '$\bar{X^{\prime}_V}$', ...
    '$\bar{X^{\prime}_C}$', ...
    '$\bar{X^{\prime}_{C_{BP}}}$', ...
    '$\bar{X^{\prime\prime}_{C1}}$', ...
    '$\bar{X^{\prime\prime}_{C2}}$', ...
    '$\bar{X_{V}}$', ...
    '$\bar{X_{C}}$', ...
    'Interpreter','Latex')

%% fft
xControl = X(frame,:)-mean(X(frame,:));
xv_1 = Xv_1(frame,:)-mean(Xv_1(frame,:));
fs = 47.68;
N = length(xControl);
w = window(@hamming,N);                         % window
[XControl, fXAxis] = ffts(xControl.*w', N*10, fs);   % zero-padding and windowing
[Xv_1fft, fXAxis] = ffts(xv_1.*w', N*10, fs);   % zero-padding and windowing

figure
subplot(2,1,1)
plot(fXAxis*60,XControl(1:length(fXAxis)))
xlim([0 250])
subplot(2,1,2)
plot(fXAxis*60,Xv_1fft(1:length(fXAxis)))
xlim([0 250])

%%
N = length(Xc);
w = window(@blackman,N);                     	% window
[Xfft, fAxis] = ffts(X(800,:).*w',N*10,fs);
[XV, fAxis] = ffts(Xv(800,:).*w',N*10,fs);
[XC, fAxis] = ffts(Xc(800,:).*w',N*10,fs);
bpmAxis = fAxis*60;

figure
subplot(3,1,1)
plot(bpmAxis,Xfft(1:length(fAxis)))
xlim([0 100])
subplot(3,1,2)
plot(bpmAxis,XV(1:length(fAxis)))
xlim([0 100])
subplot(3,1,3)
plot(bpmAxis,XC(1:length(fAxis)))
xlim([0 100])
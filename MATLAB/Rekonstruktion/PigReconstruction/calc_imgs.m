function [imgs1, imgs2, varargout] = calc_imgs(imdl, v_v1, v_v2, varargin)
switch nargin
    case 2
        v_h1  = mean(v_v1,2);                 % get reference voltages
        dZ1   = inv_solve(imdl, v_h1,v_v1);    % Reconstruct Images
        imgs1 = calc_slices(dZ1);
    case 3
        v_h1  = mean(v_v1,2);               v_h2  = mean(v_v2,2);
        dZ1   = inv_solve(imdl, v_h1,v_v1); dZ2   = inv_solve(imdl, v_h2,v_v2);
        imgs1 = calc_slices(dZ1);           imgs2 = calc_slices(dZ2);
end
end
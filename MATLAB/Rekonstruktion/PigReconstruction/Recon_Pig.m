% Recon_Pig.m
% reconstruct pig eit data
%% RUN EIDORS
% LMK 2019
if 0
clearvars -except data

addpath(genpath('C:\Users\Universität\Documents\Masterarbeit\MATLAB\'));
run startup             % EIDORS

datafolder = 'C:\Users\Universität\Documents\Masterarbeit\MATLAB\Messdaten\Pig06\';
datafolder = 'C:\Users\Universität\Documents\Masterarbeit\MATLAB\Messdaten\Pig14\';
datafolder = 'C:\Users\Universität\Documents\Masterarbeit\MATLAB\Messdaten\veraenderl_HR\';

filenames = dir(fullfile(datafolder,'*.eit'));
end
clearvars -except datafolder filenames

%% Load EIT Data
% get voltages
nfile = 2;
[v_v, auxdata, stim] = eidors_readdata(char(filenames(nfile).name), 'LQ4');
% [v_v, auxdata, stim ]         = eidors_readdata(EITfile, 'LQ4');
% v_v = v_v(:, 101:end-100);

nel      = 32;
offset   = 4;
neighbor = 2;

disc   = elec_to_discard_by_pos(nel, offset);
neigh  = elec_neighbor_pos(nel, offset, neighbor);

v_v(neigh,:)    = 0;                % set neighbors to 0
v_v(disc,:)     = [];               % discard measurements with electrode lying on sink/source
v_v             = abs(v_v);

figure; plot(v_v(800,1:500))
cut = input('Anfang - Wo abschneiden? Eingabe: ');
v_v = v_v(:,cut+1:end); close
figure; plot(v_v(800,end-499:end))
cut = input('Ende - Wo abschneiden? Eingabe: '); cut = 500-cut;
v_v = v_v(:,1:end-cut); close


%% Reconstruction
load('Pig_Model.mat')               % load model
data.imgs.raw       = calc_imgs(imdl, v_v);
data.voltages.raw	= v_v;

% (:,1:ceil(47.68*120))

reconMeth_ = (['pca'; 'ica'; 'ipa']);
for k = 3%:length(reconMeth_)
    reconMeth = reconMeth_(k,:);
    switch reconMeth
        case 'pca'
            disp('Calc PCA ...')
            % OHNE APNOE: (:,1:5750);  APNOE: (:,6001:7600)
%             [v_vVen, v_vCar,  v_vVen1, v_vCar1, v_vCar2, v_vCar2Ven]= bss_pca(v_v(:,1:5750));
%             [~, ~, v_vVen1, v_vCar1, v_vCar2, v_vCar2Ven]= bss_pca(v_v(:,6001:7600));
            [v_vVen, v_vCar, ~, ~, ~, ~]= bss_pca(v_v);
            [data.imgs.pca.ven, data.imgs.pca.car]  = ...
                calc_imgs(imdl, v_vVen, v_vCar);
            data.voltages.pca.ven           = v_vVen;
            data.voltages.pca.car           = v_vCar;
            [data.imgs.pca.comp.ven1, data.imgs.pca.comp.car1] = ...
                calc_imgs(imdl, v_vVen1, v_vCar1);
            data.voltages.pca.comp.ven1     = v_vVen1;
            data.voltages.pca.comp.car1     = v_vCar1;
            [data.imgs.pca.comp.car2, data.imgs.pca.comp.car2ven] = ...
                calc_imgs(imdl, v_vCar2, v_vCar2Ven);
            data.voltages.pca.comp.car2     = v_vCar2;
            data.voltages.pca.comp.car2ven = v_vCar2Ven;
        case 'ica'
            disp('Calc ICA ...')
            [v_vVen, v_vCar]                = bss_ica(v_v,imdl);
            [data.imgs.ica.ven, data.imgs.ica.car] = ...
                calc_imgs(imdl, v_vVen, v_vCar);
            data.voltages.ica.ven           = v_vVen;
            data.voltages.ica.car           = v_vCar;
        case 'ipa'
            disp('Calc ICA of PCs ...')
            [v_vVen, v_vCar]                = bss_ica_of_pcs(v_v);
            [data.imgs.icapca.ven, data.imgs.icapca.car] = ...
                calc_imgs(imdl, v_vVen, v_vCar);
            data.voltages.icapca.ven        = v_vVen;
            data.voltages.icapca.car        = v_vCar;
    end
end

%%
% Reconstruction (filter and EA)
% filter
disp('Calc Filtering ...')
[v_vVen, v_vCar] = eit_filter(v_v);
[data.imgs.filt.ven, data.imgs.filt.car] = ...
    calc_imgs(imdl, v_vVen, v_vCar);
data.voltages.filt.ven          = v_vVen;
data.voltages.filt.car          = v_vCar;

% EA (only cardiac)
disp('Calc EA ...')
y = 12; x = 13;
data.imgs.ea  = eit_EA(data.imgs.raw, y, x);
%%
% save
disp('Save data ...')
name = char(filenames(nfile).name);
save([datafolder, name(1:end-4), '_reconstructed.mat'], 'data', '-v7.3')


%% old
load('Pig_Model.mat')               % load model
data.imgs.raw       = calc_imgs(imdl, v_v);
data.voltages.raw	= v_v;

reconMeth_ = (['pca'; 'ica'; 'ipa']);
for k = 1:length(reconMeth_)
    reconMeth = reconMeth_(k,:);
    switch reconMeth
        case 'pca'
            disp('Calc PCA ...')
            [v_vVen, v_vCar]                = v2008_bss_pca(v_v);
            [data.imgs.pca.ven, data.imgs.pca.car] = ...
                calc_imgs(imdl, v_vVen', v_vCar');
            data.voltages.pca.ven           = v_vVen';
            data.voltages.pca.car           = v_vCar';
        case 'ica'
            disp('Calc ICA ...')
            [v_vVen, v_vCar]                = v2013_bss_ica(v_v,imdl);
            [data.imgs.ica.ven, data.imgs.ica.car] = ...
                calc_imgs(imdl, v_vVen', v_vCar');
            data.voltages.ica.ven           = v_vVen';
            data.voltages.ica.car           = v_vCar';
        case 'ipa'
            disp('Calc ICA of PCs ...')
            [v_vVen, v_vCar]                = v2020_bss_ica_of_pcs(v_v);
            [data.imgs.icapca.ven, data.imgs.icapca.car] = ...
                calc_imgs(imdl, v_vVen', v_vCar');
            data.voltages.icapca.ven        = v_vVen';
            data.voltages.icapca.car        = v_vCar';
    end
end
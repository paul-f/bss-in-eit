% Reconstrcution of data recorded with sentec advanced interface
% LMK 2019
clear

addpath(genpath('C:\Users\Universität\Documents\Masterarbeit\MATLAB\'));

run startup             % EIDORS

% datafolder = '\\nvs2.uni-rostock.de\elainedata\Elektroimpedanztomografie\Studentenprojekte\Signale\Masterarbeit\Messdaten\EIT Test Adapter';
datafolder = 'C:\Users\Universität\Documents\Masterarbeit\MATLAB\Messdaten\EIT Test Adapter\';
filenames = dir(fullfile(datafolder,'*.eit'));

%% load data and reconstruct images

load('ImdlHuman32by32');
% for 
    nfile = 1;%1:length(filenames)
    [v_v, auxdata, stim] = eidors_readdata([datafolder, char(filenames(nfile).name)], 'LQ4');
    
    % reconstruction
    nel      = 32;
    offset   = 4;
    neighbor = 2;
    
    % mapVec = mappingReprocity(nel, offset);
    disc   = elec_to_discard_by_pos(nel, offset);
    neigh  = elec_neighbor_pos(nel, offset, neighbor);
    
    % set neighbors to 0
    v_v(neigh,:) = 0;
    
    % discard measurements with electrode lying on sink/source
    v_v(disc,:) = [];
    v_v = real(v_v);                % hier: Syncsignal am Ende und Anfang abschneiden !!! v_v(:,101:length(v_v)-100)
    %    v_v = v_v(:,200:length(v_v)-201);       % für Schweine
    
    % BSS
    % PCA
%     [v_v_pcaVen, v_v_pcaCar] = bss_pca(v_v);
%     % ICA
    [v_v_icaVen, v_v_icaCar] = bss_ica(v_v, imdl);
    % ICA of PCA
    [v_v_icapcaVen, v_v_icapcaCar] = bss_ica_of_pcs(v_v);

    
    % image reconstruction
    v_h             = mean(v_v,2);    % Referenz
    v_h_pcaVen      = mean(v_v_pcaVen,2);
    v_h_pcaCar      = mean(v_v_pcaCar,2);
    v_h_icaVen      = mean(v_v_icaVen,2);
    v_h_icaCar      = mean(v_v_icaCar,2);
    v_h_icapcaVen   = mean(v_v_icapcaVen,2);
    v_h_icapcaCar   = mean(v_v_icapcaCar,2);

    dZ = inv_solve(imdl, v_h,v_v);
    dZpcaVen        = inv_solve(imdl, v_h_pcaVen,v_v_pcaVen);
    dZpcaCar        = inv_solve(imdl, v_h_pcaCar,v_v_pcaCar);
    dZicaVen        = inv_solve(imdl, v_h_icaVen,v_v_icaVen);
    dZicaCar        = inv_solve(imdl, v_h_icaCar,v_v_icaCar);
    dZicapcaVen     = inv_solve(imdl, v_h_icapcaVen,v_v_icapcaVen);
    dZicapcaCar     = inv_solve(imdl, v_h_icapcaCar,v_v_icapcaCar);
    imgs            = calc_slices(dZ);
    imgspcaVen      = calc_slices(dZpcaVen);
    imgspcaCar      = calc_slices(dZpcaCar);
    imgsicaVen      = calc_slices(dZicaVen);
    imgsicaCar      = calc_slices(dZicaCar);
    imgsicapcaVen	= calc_slices(dZicapcaVen);
    imgsicapcaCar   = calc_slices(dZicapcaCar);
    data.imgs.raw               = imgs;
    data.imgs.pca.ven           = imgspcaVen;
    data.imgs.pca.car           = imgspcaCar;
    data.imgs.ica.ven           = imgsicaVen;
    data.imgs.ica.car           = imgsicaCar;
    data.imgs.icapca.ven        = imgsicapcaVen;
    data.imgs.icapca.car        = imgsicapcaCar;
    data.voltages.raw           = v_v;
    data.voltages.pca.ven       = v_v_pcaVen;
    data.voltages.pca.car       = v_v_pcaCar;
    data.voltages.ica.ven       = v_v_icaVen;
    data.voltages.ica.car       = v_v_icaCar;
    data.voltages.icapca.ven    = v_v_icapcaVen;
    data.voltages.icapca.car    = v_v_icapcaCar;
    
    name = char(filenames(nfile).name);
    save([datafolder, name(1:end-4), '_reconstructed.mat'], 'data', '-v7.3')
    
% end
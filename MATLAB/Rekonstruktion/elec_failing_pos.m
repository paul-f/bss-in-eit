function out = elec_failing_pos(n_elec, offset, el_fail)
%elect_failing_pos   Indices of measurements to discard due to
%                               failing
%
%  OUT=elect_to_discard_failing_bmu(N_ELEC, OFFSET) calculates a logical vector
%  [1 x N_ELEC*N_ELEC] indicating which measurements in a frame are not
%  suitable for reconstruction beacause of failing
% 
%  INPUTS:
%    N_ELEC  - number of electrodes
%    OFFSET  - the "gap" between the injection and sink electrode
%              e.g. stimulation between elec 1 and 6: offset = 4;
%    EL_FAIL - Failing electrodes
%

% Author : beat mueller
% Project: Voucher
% Task   : Matlab TIC Simulator
% Copyright 2012, Swisstom AG

% run test
if ischar(n_elec) && strcmp(n_elec,'UNIT_TEST'), out = do_unit_test; return; end

%% Main Body

out = false(1,n_elec*n_elec);

for i = el_fail
    % if electrode i injecting, then all this measurements are useless
    out((i-1)*n_elec + 1 : i*n_elec) = 1;
    %the same for the offset-pair
    % the sink is in front of the source   
    % 20130208bmu: I think this way round is correct, not adding the offset
    i_off = i - offset - 1;
    if i_off < 1
        i_off = n_elec + i_off;
    end

    out((i_off - 1)*n_elec + 1 : i_off*n_elec) = 1;

    % electrode cannot be used for measurements
    out(i:n_elec:end) = 1;
    out(i_off:n_elec:end) = 1;

    if 0
        % try optimal: remove measurement (fail-1) pattern, el (fail+1)
        i_opt1 = i - 1;
        if  i_opt1 < 1
            i_opt1 = n_elec + i_opt1;
        end
        
        i_opt2 = i + 1;
        if  i_opt2 > n_elec;
            i_opt2 = i_opt2 - n_elec;
        end
        
        out((i_opt1 - 1) * 32 + i_opt2) = 1; 
    end
    
    if 0
    % try optimal: remove all measurement for el (fail+1)
        
        i_opt2 = i + 1;
        if  i_opt2 > n_elec;
            i_opt2 = i_opt2 - n_elec;
        end
        
        out(i_opt2:32:end) = 1; 
    end
    
    if 0
    % try optimal: remove all measurement for el (fail+1) + opposite
        
        i_opt1 = i + 1;
        if  i_opt1 > n_elec;
            i_opt1 = i_opt1 - n_elec;
        end
        
        i_opt2 = i_opt1 - offset - 1;
        if i_opt2 < 1
            i_opt2 = n_elec + i_opt2;
        end   
        out(i_opt1:32:end) = 1; 
        out(i_opt2:32:end) = 1;
    end
    
end


function out = do_unit_test
% Run unit tests
n_elec = 32;
% the current default:
p = elec_failing_pos(n_elec,4,[1]);
disp([p(1), p(6), p(28), p(34), p(39)])
% adjecent stimulation:
p = elec_failing_pos(16,0,[]);
disp(p(17:n_elec));
% 3 electrode gap
p = elec_failing_pos(16,3,[1,2]);
disp(p(17:n_elec));
% opposite electrodes
p = elec_failing_pos(16,7,[1 4]);
out = p(1:16);

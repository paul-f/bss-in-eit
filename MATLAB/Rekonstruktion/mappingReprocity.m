function [mapVec, mapVecReduce] = mappingReprocity(n_elec, offset)
%MAPPINGREPROCITY 
%  creates vectors containing the mapping between the reprocity-pairs
%
%  mapVec: size 928, contains for each index the index of the reprocity pair
%  mapVecReduce: contains true always for the first entry of the reprocitiy pair
%
%  INPUTS:
%    N_ELEC  - number of electrodes
%    OFFSET  - the "gap" between the injection and sink electrode
%

% Author : beat mueller
% Copyright 2013, Swisstom AG

   mapMat = zeros(n_elec, n_elec); 
   mapMatReduce = false(n_elec, n_elec);

   %% disc_vector
   el_vec = false(1,n_elec*n_elec);
   el_vec(1) = 1; % injection electrode
   el_vec(offset + 2) = 1; % read electrode
   el_vec(n_elec - offset) = 1; % opposite read electrode

   for i = 2:n_elec
      el_vec( (i-1)*n_elec + (1:n_elec) ) = circshift(el_vec(1:n_elec),[0 i-1]);
   end

   count = 1;
   for i = 1:n_elec
      for j = i:n_elec
	if el_vec((i-1) * n_elec + j) == 0
		%mapVec(i,j) = mapVec(j,i)
		%mapVec((i-1) * 32 + j) = count;
		%mapVec((j-1) * 32 + i) = count;
		mapMat(i,j) = count;
		mapMat(j,i) = count;
		mapMatReduce(i,j) = true;
		count = count + 1;
	end
      end
   end

   %mapMat = mapMat';
   mapVec = mapMat(:);
   mapVec(el_vec) = [];

   %mapMatReduce = mapMatReduce';
   mapVecReduce = mapMatReduce(:);
   mapVecReduce(el_vec) = [];

   %save('/home/bmu/mapVec.mat', 'mapVec', 'mapVecReduce');

function out = elec_neighbor_pos(n_elec, offset, range)
%ELEC_OUTSIDE_POS Indices of measurements that are neighbors within +- range 
%  OUT=ELEC_OUTSIDE_POS(N_ELEC, OFFSET, RANGE) calculates a logical vector
%  [1 x N_ELEC*N_ELEC] indicating which measurements are neighbors
% 
%  INPUTS:
%    N_ELEC  - number of electrodes
%    OFFSET  - the "gap" between the injection and sink electrode
%              e.g. stimulation between elec 1 and 6: offset = 4;
%    RANGE   - distance for neighboring
%

% Author : beat mueller
% Project: Voucher
% Task   : Matlab TIC Simulator
% Copyright 2012, Swisstom AG
% $Id: elec_to_discard_by_pos.m 95 2012-04-16 20:56:48Z bgr $

% run test
if ischar(n_elec) && strcmp(n_elec,'UNIT_TEST'), out = do_unit_test; return; end
% identify myself

%% Main Body
out = false(1,n_elec*n_elec);
out(1) = 1; % injection electrode
out(offset + 2) = 1; % read electrode
out(n_elec - offset) = 1; % opposite read electrode

for i = 1:range
   out(n_elec + 1 - i) = 1; % injection electrode
   out(1 + i) = 1; % injection electrode
   out(mod(offset + 1 - i,n_elec) + 1) = 1; % read electrode
   out(offset + 2 + i) = 1; % read electrode
   out(n_elec - offset - i) = 1; % opposite read electrode
   out(mod(n_elec - offset + i - 1, n_elec) + 1) = 1; % opposite read electrode
end 

for i = 2:n_elec
   out( (i-1)*n_elec + (1:n_elec) ) = circshift(out(1:n_elec),[0 i-1]);
end



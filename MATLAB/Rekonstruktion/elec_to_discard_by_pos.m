function out = elec_to_discard_by_pos(n_elec, offset)
%ELEC_TO_DISCARD_BY_POS   Indices of measurements to discard.
%  OUT=ELEC_TO_DISCARD_BY_POS(N_ELEC, OFFSET) calculates a logical vector
%  [1 x N_ELEC*N_ELEC] indicating which measurements in a frame are not
%  suitable for reconstruction.
% 
%  INPUTS:
%    N_ELEC  - number of electrodes
%    OFFSET  - the "gap" between the injection and sink electrode
%              e.g. stimulation between elec 1 and 6: offset = 4;
%

% Author : Bartlomiej Grychtol <b.grychtol@gmail.com>
% Project: Voucher
% Task   : Matlab TIC Simulator
% Copyright 2012, Swisstom AG
% $Id: elec_to_discard_by_pos.m 95 2012-04-16 20:56:48Z bgr $

% run test
if ischar(n_elec) && strcmp(n_elec,'UNIT_TEST'), out = do_unit_test; return; end
% identify myself

%% Main Body
out = false(1,n_elec*n_elec);
out(1) = 1; % injection electrode
out(offset + 2) = 1; % read electrode
out(n_elec - offset) = 1; % opposite read electrode

for i = 2:n_elec
   out( (i-1)*n_elec + (1:n_elec) ) = circshift(out(1:n_elec),[0 i-1]);
end



function out = do_unit_test
% Run unit tests

% the current default:
p = elec_to_discard_by_pos(32,4);
disp([p(1), p(6), p(28), p(34), p(39)]) % all should be 1;
% adjecent stimulation:
p = elec_to_discard_by_pos(16,0);
disp(p(17:32));
% 3 electrode gap
p = elec_to_discard_by_pos(16,3);
disp(p(17:32));
% opposite electrodes
p = elec_to_discard_by_pos(16,7);
out = p(1:16);
